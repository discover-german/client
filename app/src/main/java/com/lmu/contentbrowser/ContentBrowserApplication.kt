package com.lmu.contentbrowser

import android.app.Application
import com.lmu.contentbrowser.database.MyRepository
import com.lmu.contentbrowser.database.MyRoomDatabase
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.SupervisorJob

class ContentBrowserApplication : Application() {
    var applicationScope = CoroutineScope(SupervisorJob())

    val database by lazy {
        MyRoomDatabase.getDatabase(this, applicationScope)
    }
    val repository by lazy {
        MyRepository(
            database.whitelistItemDao(),
            database.phraseDao(),
            database.favoriteDao(),
            database.historyItemDao(),
            database.eventDao(),
            database.eventMetaDao(),
            database.contentPreferenceDao(),
            database.logEventDao()
        )
    }
}