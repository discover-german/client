package com.lmu.contentbrowser.data

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "events")
class Event(

        @PrimaryKey(autoGenerate = true) val id: Long,

        @ColumnInfo(name= "type") val type: String

)