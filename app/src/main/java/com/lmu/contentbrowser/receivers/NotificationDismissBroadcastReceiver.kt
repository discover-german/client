package com.lmu.contentbrowser.receivers

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import com.lmu.contentbrowser.ContentBrowserApplication
import com.lmu.contentbrowser.data.LogEvent
import com.lmu.contentbrowser.data.LogEventType
import kotlinx.coroutines.*
import kotlin.coroutines.coroutineContext
import kotlin.coroutines.suspendCoroutine

class NotificationDismissBroadcastReceiver: BroadcastReceiver() {
    override fun onReceive(context: Context?, intent: Intent?) {
        if (context!= null && intent != null) {
            val extras = intent.extras
            if (context.applicationContext is ContentBrowserApplication && extras != null) {
                val eventType = extras.getString("eventType")
                if (eventType != null) {
                    val logEventType =
                        if (eventType == "article") {LogEventType.ARTICLE_NOTIFICATION_DISMISS}
                        else if (eventType == "book") {LogEventType.BOOK_NOTIFICATION_DISMISS}
                        else if (eventType == "podcast") {LogEventType.PODCAST_NOTIFICATION_DISMISS}
                        else {LogEventType.NONE}
                    val currentTime = System.currentTimeMillis()
                    val logEvent = LogEvent(0, logEventType, currentTime, null, false)
                    CoroutineScope(Dispatchers.IO).launch {
                        (context.applicationContext
                                as ContentBrowserApplication).repository.insertLogEvent(logEvent)
                    }
                }
            }
        }
    }
}