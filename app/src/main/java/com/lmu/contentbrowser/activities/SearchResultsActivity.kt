package com.lmu.contentbrowser.activities

import android.app.Activity
import android.os.Bundle
import android.widget.ListView
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import com.lmu.contentbrowser.ContentBrowserApplication
import com.lmu.contentbrowser.R
import com.lmu.contentbrowser.adapters.HistoryAdapter
import com.lmu.contentbrowser.data.HistoryItem
import com.lmu.contentbrowser.viewmodels.FavoritesViewModel
import com.lmu.contentbrowser.viewmodels.FavoritesViewModelFactory
import com.lmu.contentbrowser.viewmodels.HistoryViewModel
import com.lmu.contentbrowser.viewmodels.HistoryViewModelFactory
import kotlinx.coroutines.launch

class SearchResultsActivity: AppCompatActivity() {

    val favoritesViewModel: FavoritesViewModel by viewModels {
        FavoritesViewModelFactory((application
                as ContentBrowserApplication).repository)
    }

    val historyViewModel: HistoryViewModel by viewModels {
        HistoryViewModelFactory((application as ContentBrowserApplication).repository)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_search_results)

        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        val b = intent.extras
        val query = b?.getString("query") ?: ""
        val favoritesOnly = b?.getBoolean("favoritesOnly") ?: false

        supportActionBar?.title = "Results for: $query"

        lifecycleScope.launch {
            historyViewModel.searchWithScore(query)
        }

        historyViewModel.searchResults.observe(this) {
            val entries = arrayListOf<String>()
            Thread {
                for (historyItem in it) {
                    if (favoritesOnly) {
                        val favorite = favoritesViewModel.getItemByOidAndContentType(
                            historyItem.oid,
                            historyItem.contentType
                        )
                        if (favorite != null && favorite.isFavorite) {
                            entries.add(createEntryString(historyItem))
                        }
                    } else {
                        entries.add(createEntryString(historyItem))
                    }
                }
                runOnUiThread {
                    val listview = findViewById<ListView>(R.id.listview)
                    val myAdapter = HistoryAdapter(this, entries.toTypedArray())
                    listview.adapter = myAdapter
                }
            }.start()
        }
    }

    private fun createEntryString(historyItem: HistoryItem): String {
        return "${historyItem.contentType}|${historyItem.title}|" +
                    "${historyItem.timeAdded}|${historyItem.description}|${historyItem.oid}|" +
                    "${historyItem.imageLink}|${historyItem.url}|${historyItem.id}"
    }
}