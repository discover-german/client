package com.lmu.contentbrowser.data

data class ContentItem(
    val title: String,
    val body: String
)
