package com.lmu.contentbrowser.data

data class TranslationResponse(
    val data: TranslationData
)

data class TranslationData(
    val translations: ArrayList<Translation>
)

data class Translation(
    var translatedText: String,
    val detectedSourceLanguage: String
)
