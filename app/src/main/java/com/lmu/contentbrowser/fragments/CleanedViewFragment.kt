package com.lmu.contentbrowser.fragments

import android.annotation.SuppressLint
import android.graphics.Typeface
import android.os.Bundle
import android.text.Html
import android.util.Log
import android.view.*
import android.widget.*
import androidx.appcompat.widget.LinearLayoutCompat
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.text.HtmlCompat
import androidx.core.widget.NestedScrollView
import androidx.fragment.app.Fragment
import com.google.gson.Gson
import com.lmu.contentbrowser.R
import com.lmu.contentbrowser.Constants
import com.lmu.contentbrowser.activities.ViewerActivity
import com.lmu.contentbrowser.data.Book
import com.lmu.contentbrowser.helpers.GzipHelper
import okhttp3.*
import okhttp3.HttpUrl.Companion.toHttpUrlOrNull
import java.lang.Exception
import java.util.concurrent.TimeUnit


class CleanedViewFragment: Fragment() {
    private var bookPages: ArrayList<String>? = null
    private var bookPageIndex = 0
    private val itemUrl = Constants.SERVER_URL+"/item"
    private val client = OkHttpClient.Builder().connectTimeout(Constants.CONNECT_TIMEOUT,
        TimeUnit.SECONDS).build()
    private val gson = Gson()
    private val gzipHelper = GzipHelper()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_cleanedview, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val b = requireActivity().intent.extras
        val item = b?.getString("item")
        var body = ""
        var contentType = ""
        var oid = ""
        if (item!=null && item.split("|").size >= 6) {
            contentType = item.split("|")[0]
            body = item.split("|")[2]
            oid = item.split("|")[3]
        }
        else if ((requireActivity() as ViewerActivity).itemContentType != "" &&
            (requireActivity() as ViewerActivity).itemDescription != "" &&
            (requireActivity() as ViewerActivity).itemOid != "") {
            contentType = (requireActivity() as ViewerActivity).itemContentType
            body = (requireActivity() as ViewerActivity).itemDescription
            oid = (requireActivity() as ViewerActivity).itemOid
        }
        else {
            return
        }
        when (contentType) {
            "article" -> handleArticle(body)
            "book" -> handleBook(body, oid)
        }
    }

    private fun handleArticle(body: String) {
        val textContainer: LinearLayoutCompat = requireActivity().findViewById(R.id.textContainer)
        for ((i, line) in body.split("\n").withIndex()){
            val paragraphLayout = layoutInflater.inflate(R.layout.layout_paragraph,
                textContainer, false) as TextView
            paragraphLayout.text = line
            if (i==0) {
                paragraphLayout.setTypeface(paragraphLayout.typeface, Typeface.BOLD);
            }
            enhanceTextSelection(paragraphLayout)
            textContainer.addView(paragraphLayout)
        }
        val spaceLayout = layoutInflater.inflate(R.layout.layout_menu_bar_space,
            textContainer, false) as Space
        textContainer.addView(spaceLayout)
    }

    private fun handleBook(firstPage: String, oid: String) {
        bookPages = arrayListOf(firstPage)
        Thread{
            populatePages(oid)
        }.start()
        displayPage(bookPageIndex)
    }

    @SuppressLint("SetTextI18n")
    private fun displayPage(index: Int) {
        if (bookPages != null) {
            val nestedScrollView: NestedScrollView =
                requireActivity().findViewById(R.id.cleanedContainer)
            nestedScrollView.scrollTo(0, 0);
            val textContainer: LinearLayoutCompat = requireActivity().findViewById(R.id.textContainer)
            textContainer.removeAllViews()
            val navigationBarTop = prepareNavigationBar(index, textContainer)
            textContainer.addView(navigationBarTop)
            if (bookPages!!.size > index) {
                val page = bookPages!![index]
                val pageView = layoutInflater.inflate(
                    R.layout.layout_paragraph,
                    textContainer, false) as TextView
                var text = ""
                for (line in page.split("\n")) {
                    if (line.startsWith("##")) {
                        text += "<b><p>"+line.replace("#", "")+"</p></b>"
                    }
                    else {
                        text += "<p>"+line+"</p>"
                    }
                }
                pageView.text = Html.fromHtml(text, HtmlCompat.FROM_HTML_MODE_LEGACY)
                enhanceTextSelection(pageView)
                textContainer.addView(pageView)
                val navigationBarBottom = prepareNavigationBar(index, textContainer)
                textContainer.addView(navigationBarBottom)
                val spaceLayout = layoutInflater.inflate(
                    R.layout.layout_menu_bar_space,
                    textContainer, false
                ) as Space
                textContainer.addView(spaceLayout)
            }
        }
    }

    /**
     * val paragraphLayout = layoutInflater.inflate(
    R.layout.layout_paragraph,
    textContainer, false
     */

    private fun prepareNavigationBar(index: Int,
                                     textContainer: LinearLayoutCompat): ConstraintLayout {
        val navigationBar: ConstraintLayout = layoutInflater.inflate(
            R.layout.layout_nav_bar_book,
            textContainer, false
        ) as ConstraintLayout
        val navBack: ImageView = navigationBar.findViewById(R.id.navBack)
        val navBackText: TextView = navigationBar.findViewById(R.id.navBackText)
        val navForward: ImageView = navigationBar.findViewById(R.id.navForward)
        val navForwardText: TextView = navigationBar.findViewById(R.id.navForwardText)
        val pageIndexDescription: TextView = navigationBar.findViewById(R.id.pageIndexDescription)
        if (index == 0) {
            navBack.visibility = View.INVISIBLE
            navBackText.visibility = View.INVISIBLE
            navForward.setOnClickListener {
                bookPageIndex += 1
                displayPage(bookPageIndex)
            }
        }
        else if(index == bookPages!!.size - 1) {
            navForward.visibility = View.INVISIBLE
            navForwardText.visibility = View.INVISIBLE
            navBack.setOnClickListener {
                bookPageIndex -= 1
                displayPage(bookPageIndex)
            }
        }
        else {
            navForward.setOnClickListener {
                bookPageIndex += 1
                displayPage(bookPageIndex)
            }
            navBack.setOnClickListener {
                bookPageIndex -= 1
                displayPage(bookPageIndex)
            }
        }
        pageIndexDescription.text = "Page Number: ${bookPageIndex+1}"
        return navigationBar
    }

    private fun populatePages(oid: String) {
        try {
            val urlBuilder: HttpUrl.Builder =
                itemUrl.toHttpUrlOrNull()!!
                    .newBuilder()
            urlBuilder.addQueryParameter("oid", oid)
            urlBuilder.addQueryParameter("contentType", "book")
            val request: Request = Request.Builder().url(urlBuilder.build())
                .header("participantId", Constants.participantId).build()
            val response: Response = client.newCall(request).execute()
            if (response.code == 200 && response.body != null) {
                val responseBody = response.body!!.bytes()
                val responseDecompressed = gzipHelper.unzip(responseBody)
                val book = gson.fromJson(responseDecompressed, Book::class.java)
                for (page in book.text.drop(1)) {
                    bookPages?.add(page)
                }
            }
        }
        catch (e: Exception) {
            Log.e("API", "could not fetch books: $e")
            if (activity!=null) {
                requireActivity().runOnUiThread {
                    Toast.makeText(
                        requireContext(),
                        "Could not fetch book pages",
                        Toast.LENGTH_SHORT
                    ).show()
                }
            }
        }
    }

    private fun enhanceTextSelection(textView: TextView) {
        textView.customSelectionActionModeCallback = object: ActionMode.Callback {
            override fun onPrepareActionMode(mode: ActionMode?, menu: Menu): Boolean {
                // id of 'Share'
                menu.removeItem(android.R.id.shareText)
                menu.removeItem(android.R.id.textAssist)
                return true
            }

            override fun onCreateActionMode(mode: ActionMode?, menu: Menu): Boolean {
                return true
            }

            override fun onDestroyActionMode(mode: ActionMode?) {
            }

            override fun onActionItemClicked(mode: ActionMode, item: MenuItem): Boolean {
                return true
            }
        }
    }
}