package com.lmu.contentbrowser.data

data class DictionaryData(
    val lang: String,
    val hits: Array<HitData>
)

data class HitData(
    val type: String,
    val primary_entry: HitData?,
    val opendict: Boolean,
    val roms: Array<RomData>
)

data class RomData(
    val headword: String,
    val headword_full: String,
    val wordclass: String?,
    val arabs: Array<ArabData>
)

data class ArabData (
    val header: String,
    val translations: Array<DictTranslationData>
)

data class DictTranslationData (
    val source: String,
    val target: String
)