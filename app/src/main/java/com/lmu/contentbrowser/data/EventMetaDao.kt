package com.lmu.contentbrowser.data

import androidx.room.*
import androidx.sqlite.db.SimpleSQLiteQuery
import kotlinx.coroutines.flow.Flow

@Dao
interface EventMetaDao {
    @Insert(onConflict = OnConflictStrategy.IGNORE)
    suspend fun insert(eventMeta: EventMeta): Long

    @RawQuery(observedEntities = arrayOf(Event::class))
    fun getEventMetasForQueryOnce(query: SimpleSQLiteQuery): List<EventMeta>

    @Query("SELECT * FROM events_meta")
    fun getAllEventMetas(): Flow<List<EventMeta>>

    @Query("SELECT * FROM events_meta")
    fun getAllEventMetasOnce(): List<EventMeta>

    @Query("DELETE FROM events_meta WHERE event_id = :eventId")
    suspend fun deleteForEventId(eventId: Long)

    @Query("DELETE FROM events_meta")
    suspend fun deleteAll()
}