package com.lmu.contentbrowser.fragments

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.cardview.widget.CardView
import androidx.fragment.app.Fragment
import com.google.android.gms.oss.licenses.OssLicensesMenuActivity
import com.lmu.contentbrowser.R
import com.lmu.contentbrowser.activities.*

class ToolsFragment: Fragment() {
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_tools, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val filterContentButton = view.findViewById<CardView>(R.id.filterContentButton)
        val myWordsButton = view.findViewById<CardView>(R.id.myWordsButton)
        val notificationsButton = view.findViewById<CardView>(R.id.notificationsButton)
        val flashcardButton = view.findViewById<CardView>(R.id.flashcardButton)
        val userManagementButton = view.findViewById<CardView>(R.id.userManagementButton)
        val dsgvoButton = view.findViewById<CardView>(R.id.dsgvoButton)
        val publisherButton = view.findViewById<CardView>(R.id.publisherButton)
        val ossLicensesButton = view.findViewById<CardView>(R.id.ossLicensesButton)

        filterContentButton.setOnClickListener {
            val intent = Intent(activity, FilterContentActivity::class.java)
            startActivity(intent)
        }
        myWordsButton.setOnClickListener {
            val intent = Intent(activity, PhrasesActivity::class.java)
            startActivity(intent)
        }
        notificationsButton.setOnClickListener {
            val intent = Intent(activity, NotificationManagerActivity::class.java)
            startActivity(intent)
        }
        flashcardButton.setOnClickListener {
            val intent = Intent(activity, FlashCardActivity::class.java)
            startActivity(intent)
        }
        userManagementButton.setOnClickListener {
            val intent = Intent(activity, UserManagementActivity::class.java)
            startActivity(intent)
        }
        dsgvoButton.setOnClickListener {
            val intent = Intent(activity, DsgvoActivity::class.java)
            startActivity(intent)
        }
        publisherButton.setOnClickListener {
            val intent = Intent(activity, PublisherActivity::class.java)
            startActivity(intent)
        }
        ossLicensesButton.setOnClickListener {
            startActivity(Intent(activity, OssLicensesMenuActivity::class.java))
        }
    }
}