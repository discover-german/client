package com.lmu.contentbrowser.dialogs

import android.app.Dialog
import android.content.Context
import android.content.DialogInterface
import android.os.Bundle
import androidx.fragment.app.DialogFragment
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.lmu.contentbrowser.R
import com.lmu.contentbrowser.activities.MainActivity
import com.lmu.contentbrowser.fragments.HomeFragment

class EmailReminderDialog(val mContext: Context): DialogFragment() {
    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        return activity?.let {
            val builder = MaterialAlertDialogBuilder(mContext)
            // Get the layout inflater
            val inflater = requireActivity().layoutInflater
            val dialogView = inflater.inflate(R.layout.layout_email_reminder_dialog, null)
            builder.setView(dialogView)
                .setPositiveButton("OK",
                    DialogInterface.OnClickListener { dialog, id ->
                        val sharedPref = mContext.getSharedPreferences(getString(R.string.preference_file_key),
                            Context.MODE_PRIVATE)
                        with (sharedPref.edit()) {
                            putString(getString(R.string.email_reminder_shown), "true")
                            apply()
                        }
                    })
            builder.create()
        } ?: throw IllegalStateException("Activity cannot be null")
    }
}