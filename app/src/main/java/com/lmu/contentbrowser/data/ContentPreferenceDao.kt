package com.lmu.contentbrowser.data

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query

@Dao
interface ContentPreferenceDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(contentPreference: ContentPreference): Long

    @Query("SELECT * FROM contentPreferences")
    fun getAllItemsAsync(): List<ContentPreference>

    @Query("SELECT * FROM contentPreferences WHERE `key` = :key")
    fun getItemByKeyAsync(key: String): ContentPreference?

    @Query("DELETE FROM contentPreferences WHERE `key` = :key")
    suspend fun deleteItem(key: String)

    @Query("DELETE FROM contentPreferences")
    suspend fun deleteAll()
}