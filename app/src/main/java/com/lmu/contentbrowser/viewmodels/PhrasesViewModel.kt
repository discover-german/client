package com.lmu.contentbrowser.viewmodels

import androidx.lifecycle.*
import com.lmu.contentbrowser.data.Phrase
import com.lmu.contentbrowser.database.MyRepository
import kotlinx.coroutines.launch

class PhrasesViewModel (private val repository: MyRepository) : ViewModel() {

    val allPhrases: LiveData<List<Phrase>> = repository.allPhrases.asLiveData()

    fun insertPhrase(phrase: Phrase) = viewModelScope.launch {
        repository.insertPhrase(phrase)
    }

    fun deletePhrase(id: Long) = viewModelScope.launch {
        repository.deletePhrase(id)
    }

    suspend fun getRandomPhrase(): Phrase? {
        return repository.getRandomPhrase()
    }
}

class PhrasesViewModelFactory(private val repository: MyRepository) : ViewModelProvider.Factory {
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(PhrasesViewModel::class.java)) {
            @Suppress("UNCHECKED_CAST")
            return PhrasesViewModel(repository) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }
}