package com.lmu.contentbrowser.adapters

import android.app.Activity
import android.content.Intent
import android.graphics.drawable.GradientDrawable
import com.squareup.picasso.Picasso
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.FrameLayout
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.content.res.AppCompatResources
import androidx.cardview.widget.CardView
import androidx.core.content.ContextCompat
import com.lmu.contentbrowser.R
import com.lmu.contentbrowser.activities.PublisherActivity
import com.lmu.contentbrowser.activities.StatsActivity
import com.lmu.contentbrowser.callbacks.ImageCallback
import com.lmu.contentbrowser.dialogs.DismissContentItemDialog
import kotlin.random.Random

class ContentAdapter(private val context: Activity, private val entries: List<String>):
    ArrayAdapter<String>(context, R.layout.layout_content_item, entries) {

    override fun getView(position: Int, view: View?, parent: ViewGroup): View {
        val inflater = context.layoutInflater
        val rowView = inflater.inflate(R.layout.layout_content_item, parent, false)

        val entry: String = entries[position]
        if (entry.split("|").size >= 6) {
            val contentType = entry.split("|")[0]
            val title = entry.split("|")[1]
            var description = entry.split("|")[2]
            val oid = entry.split("|")[3]
            val triggerTitle = entry.split("|")[4]
            val imageLink: String = entry.split("|")[5]

            val episodeIndex: String
            episodeIndex = if (contentType == "podcast" && entry.split("|").size >= 7) {
                entry.split("|")[6]
            } else {
                ""
            }

            val titleView: TextView = rowView.findViewById(R.id.entryTitle)
            val descriptionView: TextView = rowView.findViewById(R.id.entryDescription)
            val frameLayout: FrameLayout = rowView.findViewById(R.id.entryFrame)
            val contentTypeView: TextView = rowView.findViewById(R.id.contentType)
            val statsButtonView: CardView = rowView.findViewById(R.id.statsButton)
            val dismissItemButton:CardView = rowView.findViewById(R.id.dismissItemButton)
            val contactButton: CardView = rowView.findViewById(R.id.contactButton)

            when(contentType) {
                "book" -> description = description.replace("#", "")
                "error" -> statsButtonView.visibility = View.INVISIBLE
            }

            titleView.text = title
            descriptionView.text = description
            contentTypeView.text = contentType

            when(contentType) {
                "article" -> setGradientDrawable(frameLayout, R.drawable.article_gradient)
                "book" -> setGradientDrawable(frameLayout, R.drawable.book_gradient)
                "podcast" -> setGradientDrawable(frameLayout, R.drawable.podcast_gradient)
                "error" -> setGradientDrawable(frameLayout, R.drawable.error_gradient)
                else -> false
            }

            val imageView: ImageView = rowView.findViewById(R.id.entryImage)
            if (imageLink != "" && imageLink != "null") {
                Picasso.get().load(imageLink).resize(1000, 0)
                    .onlyScaleDown().into(imageView, ImageCallback(imageView))
            }
            else {
                imageView.scaleType = ImageView.ScaleType.FIT_CENTER
                when(contentType) {
                    "article" -> imageView.setImageResource(R.drawable.baseline_newspaper_black_48dp)
                    "book" -> imageView.setImageResource(R.drawable.baseline_menu_book_black_48dp)
                    "podcast" -> imageView.setImageResource(R.drawable.baseline_podcasts_black_48dp)
                    "error" -> imageView.setImageResource(R.drawable.baseline_error_outline_black_24dp)
                    else -> false
                }
            }

            if (contentType == "article" && entry.split("|").size >= 8) {
                contactButton.visibility = View.VISIBLE
                val author = entry.split("|")[6]
                val url = entry.split("|")[7]
                contactButton.setOnClickListener {
                    val intent = Intent(context, PublisherActivity::class.java)
                    intent.putExtra("author", author)
                    intent.putExtra("url", url)
                    context.startActivity(intent)
                }
            }

            statsButtonView.setOnClickListener {
                val intent = Intent(context, StatsActivity::class.java)
                intent.putExtra("oid", oid)
                intent.putExtra("contentType", contentType)
                intent.putExtra("triggerTitle", triggerTitle)
                intent.putExtra("episodeIndex", episodeIndex)
                context.startActivity(intent)
            }

            if (contentType == "article" || contentType == "book" || contentType == "podcast") {
                dismissItemButton.visibility = View.VISIBLE
            }

            dismissItemButton.setOnClickListener {
                val dismissContentItemDialog = DismissContentItemDialog(context, this,
                    entries, position, oid, contentType,
                    episodeIndex)
                dismissContentItemDialog.show(
                    (context as AppCompatActivity).supportFragmentManager,
                    "dismiss item")
            }
        }

        return rowView
    }

    private fun setColor(myView: View, color: Int) {
        context.runOnUiThread {
            myView.setBackgroundColor(ContextCompat.getColor(context,(color)))
        }
    }

    private fun setGradientDrawable(myView: View, drawable: Int) {
        context.runOnUiThread {
            val bg = AppCompatResources.getDrawable(context, drawable) as GradientDrawable
            val random = Random(System.nanoTime())
            val xVal = random.nextDouble(0.3, 0.7).toFloat()
            val yVal = random.nextDouble(0.3, 0.7).toFloat()
            bg.setGradientCenter(xVal, yVal)
            myView.background = bg
        }
    }
}