package com.lmu.contentbrowser.services

import android.app.PendingIntent
import android.app.Service
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Canvas
import android.net.Uri
import android.os.Binder
import android.os.IBinder
import android.util.Log
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.core.app.NotificationCompat.PRIORITY_HIGH
import com.google.android.exoplayer2.ExoPlayer
import com.google.android.exoplayer2.MediaItem
import com.google.android.exoplayer2.Player
import com.google.android.exoplayer2.source.DefaultMediaSourceFactory
import com.google.android.exoplayer2.ui.PlayerNotificationManager
import com.google.android.exoplayer2.ui.StyledPlayerControlView
import com.google.android.exoplayer2.upstream.DefaultDataSource
import com.google.android.exoplayer2.upstream.DefaultHttpDataSource
import com.google.android.exoplayer2.upstream.HttpDataSource
import com.lmu.contentbrowser.ContentBrowserApplication
import com.lmu.contentbrowser.R
import com.lmu.contentbrowser.activities.PodcastActivity
import com.lmu.contentbrowser.data.LogEvent
import com.lmu.contentbrowser.data.LogEventType
import com.lmu.contentbrowser.data.Song
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch


class MusicService: Service(), PlayerNotificationManager.MediaDescriptionAdapter, PlayerNotificationManager.NotificationListener {
    private val musicBinder = MusicBinder()
    private lateinit var playerNotificationManager: PlayerNotificationManager
    private val CHANNEL_ID = "MusicPlayer"
    private var podcastThumbnail: ImageView? = null

    inner class MusicBinder: Binder() {
        fun getService() : MusicService {
            return this@MusicService
        }
    }

    override fun onBind(intent: Intent): IBinder {
        return musicBinder
    }

    override fun onCreate() {
        super.onCreate()

        if (player == null) {
            val httpDataSourceFactory: HttpDataSource.Factory =
                DefaultHttpDataSource.Factory().setAllowCrossProtocolRedirects(true)
            val dataSourceFactory = DefaultDataSource.Factory(this, httpDataSourceFactory)
            player = ExoPlayer.Builder(applicationContext)
                .setMediaSourceFactory(DefaultMediaSourceFactory(dataSourceFactory)).build()
            if (player != null) {
                player!!.addListener(LogEventHandler())
            }
        }
        playerNotificationManager = PlayerNotificationManager
            .Builder(applicationContext, R.string.podcast_notification_id, CHANNEL_ID)
            .setChannelNameResourceId(R.string.notification_channel_name)
            .setChannelDescriptionResourceId(R.string.notification_channel_description)
            .setMediaDescriptionAdapter(this)
            .setNotificationListener(this).build()
        playerNotificationManager.setPriority(PRIORITY_HIGH)
        playerNotificationManager.setPlayer(player)
    }

    fun setPlayerControls(playerControls: StyledPlayerControlView) {
        playerControls.player = player
    }

    fun setPodcastThumbnail(imageView: ImageView){
        podcastThumbnail = imageView
    }

    fun playSong(song: Song) {
        if (player != null) {
            currentSong = song
            if (player!!.isPlaying) {
                player!!.stop()
            }
            val mediaItem = MediaItem.fromUri(Uri.parse(song.url))
            player!!.setMediaItem(mediaItem)
            player!!.prepare()

            player!!.playWhenReady = true
        }
    }

    fun isPlaying(): Boolean {
        return if (player != null) {
            player!!.isPlaying
        } else {
            false
        }
    }

    fun pause() {
        if (player != null && player!!.isPlaying) {
            player!!.pause()
        }
    }

    fun getCurrentTitle(): String {
        return if (currentSong!=null) {
            currentSong!!.title
        } else {
            ""
        }
    }

    override fun getCurrentContentTitle(player: Player): CharSequence {
        return getCurrentTitle()
    }

    override fun createCurrentContentIntent(player: Player): PendingIntent? {
        val intent = Intent(applicationContext, PodcastActivity::class.java)
        if (currentSong!=null) {
            intent.putExtra("url", currentSong?.url)
            intent.putExtra("item", "podcast|${currentSong!!.title}" +
                    "|${currentSong!!.description}|${currentSong!!.oid}" +
                    "|null|${currentSong!!.imageUrl}")
        }
        return PendingIntent.getActivity(applicationContext, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT or PendingIntent.FLAG_IMMUTABLE)
        return null
    }

    override fun getCurrentContentText(player: Player): CharSequence? {
        return if(currentSong!=null) {
            currentSong!!.description
        } else {
            ""
        }
    }

    override fun getCurrentLargeIcon(
        player: Player,
        callback: PlayerNotificationManager.BitmapCallback
    ): Bitmap {
        return return if (podcastThumbnail != null) {
            getBitmapFromView(podcastThumbnail!!)
        } else {
            BitmapFactory.decodeResource(applicationContext.resources,
                R.drawable.headphones)
        }
    }

    fun getBitmapFromView(view: View): Bitmap {
        try {
            val bitmap = Bitmap.createBitmap(view.width, view.height, Bitmap.Config.ARGB_8888)
            val canvas = Canvas(bitmap)
            view.draw(canvas)
            return bitmap
        }
        catch (e: Exception) {
            Log.e("Bitmap", e.toString())
        }
        return BitmapFactory.decodeResource(applicationContext.resources,
            R.drawable.headphones)
    }

    private inner class LogEventHandler: Player.Listener {
        override fun onIsPlayingChanged(isPlaying: Boolean) {
            super.onIsPlayingChanged(isPlaying)
            if (application is ContentBrowserApplication && currentSong != null) {
                val logEventType = if (isPlaying) {
                    LogEventType.PODCAST_SESSION_START
                }
                else {
                    LogEventType.PODCAST_SESSION_END
                }
                val currentTime = System.currentTimeMillis()
                val logEvent = LogEvent(0, logEventType, currentTime, currentSong!!.oid, false)
                CoroutineScope(Dispatchers.IO).launch {
                    (application as ContentBrowserApplication).repository.insertLogEvent(logEvent)
                }
            }
        }
    }

    companion object {
        var player: ExoPlayer? = null
        var currentSong: Song? = null
    }
}