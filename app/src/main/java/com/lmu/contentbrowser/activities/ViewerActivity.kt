package com.lmu.contentbrowser.activities

import android.R.attr.left
import android.R.attr.right
import android.content.Context
import android.content.Intent
import android.content.res.Resources
import android.os.Bundle
import android.os.Handler
import android.text.Html
import android.util.Log
import android.util.TypedValue
import android.view.*
import android.webkit.WebView
import android.widget.*
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.LinearLayoutCompat
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.ContextCompat
import androidx.core.text.HtmlCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.lmu.contentbrowser.Constants
import com.lmu.contentbrowser.ContentBrowserApplication
import com.lmu.contentbrowser.R
import com.lmu.contentbrowser.data.*
import com.lmu.contentbrowser.fragments.*
import com.lmu.contentbrowser.helpers.GzipHelper
import com.lmu.contentbrowser.viewmodels.*
import kotlinx.coroutines.launch
import okhttp3.*
import okhttp3.HttpUrl.Companion.toHttpUrl
import okhttp3.MediaType.Companion.toMediaType
import okhttp3.RequestBody.Companion.toRequestBody
import java.io.IOException
import java.lang.reflect.Type
import java.net.SocketTimeoutException
import java.util.*
import java.util.concurrent.TimeUnit
import kotlin.concurrent.thread


class ViewerActivity: AppCompatActivity() {
    private val registerInterestUrl = Constants.SERVER_URL+"/register_interest"
    private val getItemUrl = Constants.SERVER_URL + "/item"
    private lateinit var webView: WebView
    private var mActionMode: ActionMode? = null
    private val DICTIONARY = 0
    private val client = OkHttpClient.Builder().connectTimeout(Constants.CONNECT_TIMEOUT, TimeUnit.SECONDS).build()
    private val gson = Gson()
    private val gzipHelper = GzipHelper()
    private lateinit var handler: Handler
    private lateinit var selectedFragment: Fragment
    private val webViewFragment = WebViewFragment()
    private var favoriteId: Long? = null
    var itemOid: String = ""
    var itemContentType: String = ""
    var itemTitle: String = ""
    var itemDescription: String = ""
    var itemImageLink: String = ""
    var itemUrl: String = ""
    var itemAuthor: String = ""

    val phrasesViewModel: PhrasesViewModel by viewModels {
        PhrasesViewModelFactory((application as ContentBrowserApplication).repository)
    }

    val favoriteViewModel: FavoritesViewModel by viewModels {
        FavoritesViewModelFactory((application as ContentBrowserApplication).repository)
    }

    val historyViewModel: HistoryViewModel by viewModels {
        HistoryViewModelFactory((application as ContentBrowserApplication).repository)
    }

    val logEventViewModel: LogEventViewModel by viewModels {
        LogEventViewModelFactory((application as ContentBrowserApplication).repository)
    }

    val contentPreferenceModel: ContentPreferenceViewModel by viewModels {
        ContentPreferenceViewModelFactory((application as ContentBrowserApplication).repository)
    }

    val whitelistModel: WhitelistViewModel by viewModels {
        WhitelistViewModelFactory((application as ContentBrowserApplication).repository)
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_viewer)

        Log.i("viewer", "session started")

        if (Constants.participantId == "") {
            initializePassword()
        }

        handler = Handler(mainLooper)

        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        val b = intent.extras
        val url = b?.getString("url") ?: ""
        if (url != "") {
            itemUrl = url
        }
        val recommend = b?.getString("recommend") ?: ""
        val triggeredByNotification = b?.getBoolean("notification") ?: false
        val item = b?.getString("item")
        if (item != null && item.split("|").size >= 6) {
            initValuesWithItem(item, triggeredByNotification)
        }
        else if (recommend != "") {
            itemContentType = recommend
            Thread{
                val recommendedItem = getItemFromRecommendation(recommend)
                runOnUiThread {
                    initValuesWithItem(recommendedItem, triggeredByNotification)
                }
            }.start()
        }
        else {
            return
        }

        handler.postDelayed({registerInterest(itemOid, itemContentType)}, (15*1000).toLong())
    }

    private fun initValuesWithItem(item: String, triggeredByNotification: Boolean){
        if (item.split("|").size >= 6)
        itemContentType = item.split("|")[0]
        itemTitle = item.split("|")[1]
        itemDescription = item.split("|")[2]
        itemOid = item.split("|")[3]
        itemImageLink = item.split("|")[5]

        var author = if(itemContentType == "article" && item.split("|").size >= 7) {
            item.split("|")[6]
        } else {
            ""
        }

        itemAuthor = author

        invalidateOptionsMenu()
        supportActionBar?.title = itemTitle

        selectedFragment = webViewFragment

        val currentTime = System.currentTimeMillis()//Constants.simpleDateFormat.format(Date())

        lifecycleScope.launch {
            historyViewModel.insertHistoryItem(HistoryItem(0, itemOid,
                itemContentType, itemTitle, itemDescription, itemImageLink, itemUrl, currentTime))
        }

        if (triggeredByNotification) {
            val notificationLogEventType = when(itemContentType) {
                "article" -> LogEventType.ARTICLE_NOTIFICATION_CLICK
                "book" -> LogEventType.BOOK_NOTIFICATION_CLICK
                else -> LogEventType.NONE
            }
            logEventViewModel.insertEvent(
                LogEvent(0, notificationLogEventType,
                    currentTime, null, false)
            )
        }
        setupBottomNavBar()
    }

    private fun setupBottomNavBar() {
        val bottomNavigationView: BottomNavigationView = findViewById(R.id.bottomNavigationView)
        bottomNavigationView.setOnItemSelectedListener { menuItem ->
            var tag = ""
            lateinit var newFragment: Fragment
            val clickTime = System.currentTimeMillis()
            when(menuItem.itemId) {
                R.id.original ->  {
                    newFragment = WebViewFragment()
                    tag = "webviewFragment"
                    logEventViewModel.insertEvent(LogEvent(0,
                        LogEventType.WEBVIEW_READER_CLICK, clickTime, itemOid, false))
                }
                R.id.cleaned -> {
                    newFragment = CleanedViewFragment()
                    tag = "cleanedviewFragment"
                    logEventViewModel.insertEvent(LogEvent(0,
                        LogEventType.SIMPLE_READER_CLICK, clickTime, itemOid, false))
                }
                R.id.backToBack -> {
                    newFragment = TranslatedViewFragment()
                    tag = "translatedviewFragment"
                    logEventViewModel.insertEvent(
                        LogEvent(0,
                            LogEventType.PARAGRAPH_READER_CLICK, clickTime, itemOid, false))
                }
                else -> false
            }
            if (selectedFragment.tag != tag) {
                selectedFragment = newFragment
            }
            val transaction = supportFragmentManager.beginTransaction()
            transaction.replace(R.id.fragmentContainerView, selectedFragment, tag)
            transaction.commit()
            return@setOnItemSelectedListener true
        }
        bottomNavigationView.selectedItemId = R.id.original
    }

    private fun getItemFromRecommendation(eventType: String): String {
        var item = ""
        val response = getItemResponse(eventType)
        if (response != null && response.isSuccessful && response.body != null) {
            val responseBody = response.body!!.bytes()
            response.body!!.close()
            val responseDecompressed = gzipHelper.unzip(responseBody)
            when(eventType) {
                "article" -> {
                    val articles = gson.fromJson(responseDecompressed, Array<Article>::class.java)
                    val article = articles[0]
                    itemUrl = article.link
                    val articleTitle = article.title.replace("|", ",")
                    val articleDescription = article.body.replace("|", ",")
                    item = "article|${articleTitle}|${articleDescription}|" +
                        article._id.oid + "||${article.image_link}|${article.author}"
                }
                "book" -> {
                    val books = gson.fromJson(responseDecompressed, Array<Book>::class.java)
                    val book = books[0]
                    if (book.text.isNotEmpty()) {
                        val bookTitle = book.title.replace("|", ",")
                        itemUrl =  book.link
                        val bookDescription = book.text[0]
                        item =  "book|${bookTitle}|${bookDescription}|" +
                                    book._id.oid + "||${book.image_link}"
                    }
                }
                else -> {
                    item = ""
                }
            }
        }
        else {
            item = ""
        }
        return item
    }

    private fun getItemResponse(eventType: String): Response? {
        return try {
            val urlBuilder: HttpUrl.Builder = getItemUrl.toHttpUrl().newBuilder()
            urlBuilder.addQueryParameter("contentType", eventType)
            urlBuilder.addQueryParameter("oid", "")
            val whitelistItems = whitelistModel.getAllWhitelistItemsAsync()
            var blacklistJson = ""
            for (item in whitelistItems) {
                if (!item.allowed) {
                    blacklistJson += "\"${item.source.name}\","
                }
            }
            blacklistJson = removeSuperfluousComma(blacklistJson)
            urlBuilder.addQueryParameter("blacklist", "[$blacklistJson]")
            val difficulty =
                contentPreferenceModel.getContentPreferenceByKeyAsync("difficulty")
            if (difficulty != null) {
                urlBuilder.addQueryParameter("difficulty", difficulty.value.toString())
            }
            else {
                urlBuilder.addQueryParameter("difficulty", "2")
            }
            val request: Request = Request.Builder().url(urlBuilder.build())
                .header("participantId", Constants.participantId).build()
            val response: Response = client.newCall(request).execute()
            response
        } catch (e: SocketTimeoutException) {
            Log.e("notification", e.toString())
            null
        }
    }

    private fun removeSuperfluousComma(itemJson: String): String {
        val cleanedItems = arrayListOf<String>()
        val items = itemJson.split(",")
        for (item in items) {
            if (item == "") {
                continue
            }
            cleanedItems.add(item)
        }
        return cleanedItems.joinToString(",")
    }

    private fun initializePassword() {

        val sharedPref = this.getSharedPreferences(getString(R.string.preference_file_key),
            Context.MODE_PRIVATE)
        if (sharedPref.contains(getString(R.string.participant_id_key))) {
            Log.i("participant_id", "Successfully retrieved participant id")
            Constants.participantId =
                sharedPref.getString(getString(R.string.participant_id_key), "") ?: ""
        } else {
            Log.e("preferences", "no password found")
            Toast.makeText(this, "No user ID found :(", Toast.LENGTH_SHORT).show()
        }
    }

    override fun onResume() {
        super.onResume()
        val currentTime = System.currentTimeMillis()
        val logEventType = if (itemContentType == "article") {
            LogEventType.ARTICLE_SESSION_START
        } else if (itemContentType == "book") {
            LogEventType.BOOK_SESSION_START
        } else {
            LogEventType.NONE
        }
        logEventViewModel.insertEvent(LogEvent(0, logEventType, currentTime, itemOid,
            false))
    }

    override fun onPause() {
        super.onPause()
        Log.i("viewer", "session ended")
        val currentTime = System.currentTimeMillis()
        val logEventType = if (itemContentType == "article") {
            LogEventType.ARTICLE_SESSION_END
        } else if (itemContentType == "book") {
            LogEventType.BOOK_SESSION_END
        } else {
            LogEventType.NONE
        }
        logEventViewModel.insertEvent(LogEvent(0, logEventType, currentTime, itemOid,
            false))
        handler.removeCallbacksAndMessages(null)
    }

    override fun onActionModeStarted(mode: ActionMode) {
        super.onActionModeStarted(mode)
        if (mActionMode == null) {
            mActionMode = mode
            val menu: Menu = mode.getMenu()
            // Remove the default menu items (select all, copy, paste, search)
            //menu.clear()

            menu.add(0, DICTIONARY, 0, "Dictionary / Translate")
                .setIcon(R.drawable.baseline_menu_book_black_48dp)
                .setOnMenuItemClickListener {
                    val logEventType = LogEventType.TRANSLATOR_CLICK
                    val currentTime = System.currentTimeMillis()
                    logEventViewModel.insertEvent(LogEvent(0, logEventType, currentTime, itemOid,
                        false))
                    if (selectedFragment.tag == "webviewFragment") {
                        webView = findViewById<WebView>(R.id.webview)
                        webView.evaluateJavascript(
                            "(function(){" +
                                    "return window.getSelection().toString()})()"
                        )
                        { value ->
                            if (value.length < 2000) {
                                showDictionaryPopup(value, webView)
                            } else {
                                Toast.makeText(this,
                                    "The selection is too long. Max length: 2000 characters",
                                    Toast.LENGTH_LONG).show()
                            }
                        }
                    }
                    else if (selectedFragment.tag == "cleanedviewFragment") {
                        val textContainer =
                            selectedFragment.view?.findViewById<LinearLayoutCompat>(R.id.textContainer)
                        if (textContainer!=null) {
                            val selectedText = getSelectedTextFromFragment(textContainer)
                            if (selectedText.length < 2000) {
                                showDictionaryPopup(selectedText, textContainer)
                            }
                            else {
                                Toast.makeText(this,
                                    "The selection is too long. Max length: 2000 characters",
                                    Toast.LENGTH_LONG).show()
                            }
                        }
                    }
                    true
                }
            // id for 'Share'
            menu.removeItem(34275980)
            menu.removeItem(android.R.id.shareText)
            menu.removeItem(android.R.id.textAssist)

            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.S) {
                mode.invalidate()
            }
            // Inflate your own menu items
            //mode.getMenuInflater().inflate(R.menu.my_custom_menu, menu)
        }
    }

    override fun onActionModeFinished(mode: ActionMode) {
        super.onActionModeFinished(mode)
        mActionMode = null
    }

    private fun showDictionaryPopup(selectedText: String, parentView: View) {
        thread {
            val phrasesToSave = arrayListOf<Phrase>()
            val dialogPopup: View = layoutInflater.inflate(R.layout.popup_dictionary, null)
            val titleView = dialogPopup.findViewById<TextView>(R.id.dictPopupTitle)
            val textContainer = dialogPopup.findViewById<LinearLayoutCompat>(R.id.dictPopupTextContainer)
            val saveButton = dialogPopup.findViewById<Button>(R.id.saveWordParagraph)
            var popupHeader: String
            var processedText: String
            if (selectedText.trim().split(" ").size > 1){
                popupHeader = "Automatic Translation:"
                val translation = fetchTranslation(selectedText)
                if (translation != null) {
                    processedText = translation.translatedText
                    saveButton.text = resources.getString(R.string.save_to_my_paragraphs_list)
                    phrasesToSave.add(Phrase(0, selectedText, selectedText,
                        translation.translatedText, null, null, null))
                }
                else {
                    processedText = "No Translation Found :("
                    runOnUiThread {
                        saveButton.isEnabled = false
                    }
                }
                val paragraphContainer = layoutInflater.inflate(R.layout.layout_paragraph_translation,
                    textContainer, false) as ConstraintLayout
                val originalText = paragraphContainer.findViewById<TextView>(R.id.original)
                originalText.visibility = View.VISIBLE
                originalText.text = selectedText.trim()
                val textView = paragraphContainer.findViewById<TextView>(R.id.phrase)
                textView.text = processedText
                textContainer.addView(paragraphContainer)
            }
            else {
                runOnUiThread {
                    saveButton.isEnabled = false
                }
                popupHeader = "Dictionary Entry:"
                val dictionaryResults = fetchDictionaryData(selectedText.trim(), "dedx", "de")
                if (dictionaryResults != null) {
                    val dictionaryHits: Array<HitData> = dictionaryResults.hits
                    for (dictHit in dictionaryHits.take(3)) {
                        if (dictHit.roms == null) {
                            continue
                        }
                        for (rom in dictHit.roms) {
                            val syllables = rom.headword
                            val cleanedText = syllables.replace("[^A-Za-zÀ-ſ]".toRegex(), "")
                            val wordClass = rom.wordclass
                            val examples = arrayListOf<String>()
                            processedText = "${cleanedText}, Type:  ${wordClass}"
                            val wordCandidate = layoutInflater.inflate(R.layout.layout_word_candidate,
                                textContainer, false) as ConstraintLayout
                            val phraseView = wordCandidate.findViewById<TextView>(R.id.phrase)
                            phraseView.text = processedText
                            val syllablesView = wordCandidate.findViewById<TextView>(R.id.syllables)
                            syllablesView.text = syllables
                            val examplesContainer = wordCandidate.findViewById<LinearLayoutCompat>(R.id.examplesContainer)
                            for (arabs in rom.arabs) {
                                for (translation in arabs.translations) {
                                    val example = createExampleView(translation)
                                    examples.add(example.text.toString())
                                    examplesContainer.addView(example)
                                }
                            }
                            var translation = ""
                            val translationView =
                                wordCandidate.findViewById<TextView>(R.id.wordTranslation)
                            val translationResult =
                                fetchTranslation(rom.headword.replace("[^A-Za-zÀ-ſ]".toRegex(), ""))
                            if (translationResult != null) {
                                translationView.text = translationResult.translatedText
                                translation = translationResult.translatedText
                            }
                            else {
                                translationView.visibility = View.INVISIBLE
                            }
                            val checkBox = wordCandidate.findViewById<CheckBox>(R.id.phraseCheckbox)
                            checkBox.setOnClickListener {
                                val phrase = Phrase(0, selectedText, cleanedText,
                                    translation, syllables, wordClass, examples)
                                runOnUiThread {
                                    it as CheckBox
                                    if (it.isChecked) {
                                        phrasesToSave.add(phrase)
                                    }
                                    else {
                                        var deleteIndex = 0
                                        var found = false
                                        for (phrase in phrasesToSave) {
                                            if (phrase.phrase == cleanedText) {
                                                found = true
                                                break
                                            }
                                            deleteIndex += 1
                                        }
                                        if (found) {
                                            phrasesToSave.removeAt(deleteIndex)
                                        }
                                    }
                                    saveButton.isEnabled = phrasesToSave.size > 0 || it.isChecked
                                }
                            }
                            wordCandidate.layoutParams = wordCandidate.layoutParams.apply {
                                width = LinearLayout.LayoutParams.MATCH_PARENT
                                height = LinearLayout.LayoutParams.WRAP_CONTENT
                            }
                            textContainer.layoutParams = textContainer.layoutParams.apply {
                                width = LinearLayout.LayoutParams.MATCH_PARENT
                                height = LinearLayout.LayoutParams.WRAP_CONTENT
                            }
                            textContainer.addView(wordCandidate)
                        }
                    }
                    saveButton.text = resources.getString(R.string.save_to_my_words_list)
                }
                else {
                    val translation =
                        fetchTranslation(selectedText.replace("[^A-Za-zÀ-ſ]".toRegex(), ""))
                    if (translation != null) {
                        popupHeader = "No match found, automatic translation:"
                        processedText = translation.translatedText
                    }
                    else {
                        popupHeader = "No match found"
                        processedText = ""
                    }
                    val wordCandidate = layoutInflater.inflate(R.layout.layout_word_candidate,
                        textContainer, false) as ConstraintLayout
                    val textView = wordCandidate.findViewById<TextView>(R.id.phrase)
                    textView.text = selectedText
                    val translationView = wordCandidate.findViewById<TextView>(R.id.wordTranslation)
                    translationView.text = processedText
                    val syllables = wordCandidate.findViewById<TextView>(R.id.syllables)
                    syllables.visibility = View.GONE
                    val explanations = wordCandidate.findViewById<TextView>(R.id.examplesHeader)
                    explanations.visibility = View.GONE
                    val checkBox = wordCandidate.findViewById<CheckBox>(R.id.phraseCheckbox)
                    checkBox.setOnClickListener {
                        val phrase = Phrase(0, selectedText, selectedText, processedText,
                            null, null, null)
                        runOnUiThread {
                            it as CheckBox
                            if (it.isChecked) {
                                phrasesToSave.add(phrase)
                            }
                            else {
                                var deleteIndex = 0
                                var found = false
                                for (phrase in phrasesToSave) {
                                    if (phrase.phrase == selectedText) {
                                        found = true
                                        break
                                    }
                                    deleteIndex += 1
                                }
                                if (found) {
                                    phrasesToSave.removeAt(deleteIndex)
                                }
                            }
                            saveButton.isEnabled = phrasesToSave.size > 0 || it.isChecked
                        }
                    }
                    wordCandidate.layoutParams = wordCandidate.layoutParams.apply {
                        width = LinearLayout.LayoutParams.MATCH_PARENT
                        height = LinearLayout.LayoutParams.WRAP_CONTENT
                    }
                    textContainer.addView(wordCandidate)
                    textContainer.layoutParams = textContainer.layoutParams.apply {
                        width = LinearLayout.LayoutParams.MATCH_PARENT
                        height = LinearLayout.LayoutParams.WRAP_CONTENT
                    }
                }
            }
            val googleTranslateBar =
                layoutInflater.inflate(R.layout.layout_google_translate_bar,
                    textContainer, false)
            textContainer.addView(googleTranslateBar)

            val width = ViewGroup.LayoutParams.WRAP_CONTENT
            val height = ViewGroup.LayoutParams.WRAP_CONTENT
            val popupWindow = PopupWindow(dialogPopup, width, height, true)
            popupWindow.elevation = 40f

            saveButton.setOnClickListener {
                for (phrase in phrasesToSave) {
                    phrasesViewModel.insertPhrase(phrase)
                }
                val logEventType = LogEventType.ADD_WORD_OR_PHRASE
                val currentTime = System.currentTimeMillis()
                logEventViewModel.insertEvent(LogEvent(0, logEventType, currentTime,
                    null, false))
                Toast.makeText(this, "Added to Words and Phrases", Toast.LENGTH_SHORT).show()
                popupWindow.dismiss()
            }
            titleView.text = popupHeader
            runOnUiThread {
                if (!this.isDestroyed) {
                    popupWindow.showAtLocation(parentView, Gravity.CENTER, 0, 0)
                    popupWindow.dimBehind()
                }
            }
        }
    }

    private fun createExampleView(translation: DictTranslationData): TextView {
        val params = LinearLayout.LayoutParams(
            ViewGroup.LayoutParams.WRAP_CONTENT,
            ViewGroup.LayoutParams.MATCH_PARENT
        )
        params.setMargins(0, 40, 0, 40)
        val example = TextView(this)
        example.layoutParams = params
        example.foregroundTintMode
        example.text = translation.source.replace("<[^>]*>".toRegex(), "")
        return example
    }

    private fun getSelectedTextFromFragment(textContainer: LinearLayoutCompat?): String{
        if (textContainer != null) {
            val count = textContainer.childCount
            for (i in 0..count) {
                val v: View = textContainer.getChildAt(i) ;
                if (v is TextView && v.hasSelection()) {
                    var min = 0
                    var max: Int = v.getText().length
                    if (v.isFocused()) {
                        val selStart: Int = v.getSelectionStart()
                        val selEnd: Int = v.getSelectionEnd()
                        min = Math.max(0, Math.min(selStart, selEnd))
                        max = Math.max(0, Math.max(selStart, selEnd))
                    }
                    // Perform your definition lookup with the selected text
                    val selectedText: CharSequence =
                        v.getText().subSequence(min, max)
                    return selectedText.toString()
                }
            }
        }
        return ""
    }

    fun fetchTranslation(textInput: String, keepFormat:Boolean=false): Translation? {
        var input: String
        if (!keepFormat) {
            input = textInput.replace("[^a-zA-ZÀ-ſ]".toRegex(), " ")
            input = input.replace(" +".toRegex(), " ")
        }
        else {
            input = textInput.replace("[\"]".toRegex(), "“")
            input = input.replace("[^0-9a-zA-ZÀ-ſ .;:,'»«„“›‹‟”()</>\n-]".toRegex(), " ")
            input = input.replace(" +".toRegex(), " ")
        }
        val MEDIA_TYPE_MARKDOWN = "application/json; charset=utf-8".toMediaType()
        val postBody = "{ q: \"$input\", target: \"en\", format: \"html\" }"
        val request = Request.Builder()
            .url("https://translation.googleapis.com/language/translate/v2" +
                    "?key=${Constants.GoogleApiKey}")
            .post(postBody.toRequestBody(MEDIA_TYPE_MARKDOWN))
            .build()

        client.newCall(request).execute().use { response ->
            if (response.isSuccessful && response.body != null) {
                val translationResponse =
                    gson.fromJson(response.body!!.string(), TranslationResponse::class.java)
                response.body!!.close()
                return if (translationResponse.data.translations.size > 0){
                    translationResponse.data.translations[0].translatedText =
                        Html.fromHtml(translationResponse.data.translations[0].translatedText,
                            HtmlCompat.FROM_HTML_MODE_LEGACY).toString()
                    translationResponse.data.translations[0]
                } else {
                    null
                }
            }
            else {
                Log.e("Google Translate", "Error while translating: ${response}")
            }
        }
        return null
    }

    private fun fetchDictionaryData(word: String, dictionary: String, inputLang: String): DictionaryData? {
        /***
         * Warning: this function has to be called in a background thread!!!
         */
        var dictionaryData: DictionaryData? = null
        val request = Request.Builder()
            .url("https://api.pons.com/v1/dictionary?q=$word&l=$dictionary&in=$inputLang&ref=true")
            .header("X-Secret", Constants.PonsApiKey)
            .build()

        client.newCall(request).execute().use { response ->
            if (!response.isSuccessful || response.body == null) throw IOException("Unexpected code $response")
            val body = response.body!!.string()
            response.body!!.close()
            val dictionaryListType: Type = object : TypeToken<ArrayList<DictionaryData?>?>() {}.type
            val dictionaryDataList: List<DictionaryData>? = Gson().fromJson(body, dictionaryListType)
            if (dictionaryDataList != null && dictionaryDataList.isNotEmpty()) {
                dictionaryData = dictionaryDataList[0]
            }
        }
        return dictionaryData
    }

    fun PopupWindow.dimBehind() {
        val container = contentView.rootView
        val context = contentView.context
        val wm = context.getSystemService(Context.WINDOW_SERVICE) as WindowManager
        val p = container.layoutParams as WindowManager.LayoutParams
        p.flags = p.flags or WindowManager.LayoutParams.FLAG_DIM_BEHIND
        p.dimAmount = 0.3f
        wm.updateViewLayout(container, p)
    }

    // create an action bar button
    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // R.menu.mymenu is a reference to an xml file named mymenu.xml which should be inside your res/menu directory.
        // If you don't have res/menu, just create a directory named "menu" inside res
        menuInflater.inflate(R.menu.favorite_menu, menu)
        if (itemOid != "" && itemContentType != "") {
            Thread {
                val favoriteItem = favoriteViewModel.getItemByOidAndContentType(itemOid, itemContentType)
                if (favoriteItem != null) {
                    if (favoriteItem.isFavorite) {
                        runOnUiThread {
                            setFavoriteIcon(menu.findItem(R.id.favoriteButton), true)
                            menu.findItem(R.id.favoriteButton).isChecked = true
                        }
                    } else {
                        runOnUiThread {
                            setFavoriteIcon(menu.findItem(R.id.favoriteButton), false)
                            menu.findItem(R.id.favoriteButton).isChecked = false
                        }
                    }
                }
                else {
                    lifecycleScope.launch {
                        favoriteViewModel.insertFavorite(Favorite(0, itemOid, itemContentType, false))
                    }
                    runOnUiThread {
                        setFavoriteIcon(menu.findItem(R.id.favoriteButton), false)
                        menu.findItem(R.id.favoriteButton).isChecked = false
                    }
                }
            }.start()
            menu.findItem(R.id.contactButton).isVisible = itemContentType == "article"
        }
        return super.onCreateOptionsMenu(menu)
    }

    // handle button activities
    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        val id = item.itemId
        if (id == R.id.favoriteButton && itemOid != "" && itemContentType != "") {
            if (item.isChecked) {
                setFavoriteIcon(item, false)
                item.isChecked = false
                Toast.makeText(this, "Removed from Favorites", Toast.LENGTH_SHORT).show()
                lifecycleScope.launch {
                    favoriteViewModel.updateStatus(false, itemOid, itemContentType)
                }
                val logEventType = LogEventType.REMOVE_FAVORITE
                val currentTime = System.currentTimeMillis()
                logEventViewModel.insertEvent(LogEvent(0, logEventType, currentTime, itemOid,
                    false))
            }
            else {
                setFavoriteIcon(item, true)
                item.isChecked = true
                Toast.makeText(this, "Added to Favorites", Toast.LENGTH_SHORT).show()
                lifecycleScope.launch {
                    favoriteViewModel.updateStatus(true, itemOid, itemContentType)
                }
                val logEventType = LogEventType.ADD_FAVORITE
                val currentTime = System.currentTimeMillis()
                logEventViewModel.insertEvent(LogEvent(0, logEventType, currentTime, itemOid,
                    false))
            }
        }
        else if (id == R.id.contactButton && itemUrl != "" && itemAuthor != "") {
            val intent = Intent(this, PublisherActivity::class.java)
            intent.putExtra("author", itemAuthor)
            intent.putExtra("url", itemUrl)
            startActivity(intent)
        }
        return super.onOptionsItemSelected(item)
    }

    private fun setFavoriteIcon(item: MenuItem, isFavorite: Boolean) {
        if (isFavorite) {
            item.icon =
                ContextCompat.getDrawable(
                    applicationContext,
                    R.drawable.baseline_favorite_black_24dp
                )
        }
        else {
            item.icon =
                ContextCompat.getDrawable(
                    applicationContext,
                    R.drawable.baseline_favorite_border_black_24dp
                )
        }
        item.icon.setTint(resources.getColor( R.color.live_red, resources.newTheme()))
    }

    private fun registerInterest(oid: String, contentType: String) {
        Thread{
            try {
                val formBody = FormBody.Builder()
                    .add("oid", oid)
                    .add("contentType", contentType).build()
                val request: Request = Request.Builder().url(registerInterestUrl)
                    .put(formBody)
                    .header("participantId", Constants.participantId).build()
                val response: Response = client.newCall(request).execute()
                if (response.code == 200) {
                    Log.d("API", "Registered Interest")
                }
            }
            catch (e: Exception) {
                Log.e("API", "could not register interest")
            }
        }.start()
    }
}