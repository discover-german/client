package com.awesome.germanhelper

import android.app.*
import android.content.Context
import android.content.Intent
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.NotificationCompat
import androidx.core.app.NotificationManagerCompat
import com.google.gson.Gson
import com.lmu.contentbrowser.Constants
import com.lmu.contentbrowser.ContentBrowserApplication
import com.lmu.contentbrowser.R
import com.lmu.contentbrowser.activities.FlashCardActivity
import com.lmu.contentbrowser.activities.MainActivity
import com.lmu.contentbrowser.activities.PodcastActivity
import com.lmu.contentbrowser.activities.ViewerActivity
import com.lmu.contentbrowser.helpers.GzipHelper
import com.lmu.contentbrowser.receivers.NotificationBroadcastReceiver
import com.lmu.contentbrowser.receivers.NotificationDismissBroadcastReceiver
import okhttp3.OkHttpClient
import java.util.concurrent.TimeUnit
import kotlin.random.Random


/**
 * Helper Class for creating notifications
 */
class NotificationHelper {

    private val client: OkHttpClient = OkHttpClient.Builder().connectTimeout(Constants.CONNECT_TIMEOUT, TimeUnit.SECONDS).build()
    private val gson: Gson = Gson()
    private val gzipHelper = GzipHelper()
    private val getItemUrl = Constants.SERVER_URL + "/item"

    fun checkForNotifications(context: Context) {
        Log.i("notification","Checking for notifications")
        val timeNow = (System.currentTimeMillis() / 1000L)
        val timeSinceLastCall = timeNow - lastCall
        lastCall = timeNow
        Thread{
            val currentEvents = (context.applicationContext as ContentBrowserApplication).repository.getCurrentEvents(timeSinceLastCall)
            for (event in currentEvents) {
                showInformationNotification( context,
                    NID,
                    event.type
                )
                NID += 1
            }
        }.start()
        setupNotificationReceiver(context)
    }


    fun setupNotificationReceiver(context: Context) {
        createNotificationChannel(context)
        val alarmManager =
            (context.getSystemService(AppCompatActivity.ALARM_SERVICE) as AlarmManager)
        val intent = Intent(context.applicationContext, NotificationBroadcastReceiver::class.java)
        val pendingIntent = PendingIntent.getBroadcast(
            context,
            0,
            intent,
            PendingIntent.FLAG_IMMUTABLE
        )
        alarmManager.setExactAndAllowWhileIdle(
            AlarmManager.RTC_WAKEUP,
            System.currentTimeMillis()+10*60*1000, // + five minutes
            pendingIntent
        )
    }

    private fun createNotificationChannel(context: Context) {
        val name = Constants.NOTIFICATION_CHANNEL_NAME
        val descriptionText = Constants.NOTIFICATION_CHANNEL_DESCRIPTION
        val importance = NotificationManager.IMPORTANCE_DEFAULT
        val channel =
            NotificationChannel(Constants.NOTIFICATION_CHANNEL_ID, name, importance).apply {
                description = descriptionText
            }
        // Register the channel with the system
        with(NotificationManagerCompat.from(context)) {
            this.createNotificationChannel(channel)
        }
    }

    private fun showInformationNotification(context: Context, notificationId: Int, eventType: String) {

        Thread {
            val resultIntent: Intent
            var notificationText = ""
            // Create an Intent for the activity you want to start
            if (eventType == "article") {
                val pair = createArticleIntent(context, eventType)
                resultIntent = pair.first
                notificationText = pair.second
            } else if (eventType == "book") {
                val pair = createBookIntent(context, eventType)
                resultIntent = pair.first
                notificationText = pair.second
            } else if (eventType == "podcast") {
                val pair = createPodcastIntent(context, eventType)
                resultIntent = pair.first
                notificationText = pair.second
            } else if (eventType == "flashcard") {
                resultIntent = Intent(context, FlashCardActivity::class.java)
                notificationText = "Test your knowledge with a flashcard"
            } else if (eventType == "random") {
               when(Random(System.nanoTime()).nextInt(0, 4)) {
                   0 -> {
                       val pair = createArticleIntent(context, "article")
                       resultIntent = pair.first
                       notificationText = pair.second
                   }
                   1 -> {
                       val pair = createBookIntent(context, "book")
                       resultIntent = pair.first
                       notificationText = pair.second
                   }
                   2 -> {
                       val pair = createPodcastIntent(context, "podcast")
                       resultIntent = pair.first
                       notificationText = pair.second
                   }
                   3 -> {
                       resultIntent = Intent(context, FlashCardActivity::class.java)
                       notificationText = "Test your knowledge with a flashcard"
                   }
                   else -> {
                       resultIntent = Intent(context, MainActivity::class.java)
                   }
               }
            } else {
                // Create an Intent for the activity you want to start
                resultIntent = Intent(context, MainActivity::class.java)
            }
            resultIntent.putExtra("notification", true)
            // Create the TaskStackBuilder
            val resultPendingIntent = TaskStackBuilder.create(context).run {
                // Add the intent, which inflates the back stack
                //addNextIntent(Intent(context, MainActivity::class.java))
                addNextIntentWithParentStack(resultIntent)
                // Get the PendingIntent containing the entire back stack
                getPendingIntent(RQ, PendingIntent.FLAG_IMMUTABLE
                        or PendingIntent.FLAG_UPDATE_CURRENT)
            }

            RQ++

            val builder = NotificationCompat.Builder(context, Constants.NOTIFICATION_CHANNEL_ID)
                .setSmallIcon(R.drawable.baseline_notifications_black_24dp)
                .setContentTitle("Content Browser")
                .setContentIntent(resultPendingIntent)
                .setDeleteIntent(createOnDismissedIntent(context, notificationId, eventType))
                .setContentText(notificationText)
                .setPriority(NotificationCompat.PRIORITY_HIGH)
                .setAutoCancel(true)
            with(NotificationManagerCompat.from(context)) {
                // notificationId is a unique int for each notification that you must define
                notify(notificationId, builder.build())
            }
        }.start()
    }

    private fun createArticleIntent(context: Context, eventType: String): Pair<Intent, String> {
        val resultIntent = Intent(context, ViewerActivity::class.java)
        val notificationText = "Here's a recommendation for an $eventType"
        resultIntent.putExtra("recommend", "article")
        return Pair(resultIntent, notificationText)
    }

    private fun createBookIntent(context: Context, eventType: String): Pair<Intent, String> {
        val notificationText = "Here's a recommendation for a $eventType"
        val resultIntent = Intent(context, ViewerActivity::class.java)
        resultIntent.putExtra("recommend", "book")
        return Pair(resultIntent, notificationText)
    }

    private fun createPodcastIntent(context: Context, eventType: String): Pair<Intent, String> {
        val notificationText = "Here's a recommendation for a $eventType"
        val resultIntent = Intent(context, PodcastActivity::class.java)
        resultIntent.putExtra("recommend", "podcast")
        return Pair(resultIntent, notificationText)
    }

    private fun createOnDismissedIntent(
        context: Context,
        notificationId: Int,
        eventType: String
    ): PendingIntent? {
        val intent = Intent(context, NotificationDismissBroadcastReceiver::class.java)
        intent.putExtra("eventType", eventType)
        return PendingIntent.getBroadcast(
            context.applicationContext,
            notificationId, intent, PendingIntent.FLAG_IMMUTABLE
                    or PendingIntent.FLAG_UPDATE_CURRENT
        )
    }

    init {

    }

    companion object {
        var lastCall = System.currentTimeMillis() / 1000L
        var NID = 0
        var RQ = 0
    }
}