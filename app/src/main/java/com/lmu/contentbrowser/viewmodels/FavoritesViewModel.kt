package com.lmu.contentbrowser.viewmodels

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.asLiveData
import com.lmu.contentbrowser.data.Favorite
import com.lmu.contentbrowser.database.MyRepository

class FavoritesViewModel (private val repository: MyRepository) : ViewModel() {

    val allFavorites = repository.allFavorites.asLiveData()

    suspend fun insertFavorite(favorite: Favorite): Long {
        return repository.insertFavorite(favorite)
    }

    suspend fun deleteFavorite(id: Long) {
        repository.deleteFavorite(id)
    }

    fun getFavoritesCount(): Int {
        return repository.getFavoritesCount()
    }

    suspend fun updateStatus(isFavorite: Boolean, oid: String, contentType: String) {
        repository.updateFavoriteStatus(isFavorite, oid, contentType)
    }

    fun getItemByOidAndContentType(oid: String, contentType: String): Favorite? {
        return repository.getFavoriteItemByOidAndContentType(oid, contentType)
    }
}

class FavoritesViewModelFactory(private val repository: MyRepository) : ViewModelProvider.Factory {
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(FavoritesViewModel::class.java)) {
            @Suppress("UNCHECKED_CAST")
            return FavoritesViewModel(repository) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }
}