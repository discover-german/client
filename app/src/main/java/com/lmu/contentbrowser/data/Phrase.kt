package com.lmu.contentbrowser.data

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import androidx.room.TypeConverter
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import java.lang.reflect.Type


@Entity(tableName = "phrases")
class Phrase(
    @PrimaryKey(autoGenerate = true) val id: Int,
    @ColumnInfo(name= "highlighted") val original: String,
    @ColumnInfo(name= "phrase") val phrase: String,
    @ColumnInfo(name= "translation") val translation: String,
    @ColumnInfo(name= "syllables") val syllables: String?,
    @ColumnInfo(name= "wordClass") val wordClass: String?,
    @ColumnInfo(name= "examples") val examples: ArrayList<String>?
)