package com.lmu.contentbrowser.listeners

import android.content.Context
import android.content.Intent
import com.lmu.contentbrowser.activities.SearchResultsActivity

class SearchListener(val context: Context, val favoritesOnly: Boolean):
                androidx.appcompat.widget.SearchView.OnQueryTextListener {
    override fun onQueryTextSubmit(query: String?): Boolean {
        val intent = Intent(context, SearchResultsActivity::class.java)
        intent.putExtra("query", query?:"")
        intent.putExtra("favoritesOnly", favoritesOnly)
        context.startActivity(intent)
        return false
    }

    override fun onQueryTextChange(newText: String?): Boolean {
        return false
    }
}