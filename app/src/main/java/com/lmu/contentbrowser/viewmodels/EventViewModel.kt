package com.lmu.contentbrowser.viewmodels

import androidx.lifecycle.*
import com.lmu.contentbrowser.data.Event
import com.lmu.contentbrowser.data.EventMeta
import com.lmu.contentbrowser.database.MyRepository
import kotlinx.coroutines.launch
import java.time.Instant
import java.time.LocalDateTime
import java.time.ZoneId
import java.time.format.DateTimeFormatter
import java.util.concurrent.TimeUnit

class EventViewModel(private val repository: MyRepository) : ViewModel() {

    // Using LiveData and caching what allWords returns has several benefits:
    // - We can put an observer on the data (instead of polling for changes) and only update the
    //   the UI when the data actually changes.
    // - Repository is completely separated from the UI through the ViewModel.
    val allEvents: LiveData<List<Event>> = repository.allEvents.asLiveData()
    val allEventMetas: LiveData<List<EventMeta>> = repository.allEventMetas.asLiveData()

    /**
     * Launching a new coroutine to insert the data in a non-blocking way
     */
    fun insertEvent(event: Event, eventMetas: List<Map<String, EventMeta>>) = viewModelScope.launch {
        repository.insertEvent(event, eventMetas)
    }

    fun printCurrentTime() = viewModelScope.launch {
        repository.currentTime()
    }

    fun deleteAllEventsAndMetas() = viewModelScope.launch {
        repository.deleteAllEventsAndMetas()
    }

    fun deleteEventAndMetas(id: Long) = viewModelScope.launch {
        repository.deleteEventAndMetas(id)
    }

    fun getEventsForDate(dateEpoch: Long): List<Event> {
        return repository.getEventsForDate(dateEpoch)
    }

    fun getUpcomingEvents(): List<Event> {
        /**
         * WARNING: MUST BE CALLED ASYNCHRONOUSLY!
         */
        return repository.getUpcomingEvents()
    }

    fun getCurrentEvents(timeWindow: Long): List<Event>  {
        /**
         * WARNING: MUST BE CALLED ASYNCHRONOUSLY!
         */
        return repository.getCurrentEvents(timeWindow)
    }

    fun getTimeForEvent(id: Long): String {
        /**
         * WARNING: MUST BE CALLED ASYNCHRONOUSLY!
         */
        var dateAndTime = ""
        val metas = repository.getEventMetasForID(id)
        for (element in metas) {
            if (element.meta_key == "repeat_start"){
                var interval = Long.MAX_VALUE
                for (secondElement in metas) {
                    if (secondElement.meta_key.contains("repeat_interval")) {
                        interval = secondElement.meta_value
                        break
                    }
                }
                val ldt: LocalDateTime
                if (element.meta_value*1000 < System.currentTimeMillis() && interval < Long.MAX_VALUE) {
                    var nextEventTime = element.meta_value * 1000
                    while(nextEventTime < System.currentTimeMillis()) {
                        nextEventTime += interval * 1000
                    }
                    ldt = Instant.ofEpochMilli(nextEventTime)
                        .atZone(ZoneId.systemDefault()).toLocalDateTime()
                }
                else {
                    ldt = Instant.ofEpochMilli(element.meta_value*1000)
                        .atZone(ZoneId.systemDefault()).toLocalDateTime()
                }
                dateAndTime = ldt.format(DateTimeFormatter.ofPattern("HH:mm")).toString()
                break
            }
        }
        return dateAndTime
    }

    fun getIntervalForEvent(id: Long): String {
        /**
         * WARNING: MUST BE CALLED ASYNCHRONOUSLY!
         */
        var intervalTime = ""
        val metas = repository.getEventMetasForID(id)
        for (element in metas) {
            if (element.meta_key.contains("repeat_interval")) {
                if (element.meta_value == Long.MAX_VALUE) {
                    return "never"
                }
                var interval = element.meta_value * 1000

                val days: Long = TimeUnit.MILLISECONDS
                    .toDays(interval)
                if (days!=0L) {
                    intervalTime += "${days}d"
                }

                interval = interval - TimeUnit.DAYS.toMillis(days)

                val hours: Long = TimeUnit.MILLISECONDS
                    .toHours(interval)
                if (hours!=0L) {
                    intervalTime += ", ${hours}h"
                }

                interval = interval - TimeUnit.HOURS.toMillis(hours)

                val minutes: Long = TimeUnit.MILLISECONDS
                    .toMinutes(interval)
                if (minutes!=0L) {
                    intervalTime += ", ${minutes}min"
                }

                interval = interval - TimeUnit.MINUTES.toMillis(minutes)

                val seconds: Long = TimeUnit.MILLISECONDS
                    .toSeconds(interval)
                if (seconds!=0L) {
                    intervalTime += ", ${seconds}s"
                }
                if (intervalTime.startsWith(", ")) {
                    intervalTime = intervalTime.removePrefix(", ")
                }
                break
            }
        }
        if (intervalTime == "") {
            intervalTime = "single"
        }
        return intervalTime
    }


}

class EventViewModelFactory(private val repository: MyRepository) : ViewModelProvider.Factory {
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(EventViewModel::class.java)) {
            @Suppress("UNCHECKED_CAST")
            return EventViewModel(repository) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }
}