package com.lmu.contentbrowser.viewmodels

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.lmu.contentbrowser.data.ContentPreference
import com.lmu.contentbrowser.database.MyRepository

class ContentPreferenceViewModel (private val repository: MyRepository) : ViewModel() {
    fun insertContentPreference(contentPreference: ContentPreference) : Long {
        return repository.insertContentPreference(contentPreference)
    }

    fun getContentPreferenceByKeyAsync(key: String): ContentPreference? {
        return repository.getContentPreferenceByKeyAsync(key)
    }
}

class ContentPreferenceViewModelFactory(private val repository: MyRepository) : ViewModelProvider.Factory {
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(ContentPreferenceViewModel::class.java)) {
            @Suppress("UNCHECKED_CAST")
            return ContentPreferenceViewModel(repository) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }
}