package com.lmu.contentbrowser.adapters

import android.app.Activity
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.TextView
import com.lmu.contentbrowser.R

class PublisherAdapter (private val context: Activity, private val entries: Array<String>):
    ArrayAdapter<String>(context, R.layout.layout_content_item, entries) {

    override fun getView(position: Int, view: View?, parent: ViewGroup): View {
        val inflater = context.layoutInflater
        val rowView = inflater.inflate(R.layout.layout_publisher_row, parent, false)

        val data = entries[position]
        var name = ""
        var url = ""
        var email = ""
        var phone = ""
        if (data.split("|").size >=4) {
            name = data.split("|")[0]
            url = data.split("|")[1]
            email = data.split("|")[2]
            phone = data.split("|")[3]
        }
        val nameView = rowView.findViewById<TextView>(R.id.name)
        val urlView = rowView.findViewById<TextView>(R.id.link)
        val emailView = rowView.findViewById<TextView>(R.id.email)
        val phoneView = rowView.findViewById<TextView>(R.id.phone)

        nameView.text = name
        urlView.text = url
        emailView.text = email
        phoneView.text = phone
        return rowView
    }
}