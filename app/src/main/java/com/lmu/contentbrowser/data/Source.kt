package com.lmu.contentbrowser.data

enum class Source {
    SPIEGEL, FOCUS, KRUSCHEL, LOGO, NACHRICHTENLEICHT, PM, FITFORFUN, GQ, GOFEMININ,
    NEWS, BOOKS, PODCASTS
}