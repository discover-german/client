package com.lmu.contentbrowser.data

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "whitelist")
class WhitelistItem(
    @PrimaryKey @ColumnInfo(name= "source") val source: Source,
    @ColumnInfo(name= "allowed") val allowed: Boolean,
)
