package com.lmu.contentbrowser.data

data class Article (
    val _id: ObjectId,
    val title: String,
    val link: String,
    val date: String,
    val author: String,
    val body: String,
    val image_link: String
)