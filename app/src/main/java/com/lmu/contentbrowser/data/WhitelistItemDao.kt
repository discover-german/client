package com.lmu.contentbrowser.data

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query

@Dao
interface WhitelistItemDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(WhitelistItem: WhitelistItem): Long

    @Query("SELECT * FROM whitelist")
    fun getAllItemsAsync(): List<WhitelistItem>

    @Query("SELECT Q.* FROM whitelist Q WHERE Q.source = :source")
    fun getItemBySourceAsync(source: Source): WhitelistItem?

    @Query("DELETE FROM whitelist WHERE source = :source")
    suspend fun deleteItem(source: Source)

    @Query("DELETE FROM whitelist")
    suspend fun deleteAll()
}