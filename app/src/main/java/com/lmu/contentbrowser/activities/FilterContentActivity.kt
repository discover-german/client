package com.lmu.contentbrowser.activities

import android.os.Bundle
import android.widget.NumberPicker
import android.widget.TextView
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.SwitchCompat
import com.google.android.material.slider.RangeSlider
import com.lmu.contentbrowser.ContentBrowserApplication
import com.lmu.contentbrowser.R
import com.lmu.contentbrowser.data.*
import com.lmu.contentbrowser.viewmodels.*

class FilterContentActivity: AppCompatActivity() {
    val whitelistModel: WhitelistViewModel by viewModels {
        WhitelistViewModelFactory((application as ContentBrowserApplication).repository)
    }
    val contentPreferenceModel: ContentPreferenceViewModel by viewModels {
        ContentPreferenceViewModelFactory((application as ContentBrowserApplication).repository)
    }
    val logEventViewModel: LogEventViewModel by viewModels {
        LogEventViewModelFactory((application as ContentBrowserApplication).repository)
    }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_filter)

        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        supportActionBar?.title = "Filter Content"

        val newsSwitch = findViewById<SwitchCompat>(R.id.newsSwitch)

        val spiegelSwitch = findViewById<SwitchCompat>(R.id.spiegelSwitch)
        val focusSwitch = findViewById<SwitchCompat>(R.id.focusSwitch)
        val kruschelSwitch = findViewById<SwitchCompat>(R.id.kruschelSwitch)
        val logoSwitch = findViewById<SwitchCompat>(R.id.logoSwitch)
        val nachrichtenleichtSwitch = findViewById<SwitchCompat>(R.id.nachrichtenleichtSwitch)
        val pmSwitch = findViewById<SwitchCompat>(R.id.pmSwitch)
        val fitforfunSwitch = findViewById<SwitchCompat>(R.id.fitforfunSwitch)
        val gqSwitch = findViewById<SwitchCompat>(R.id.gqSwitch)
        val gofemininSwitch = findViewById<SwitchCompat>(R.id.gofemininSwitch)
        val booksSwitch = findViewById<SwitchCompat>(R.id.booksSwitch)
        val podcastsSwitch = findViewById<SwitchCompat>(R.id.podcastsSwitch)

        newsSwitch.setOnClickListener {
            if ((it as SwitchCompat).isChecked) {
                spiegelSwitch.isChecked = true
                focusSwitch.isChecked = true
                kruschelSwitch.isChecked = true
                logoSwitch.isChecked = true
                nachrichtenleichtSwitch.isChecked = true
                pmSwitch.isChecked = true
                fitforfunSwitch.isChecked = true
                gqSwitch.isChecked = true
                gofemininSwitch.isChecked = true
                setWhitelistStatusForAllNews(true)
            }
            else {
                spiegelSwitch.isChecked = false
                focusSwitch.isChecked = false
                kruschelSwitch.isChecked = false
                logoSwitch.isChecked = false
                nachrichtenleichtSwitch.isChecked = false
                pmSwitch.isChecked = false
                fitforfunSwitch.isChecked = false
                gqSwitch.isChecked = false
                gofemininSwitch.isChecked = false
                setWhitelistStatusForAllNews(false)
            }
        }

        spiegelSwitch.setOnClickListener {
            setWhitelistStatus(it as SwitchCompat, Source.SPIEGEL)
            setNewsSwitchStatus(newsSwitch, spiegelSwitch, focusSwitch, kruschelSwitch,
                logoSwitch, nachrichtenleichtSwitch, pmSwitch, fitforfunSwitch, gqSwitch,
                gofemininSwitch)
        }
        focusSwitch.setOnClickListener {
            setWhitelistStatus(it as SwitchCompat, Source.FOCUS)
            setNewsSwitchStatus(newsSwitch, spiegelSwitch, focusSwitch, kruschelSwitch,
                logoSwitch, nachrichtenleichtSwitch, pmSwitch, fitforfunSwitch, gqSwitch,
                gofemininSwitch)
        }
        kruschelSwitch.setOnClickListener {
            setWhitelistStatus(it as SwitchCompat, Source.KRUSCHEL)
            setNewsSwitchStatus(newsSwitch, spiegelSwitch, focusSwitch, kruschelSwitch,
                logoSwitch, nachrichtenleichtSwitch, pmSwitch, fitforfunSwitch, gqSwitch,
                gofemininSwitch)
        }
        logoSwitch.setOnClickListener {
            setWhitelistStatus(it as SwitchCompat, Source.LOGO)
            setNewsSwitchStatus(newsSwitch, spiegelSwitch, focusSwitch, kruschelSwitch,
                logoSwitch, nachrichtenleichtSwitch, pmSwitch, fitforfunSwitch, gqSwitch,
                gofemininSwitch)
        }
        nachrichtenleichtSwitch.setOnClickListener {
            setWhitelistStatus(it as SwitchCompat, Source.NACHRICHTENLEICHT)
            setNewsSwitchStatus(newsSwitch, spiegelSwitch, focusSwitch, kruschelSwitch,
                logoSwitch, nachrichtenleichtSwitch, pmSwitch, fitforfunSwitch, gqSwitch,
                gofemininSwitch)
        }
        pmSwitch.setOnClickListener {
            setWhitelistStatus(it as SwitchCompat, Source.PM)
            setNewsSwitchStatus(newsSwitch, spiegelSwitch, focusSwitch, kruschelSwitch,
                logoSwitch, nachrichtenleichtSwitch, pmSwitch, fitforfunSwitch, gqSwitch,
                gofemininSwitch)
        }
        fitforfunSwitch.setOnClickListener {
            setWhitelistStatus(it as SwitchCompat, Source.FITFORFUN)
            setNewsSwitchStatus(newsSwitch, spiegelSwitch, focusSwitch, kruschelSwitch,
                logoSwitch, nachrichtenleichtSwitch, pmSwitch, fitforfunSwitch, gqSwitch,
                gofemininSwitch)
        }
        gqSwitch.setOnClickListener {
            setWhitelistStatus(it as SwitchCompat, Source.GQ)
            setNewsSwitchStatus(newsSwitch, spiegelSwitch, focusSwitch, kruschelSwitch,
                logoSwitch, nachrichtenleichtSwitch, pmSwitch, fitforfunSwitch, gqSwitch,
                gofemininSwitch)
        }
        gofemininSwitch.setOnClickListener {
            setWhitelistStatus(it as SwitchCompat, Source.GOFEMININ)
            setNewsSwitchStatus(newsSwitch, spiegelSwitch, focusSwitch, kruschelSwitch,
                logoSwitch, nachrichtenleichtSwitch, pmSwitch, fitforfunSwitch, gqSwitch,
                gofemininSwitch)
        }
        booksSwitch.setOnClickListener {
            setWhitelistStatus(it as SwitchCompat, Source.BOOKS)
        }
        podcastsSwitch.setOnClickListener {
            setWhitelistStatus(it as SwitchCompat, Source.PODCASTS)
        }

        Thread {
            val spiegelItem = whitelistModel.getWhitelistItemAsync(Source.SPIEGEL)
            val focusItem = whitelistModel.getWhitelistItemAsync(Source.FOCUS)
            val kruschelItem = whitelistModel.getWhitelistItemAsync(Source.KRUSCHEL)
            val logoItem = whitelistModel.getWhitelistItemAsync(Source.LOGO)
            val nachrichtenleichtItem = whitelistModel.getWhitelistItemAsync(Source.NACHRICHTENLEICHT)
            val pmItem = whitelistModel.getWhitelistItemAsync(Source.PM)
            val fitforfunItem = whitelistModel.getWhitelistItemAsync(Source.FITFORFUN)
            val gqItem = whitelistModel.getWhitelistItemAsync(Source.GQ)
            val gofemininItem = whitelistModel.getWhitelistItemAsync(Source.GOFEMININ)
            val booksItem = whitelistModel.getWhitelistItemAsync(Source.BOOKS)
            val podcastsItem = whitelistModel.getWhitelistItemAsync(Source.PODCASTS)

            runOnUiThread {
                if (spiegelItem != null) {
                    spiegelSwitch.isChecked = spiegelItem.allowed
                }
                else {
                    spiegelSwitch.isChecked = true
                    whitelistModel.insertWhitelistItem(WhitelistItem(Source.SPIEGEL, true))
                }
                if (focusItem != null) {
                    focusSwitch.isChecked = focusItem.allowed
                }
                else {
                    focusSwitch.isChecked = true
                    whitelistModel.insertWhitelistItem(WhitelistItem(Source.FOCUS, true))
                }
                if (kruschelItem != null) {
                    kruschelSwitch.isChecked = kruschelItem.allowed
                }
                else {
                    kruschelSwitch.isChecked = true
                    whitelistModel.insertWhitelistItem(WhitelistItem(Source.KRUSCHEL, true))
                }
                if (logoItem != null) {
                    logoSwitch.isChecked = logoItem.allowed
                }
                else {
                    logoSwitch.isChecked = true
                    whitelistModel.insertWhitelistItem(WhitelistItem(Source.LOGO, true))
                }
                if (nachrichtenleichtItem != null) {
                    nachrichtenleichtSwitch.isChecked = nachrichtenleichtItem.allowed
                }
                else {
                    nachrichtenleichtSwitch.isChecked = true
                    whitelistModel.insertWhitelistItem(WhitelistItem(Source.NACHRICHTENLEICHT, true))
                }
                if (pmItem != null) {
                    pmSwitch.isChecked = pmItem.allowed
                }
                else {
                    pmSwitch.isChecked = true
                    whitelistModel.insertWhitelistItem(WhitelistItem(Source.PM, true))
                }
                if (fitforfunItem != null) {
                    fitforfunSwitch.isChecked = fitforfunItem.allowed
                }
                else {
                    fitforfunSwitch.isChecked = true
                    whitelistModel.insertWhitelistItem(WhitelistItem(Source.FITFORFUN, true))
                }
                if (gqItem != null) {
                    gqSwitch.isChecked = gqItem.allowed
                }
                else {
                    gqSwitch.isChecked = true
                    whitelistModel.insertWhitelistItem(WhitelistItem(Source.GQ, true))
                }
                if (gofemininItem != null) {
                    gofemininSwitch.isChecked = gofemininItem.allowed
                }
                else {
                    gofemininSwitch.isChecked = true
                    whitelistModel.insertWhitelistItem(WhitelistItem(Source.GOFEMININ, true))
                }
                if (booksItem != null) {
                    booksSwitch.isChecked = booksItem.allowed
                }
                else {
                    booksSwitch.isChecked = true
                    whitelistModel.insertWhitelistItem(WhitelistItem(Source.BOOKS, true))
                }
                if (podcastsItem != null) {
                    podcastsSwitch.isChecked = podcastsItem.allowed
                }
                else {
                    podcastsSwitch.isChecked = true
                    whitelistModel.insertWhitelistItem(WhitelistItem(Source.PODCASTS, true))
                }
                setNewsSwitchStatus(newsSwitch, spiegelSwitch, focusSwitch, kruschelSwitch,
                    logoSwitch, nachrichtenleichtSwitch, pmSwitch, fitforfunSwitch, gqSwitch,
                    gofemininSwitch)
            }
        }.start()

        val difficultyPicker = findViewById<NumberPicker>(R.id.difficultyPicker)
        difficultyPicker.minValue = 0
        difficultyPicker.value = 2
        difficultyPicker.maxValue = 5
        difficultyPicker.displayedValues = listOf("A1", "A2", "B1", "B2", "C1", "C2").toTypedArray()

        val personalisationSlider = findViewById<RangeSlider>(R.id.personalisationSlider)
        val personalisationValue = findViewById<TextView>(R.id.personalisationValue)

        Thread {
                val difficulty = contentPreferenceModel.getContentPreferenceByKeyAsync("difficulty")
                if (difficulty != null) {
                    difficultyPicker.value = difficulty.value
                } else {
                    difficultyPicker.value = 2
                    contentPreferenceModel.insertContentPreference(
                        ContentPreference(
                            "difficulty",
                            2
                        )
                    )
                }
                val personalisationAmount =
                    contentPreferenceModel.getContentPreferenceByKeyAsync("personalisationAmount")
                if (personalisationAmount != null) {
                    personalisationSlider.values = listOf(personalisationAmount.value.toFloat())
                    personalisationValue.text = "${personalisationAmount.value}/10"
                } else {
                    personalisationSlider.values = listOf(8.0f)
                    personalisationValue.text = "8/10"
                    contentPreferenceModel.insertContentPreference(
                        ContentPreference("personalisationAmount", 8)
                    )
                }
        }.start()

        difficultyPicker.setOnValueChangedListener { picker, oldVal, newVal ->
            Thread {
                contentPreferenceModel.insertContentPreference(
                    ContentPreference(
                        "difficulty",
                        newVal
                    )
                )
            }.start()
        }

        personalisationSlider.addOnChangeListener { slider, value, fromUser ->
            Thread {
                contentPreferenceModel.insertContentPreference(
                    ContentPreference("personalisationAmount", value.toInt())
                )
            }.start()
            personalisationValue.text = "${value.toInt()}/10"
        }
    }

    override fun onResume() {
        super.onResume()
        val logEventType = LogEventType.FILTER_START
        val currentTime = System.currentTimeMillis()
        logEventViewModel.insertEvent(
            LogEvent(0, logEventType, currentTime, null,
                false)
        )
    }

    override fun onPause() {
        super.onPause()
        val logEventType = LogEventType.FILTER_END
        val currentTime = System.currentTimeMillis()
        logEventViewModel.insertEvent(
            LogEvent(0, logEventType, currentTime, null,
                false)
        )
    }

    private fun setWhitelistStatus(it: SwitchCompat, source: Source) {
        if (it.isChecked) {
            whitelistModel.insertWhitelistItem(WhitelistItem(source, true))
        }
        else {
            whitelistModel.insertWhitelistItem(WhitelistItem(source, false))
        }
    }

    private fun setWhitelistStatusForAllNews(status: Boolean) {
        whitelistModel.insertWhitelistItem(WhitelistItem(Source.SPIEGEL, status))
        whitelistModel.insertWhitelistItem(WhitelistItem(Source.FOCUS, status))
        whitelistModel.insertWhitelistItem(WhitelistItem(Source.KRUSCHEL, status))
        whitelistModel.insertWhitelistItem(WhitelistItem(Source.LOGO, status))
        whitelistModel.insertWhitelistItem(WhitelistItem(Source.NACHRICHTENLEICHT, status))
        whitelistModel.insertWhitelistItem(WhitelistItem(Source.PM, status))
        whitelistModel.insertWhitelistItem(WhitelistItem(Source.FITFORFUN, status))
        whitelistModel.insertWhitelistItem(WhitelistItem(Source.GQ, status))
        whitelistModel.insertWhitelistItem(WhitelistItem(Source.GOFEMININ, status))
        whitelistModel.insertWhitelistItem(WhitelistItem(Source.NEWS, status))
    }

    private fun setNewsSwitchStatus(newsSwitch: SwitchCompat, spiegelSwitch: SwitchCompat,
                                    focusSwitch: SwitchCompat, kruschelSwitch: SwitchCompat,
                                    logoSwitch: SwitchCompat, nachrichtenleichtSwitch: SwitchCompat,
                                    pmSwitch: SwitchCompat, fitforfunSwitch: SwitchCompat,
                                    gqSwitch: SwitchCompat, gofemininSwitch: SwitchCompat) {
        val newsAllowed = spiegelSwitch.isChecked || focusSwitch.isChecked ||
                kruschelSwitch.isChecked || logoSwitch.isChecked ||
                nachrichtenleichtSwitch.isChecked || pmSwitch.isChecked ||
                fitforfunSwitch.isChecked || gqSwitch.isChecked || gofemininSwitch.isChecked
        newsSwitch.isChecked = newsAllowed
        whitelistModel.insertWhitelistItem(WhitelistItem(Source.NEWS, newsAllowed))
    }
}