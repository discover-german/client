package com.lmu.contentbrowser.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ListView
import android.widget.SearchView
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import com.lmu.contentbrowser.ContentBrowserApplication
import com.lmu.contentbrowser.R
import com.lmu.contentbrowser.adapters.HistoryAdapter
import com.lmu.contentbrowser.data.Order
import com.lmu.contentbrowser.viewmodels.HistoryViewModel
import com.lmu.contentbrowser.viewmodels.HistoryViewModelFactory
import com.lmu.contentbrowser.listeners.SearchListener

class HistoryTabFragment: Fragment() {

    private var order = Order.NEWEST

    val historyViewModel: HistoryViewModel by viewModels {
        HistoryViewModelFactory((requireActivity().application
                as ContentBrowserApplication).repository)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_history_tab, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val listview = view.findViewById<ListView>(R.id.listview)

        historyViewModel.allHistoryItems.observe(viewLifecycleOwner) {
            val sortedEntries = when(order) {
                Order.NEWEST -> it.sortedBy { it.timeAdded }.reversed()
                Order.OLDEST -> it.sortedBy { it.timeAdded }
                Order.ALPHABETICAL -> it.sortedBy { it.title }
            }

            val entries = arrayListOf<String>()
            for (historyItem in sortedEntries) {
                entries.add("${historyItem.contentType}|${historyItem.title}|" +
                        "${historyItem.timeAdded}|${historyItem.description}|${historyItem.oid}|" +
                        "${historyItem.imageLink}|${historyItem.url}|${historyItem.id}")
            }
            val myAdapter = HistoryAdapter(requireActivity(), entries.toTypedArray())
            listview.adapter = myAdapter
        }
        val searchView = view.findViewById<androidx.appcompat.widget.SearchView>(R.id.searchView)
        searchView.setOnQueryTextListener(SearchListener(requireContext(), false))
    }
}