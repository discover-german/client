package com.lmu.contentbrowser.data

data class Song (
    val title: String,
    val description: String,
    val url: String,
    val imageUrl: String,
    val oid: String
)