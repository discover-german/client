package com.lmu.contentbrowser.data

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import kotlinx.coroutines.flow.Flow

@Dao
interface HistoryItemDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(historyItem: HistoryItem): Long

    @Query("SELECT * FROM history")
    fun getAllItems(): Flow<List<HistoryItem>>

    @Query("DELETE FROM history WHERE id = :id")
    suspend fun deleteItem(id: Long)

    @Query("DELETE FROM history")
    suspend fun deleteAll()

    @Query("SELECT * FROM history WHERE id = :id")
    fun getItemById(id: Long): HistoryItem

    @Query("""
        SELECT DISTINCT *
        FROM history
        JOIN history_fts ON history.title = history_fts.title
        WHERE history_fts MATCH :query
    """)
    suspend fun search(query: String): List<HistoryItem>

    @Query("""
        SELECT DISTINCT *, matchinfo(history_fts) as matchInfo
        FROM history
        JOIN history_fts ON history.title = history_fts.title
        WHERE history_fts MATCH :query
       """)
    suspend fun searchWithMatchInfo(query: String): List<HistoryItemWithMatchInfo>
}