package com.lmu.contentbrowser.data

import androidx.room.*
import java.sql.Date

@Entity(tableName = "history")
class HistoryItem (
    @PrimaryKey(autoGenerate = true) val id: Int,
    @ColumnInfo(name= "oid") val oid: String,
    @ColumnInfo(name= "contentType") val contentType: String,
    @ColumnInfo(name = "title") val title: String,
    @ColumnInfo(name = "description") val description: String,
    @ColumnInfo(name = "imageLink") val imageLink: String,
    @ColumnInfo(name = "url") val url: String,
    @ColumnInfo(name = "timeAdded") val timeAdded: Long
)

@Entity(tableName = "history_fts")
@Fts4(contentEntity = HistoryItem::class)
data class HistoryItemFTS(
    @ColumnInfo(name = "title") val title: String,
    @ColumnInfo(name = "description") val description: String,
)

data class HistoryItemWithMatchInfo(
    @Embedded
    val historyItem: HistoryItem,
    @ColumnInfo(name = "matchInfo")
    val matchInfo: ByteArray
) {
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as HistoryItemWithMatchInfo

        if (historyItem != other.historyItem) return false
        if (!matchInfo.contentEquals(other.matchInfo)) return false

        return true
    }

    override fun hashCode(): Int {
        var result = historyItem.hashCode()
        result = 31 * result + matchInfo.contentHashCode()
        return result
    }
}