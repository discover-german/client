package com.lmu.contentbrowser.data

data class PodcastRecommendation(
    val recommendation: Podcast,
    val trigger: Podcast,
    val episodeIndex: Int
)
