package com.lmu.contentbrowser.database

import android.content.Context
import androidx.room.*
import androidx.sqlite.db.SupportSQLiteDatabase
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.lmu.contentbrowser.data.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.launch
import java.lang.reflect.Type

// Annotates class to be a Room Database with a table (entity) of a class
@Database(entities = [WhitelistItem::class,
                      Phrase::class,
                      Favorite::class,
                      HistoryItem::class,
                      HistoryItemFTS::class,
                      Event::class,
                      EventMeta::class,
                      ContentPreference::class,
                      LogEvent::class], version = 1, exportSchema = false)
@TypeConverters(Converters::class)
public abstract class MyRoomDatabase: RoomDatabase() {

    abstract fun whitelistItemDao(): WhitelistItemDao
    abstract fun phraseDao(): PhraseDao
    abstract fun favoriteDao(): FavoriteDao
    abstract fun historyItemDao(): HistoryItemDao
    abstract fun eventDao(): EventDao
    abstract fun eventMetaDao(): EventMetaDao
    abstract fun contentPreferenceDao(): ContentPreferenceDao
    abstract fun logEventDao(): LogEventDao


    private class DatabaseCallback(
            private val scope: CoroutineScope
    ) : RoomDatabase.Callback() {

        override fun onCreate(db: SupportSQLiteDatabase) {
            super.onCreate(db)
            INSTANCE?.let { database ->
                scope.launch {

                }
            }
        }
    }

    companion object {
        // Singleton prevents multiple instances of database opening at the
        // same time.
        @Volatile
        private var INSTANCE: MyRoomDatabase? = null

        fun getDatabase(context: Context, scope: CoroutineScope): MyRoomDatabase {
            // if the INSTANCE is not null, then return it,
            // if it is, then create the database
            return INSTANCE
                    ?: synchronized(this) {
                        val instance = Room.databaseBuilder(
                                context.applicationContext,
                                MyRoomDatabase::class.java,
                                "application"
                        )
                                .addCallback(DatabaseCallback(scope))
                                .build()
                        INSTANCE = instance
                        // return instance
                        instance
                    }
        }
    }
}

object Converters {
    @TypeConverter
    fun fromString(value: String?): ArrayList<String>? {
        val listType: Type = object : TypeToken<ArrayList<String?>?>() {}.getType()
        if (value !=null) {
            return Gson().fromJson(value, listType)
        }
        else {
            return null
        }
    }

    @TypeConverter
    fun fromArrayList(list: ArrayList<String?>?): String {
        val gson = Gson()
        return gson.toJson(list)
    }
}