package com.lmu.contentbrowser.activities

import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.content.ServiceConnection
import android.content.res.Configuration
import android.content.res.Resources
import android.os.Bundle
import android.os.Handler
import android.os.IBinder
import android.text.Html
import android.util.Log
import android.util.TypedValue
import android.view.*
import android.view.View.MeasureSpec
import android.webkit.WebView
import android.widget.*
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.LinearLayoutCompat
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.ContextCompat
import androidx.core.text.HtmlCompat
import androidx.core.view.isNotEmpty
import androidx.fragment.app.Fragment
import androidx.lifecycle.lifecycleScope
import com.google.android.exoplayer2.ui.DefaultTimeBar
import com.google.android.exoplayer2.ui.PlayerView
import com.google.android.exoplayer2.ui.StyledPlayerControlView
import com.google.android.exoplayer2.ui.StyledPlayerView
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.lmu.contentbrowser.Constants
import com.lmu.contentbrowser.ContentBrowserApplication
import com.lmu.contentbrowser.R
import com.lmu.contentbrowser.adapters.PodcastEpisodeAdapter
import com.lmu.contentbrowser.callbacks.ImageCallback
import com.lmu.contentbrowser.data.*
import com.lmu.contentbrowser.dialogs.LoginDialog
import com.lmu.contentbrowser.helpers.GzipHelper
import com.lmu.contentbrowser.services.MusicService
import com.lmu.contentbrowser.viewmodels.*
import com.squareup.picasso.Picasso
import kotlinx.coroutines.launch
import okhttp3.*
import okhttp3.HttpUrl.Companion.toHttpUrl
import okhttp3.HttpUrl.Companion.toHttpUrlOrNull
import okhttp3.MediaType.Companion.toMediaType
import okhttp3.RequestBody.Companion.toRequestBody
import java.io.IOException
import java.lang.reflect.Type
import java.net.SocketTimeoutException
import java.util.ArrayList
import java.util.concurrent.TimeUnit
import kotlin.concurrent.thread


class PodcastActivity: AppCompatActivity() {
    var musicService: MusicService? = null
    lateinit var podcastThumbnailView: ImageView
    lateinit var podcastTitleView: TextView
    private var podcastVideoView: StyledPlayerView? = null
    var song: Song? = null
    private val registerInterestUrl = Constants.SERVER_URL+"/register_interest"
    private val getItemUrl = Constants.SERVER_URL + "/item"
    private var isBound = false
    private lateinit var playerControls: StyledPlayerControlView
    private val client = OkHttpClient.Builder().connectTimeout(Constants.CONNECT_TIMEOUT, TimeUnit.SECONDS).build()
    private val gson = Gson()
    private val gzipHelper = GzipHelper()
    private var itemUrl: String = ""
    private lateinit var itemOid: String
    private lateinit var itemContentType: String
    private lateinit var handler: Handler
    private var mActionMode: ActionMode? = null
    private val entries = arrayListOf<String>()
    private val DICTIONARY = 0

    val favoriteViewModel: FavoritesViewModel by viewModels {
        FavoritesViewModelFactory((application as ContentBrowserApplication).repository)
    }

    val historyViewModel: HistoryViewModel by viewModels {
        HistoryViewModelFactory((application as ContentBrowserApplication).repository)
    }

    val logEventViewModel: LogEventViewModel by viewModels {
        LogEventViewModelFactory((application as ContentBrowserApplication).repository)
    }

    val phrasesViewModel: PhrasesViewModel by viewModels {
        PhrasesViewModelFactory((application as ContentBrowserApplication).repository)
    }

    val contentPreferenceModel: ContentPreferenceViewModel by viewModels {
        ContentPreferenceViewModelFactory((application as ContentBrowserApplication).repository)
    }

    val whitelistModel: WhitelistViewModel by viewModels {
        WhitelistViewModelFactory((application as ContentBrowserApplication).repository)
    }


    private val musicServiceConnection = object: ServiceConnection {
        override fun onServiceDisconnected(name: ComponentName?) {
            musicService = null
        }

        override fun onServiceConnected(name: ComponentName?, service: IBinder?) {
            musicService = (service as MusicService.MusicBinder).getService()
            //musicService?.setPlayerView(playerView)
            musicService?.setPlayerControls(playerControls)
            if (song!=null) {
                musicService?.setPodcastThumbnail(podcastThumbnailView)
                if (musicService?.isPlaying() == false ||
                    (musicService?.isPlaying() == true &&
                            musicService?.getCurrentTitle() != song!!.title)) {
                    musicService?.playSong(song!!)
                }
            }
        }
    }

    fun bindService() {
        if (bindService(Intent(this, MusicService::class.java), musicServiceConnection, Context.BIND_AUTO_CREATE)) {
            isBound = true
        } else {
            // log error here
        }
    }

    fun unbindService() {
        if (isBound) {
            unbindService(musicServiceConnection)
            isBound = false
        }
    }

    override fun onDestroy() {
        unbindService()
        super.onDestroy()
    }

    override fun onStop() {
        super.onStop()
        musicService?.pause()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_podcast)

        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        handler = Handler(mainLooper)

        if (Constants.participantId == "") {
            initializePassword()
        }

        playerControls = findViewById(R.id.playerControls)
        podcastThumbnailView = findViewById<ImageView>(R.id.podcastThumbnail)
        podcastTitleView = findViewById(R.id.title)

        val b = intent.extras
        val url = b?.getString("url") ?: ""
        val recommend = b?.getString("recommend") ?: ""
        val triggeredByNotification = b?.getBoolean("notification") ?: false
        val item = b?.getString("item")
        if (url != "") {
            itemUrl = url
        }
        if (item != null && item.split("|").size >= 6) {
            initValuesWithItem(item, url, triggeredByNotification)
            bindService()
        }
        else if (recommend != "") {
            Thread{
                val recommendedItem = getItemFromRecommendation(recommend)
                runOnUiThread {
                    initValuesWithItem(recommendedItem, itemUrl, triggeredByNotification)
                    bindService()
                }
            }.start()
        }
        else {
            return
        }

        val exoProgress = findViewById<DefaultTimeBar>(R.id.exo_progress)
        when (resources.configuration.uiMode and Configuration.UI_MODE_NIGHT_MASK) {
            Configuration.UI_MODE_NIGHT_YES -> {
                exoProgress.setBufferedColor(resources.getColor(R.color.white_05,
                    resources.newTheme()))
            }
            else -> {

            }
        }
    }

    private fun initValuesWithItem(item: String, url: String, triggeredByNotification: Boolean) {

        itemContentType = item.split("|")[0]
        val itemTitle = item.split("|")[1]
        val description = item.split("|")[2]
        val oid = item.split("|")[3]
        val imageUrl = item.split("|")[5]
        var episodeIndex = ""

        supportActionBar?.title = itemTitle

        if (item.split("|").size >= 7) {
            episodeIndex = item.split("|")[6]
        }

        itemOid = oid

        val currentTime = System.currentTimeMillis()

        lifecycleScope.launch {
            historyViewModel.insertHistoryItem(HistoryItem(0, oid, itemContentType, itemTitle,
                description, imageUrl, url, currentTime))
        }

        song = Song(itemTitle, description, url?:"", imageUrl, oid)

        if (imageUrl != "" && imageUrl != "null") {
            Picasso.get().load(imageUrl).resize(1000, 0)
                .onlyScaleDown().into(podcastThumbnailView, ImageCallback(podcastThumbnailView))
        }
        podcastTitleView.text = itemTitle
        podcastTitleView.isSelected = true
        populateHeader(itemTitle, description, imageUrl)
        populateEpisodes(oid)

        handler.postDelayed({registerInterest(oid, itemContentType, episodeIndex)}, (15*1000).toLong())

        if (triggeredByNotification) {
            val notificationLogEventType = when(itemContentType) {
                "article" -> LogEventType.ARTICLE_NOTIFICATION_CLICK
                "book" -> LogEventType.BOOK_NOTIFICATION_CLICK
                "podcast" -> LogEventType.PODCAST_NOTIFICATION_CLICK
                else -> LogEventType.NONE
            }
            logEventViewModel.insertEvent(
                LogEvent(0, notificationLogEventType,
                    currentTime, null, false)
            )
        }
    }

    private fun getItemFromRecommendation(eventType: String): String {
        var item = ""
        val response = getItemResponse(eventType)
        if (response != null && response.isSuccessful && response.body != null) {
            val responseBody = response.body!!.bytes()
            response.body!!.close()
            val responseDecompressed = gzipHelper.unzip(responseBody)
            when(eventType) {
                "podcast" -> {
                    val podcasts = gson.fromJson(responseDecompressed, Array<Podcast>::class.java)
                    val podcast = podcasts[0]
                    if (podcast.entries.isNotEmpty()) {
                        val episode = podcast.entries[0]
                        itemUrl = episode.link
                        var podcastTitle = episode.title
                        podcastTitle = podcastTitle.replace("<[^>]*>".toRegex(), "")
                        podcastTitle = podcastTitle.replace("|", "")
                        var podcastDescription = episode.description
                        podcastDescription = podcastDescription.replace("<[^>]*>".toRegex(), "")
                        podcastDescription = podcastDescription.replace("|", "")
                        item = "podcast|${podcastTitle}|${podcastDescription}|" +
                                "${podcast._id.oid}||${episode.image_link}"
                    }
                }
            }
        }
        return item
    }

    private fun getItemResponse(eventType: String): Response? {
        return try {
            val urlBuilder: HttpUrl.Builder = getItemUrl.toHttpUrl().newBuilder()
            urlBuilder.addQueryParameter("contentType", eventType)
            urlBuilder.addQueryParameter("oid", "")
            val whitelistItems = whitelistModel.getAllWhitelistItemsAsync()
            var blacklistJson = ""
            for (item in whitelistItems) {
                if (!item.allowed) {
                    blacklistJson += "\"${item.source.name}\","
                }
            }
            blacklistJson = removeSuperfluousComma(blacklistJson)
            urlBuilder.addQueryParameter("blacklist", "[$blacklistJson]")
            val difficulty =
                contentPreferenceModel.getContentPreferenceByKeyAsync("difficulty")
            if (difficulty != null) {
                urlBuilder.addQueryParameter("difficulty", difficulty.value.toString())
            }
            else {
                urlBuilder.addQueryParameter("difficulty", "2")
            }
            val request: Request = Request.Builder().url(urlBuilder.build())
                .header("participantId", Constants.participantId).build()
            val response: Response = client.newCall(request).execute()
            response
        } catch (e: SocketTimeoutException) {
            Log.e("notification", e.toString())
            null
        }
    }

    private fun removeSuperfluousComma(itemJson: String): String {
        val cleanedItems = arrayListOf<String>()
        val items = itemJson.split(",")
        for (item in items) {
            if (item == "") {
                continue
            }
            cleanedItems.add(item)
        }
        return cleanedItems.joinToString(",")
    }

    fun populateHeader(headerTitle: String, headerDescription: String,
                               headerImageUrl: String) {
        val episodesContainer = findViewById<ListView>(R.id.episodesContainer)
        if (entries.isEmpty()) {
            entries.add("$headerTitle|$headerDescription|$headerImageUrl||")
        }
        else {
            entries.removeAt(0)
            entries.add(0, "$headerTitle|$headerDescription|$headerImageUrl||")
        }
        val episodeAdapter = PodcastEpisodeAdapter(this, entries)
        episodesContainer.adapter = episodeAdapter
    }

    private fun populateEpisodes(oid: String) {
        Thread {
            val urlBuilder: HttpUrl.Builder =
                getItemUrl.toHttpUrlOrNull()!!
                    .newBuilder()
            urlBuilder.addQueryParameter("oid", oid)
            urlBuilder.addQueryParameter("contentType", "podcast")
            val request: Request = Request.Builder().url(urlBuilder.build())
                .header("participantId", Constants.participantId).build()
            try {
                val response: Response = client.newCall(request).execute()
                if (response.isSuccessful && response.body != null) {
                    val responseBody = response.body!!.bytes()
                    response.body!!.close()
                    val responseDecompressed = gzipHelper.unzip(responseBody)
                    val podcast = gson.fromJson(responseDecompressed, Podcast::class.java)
                    val episodesContainer: ListView = findViewById(R.id.episodesContainer)
                    for (episode in podcast.entries) {
                        val title = episode.title
                        val description = episode.description.replace("<[^>]*>".toRegex(), "")
                        val imageLink = episode.image_link
                        val episodeLink = episode.link
                        entries.add("$title|$description|$imageLink|$episodeLink|$oid")
                    }
                    runOnUiThread {
                        if (episodesContainer.isNotEmpty() &&
                                    episodesContainer.adapter is ArrayAdapter<*>) {
                            (episodesContainer.adapter as ArrayAdapter<*>).notifyDataSetChanged()
                        } else {
                            val episodeAdapter = PodcastEpisodeAdapter(this, entries)
                            episodesContainer.adapter = episodeAdapter
                        }
                    }
                }
            }
            catch (e: SocketTimeoutException) {
                Log.e("API", "Timeout for Podcast Episode retrieval")
            }
        }.start()
    }

    private fun initializePassword() {

        val sharedPref = this.getSharedPreferences(getString(R.string.preference_file_key),
            Context.MODE_PRIVATE)
        if (sharedPref.contains(getString(R.string.participant_id_key))) {
            Log.i("participant_id", "Successfully retrieved participant id")
            Constants.participantId =
                sharedPref.getString(getString(R.string.participant_id_key), "") ?: ""
        } else {
            Log.e("preferences", "no password found")
            Toast.makeText(this, "No user ID found :(", Toast.LENGTH_SHORT).show()
        }
    }

    // create an action bar button
    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // R.menu.mymenu is a reference to an xml file named mymenu.xml which should be inside your res/menu directory.
        // If you don't have res/menu, just create a directory named "menu" inside res
        menuInflater.inflate(R.menu.favorite_menu, menu)
        if (this::itemOid.isInitialized && this::itemContentType.isInitialized) {
            Thread {
                val favoriteItem = favoriteViewModel.getItemByOidAndContentType(itemOid, itemContentType)
                if (favoriteItem != null) {
                    if (favoriteItem.isFavorite) {
                        runOnUiThread {
                            setFavoriteIcon(menu.findItem(R.id.favoriteButton), true)
                            menu.findItem(R.id.favoriteButton).isChecked = true
                        }
                    } else {
                        runOnUiThread {
                            setFavoriteIcon(menu.findItem(R.id.favoriteButton), false)
                            menu.findItem(R.id.favoriteButton).isChecked = false
                        }
                    }
                }
                else {
                    lifecycleScope.launch {
                        favoriteViewModel.insertFavorite(Favorite(0, itemOid, itemContentType, false))
                    }
                    runOnUiThread {
                        setFavoriteIcon(menu.findItem(R.id.favoriteButton), false)
                        menu.findItem(R.id.favoriteButton).isChecked = false
                    }
                }
            }.start()
        }
        return super.onCreateOptionsMenu(menu)
    }

    // handle button activities
    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        val id = item.itemId
        if (id == R.id.favoriteButton && this::itemOid.isInitialized &&
            this::itemContentType.isInitialized) {
            if (item.isChecked) {
                setFavoriteIcon(item, false)
                item.isChecked = false
                Toast.makeText(this, "Removed from Favorites", Toast.LENGTH_SHORT).show()
                lifecycleScope.launch {
                    favoriteViewModel.updateStatus(false, itemOid, itemContentType)
                }
                val logEventType = LogEventType.REMOVE_FAVORITE
                val currentTime = System.currentTimeMillis()
                logEventViewModel.insertEvent(LogEvent(0, logEventType, currentTime, itemOid,
                    false))
            }
            else {
                setFavoriteIcon(item, true)
                item.isChecked = true
                Toast.makeText(this, "Added to Favorites", Toast.LENGTH_SHORT).show()
                lifecycleScope.launch {
                    favoriteViewModel.updateStatus(true, itemOid, itemContentType)
                }
                val logEventType = LogEventType.ADD_FAVORITE
                val currentTime = System.currentTimeMillis()
                logEventViewModel.insertEvent(LogEvent(0, logEventType, currentTime, itemOid,
                    false))
            }
        }
        return super.onOptionsItemSelected(item)
    }

    private fun setFavoriteIcon(item: MenuItem, isFavorite: Boolean) {
        if (isFavorite) {
            item.icon =
                ContextCompat.getDrawable(
                    applicationContext,
                    R.drawable.baseline_favorite_black_24dp
                )
        }
        else {
            item.icon =
                ContextCompat.getDrawable(
                    applicationContext,
                    R.drawable.baseline_favorite_border_black_24dp
                )
        }
        item.icon.setTint(resources.getColor( R.color.live_red, resources.newTheme()))
    }

    override fun onActionModeStarted(mode: ActionMode) {
        super.onActionModeStarted(mode)
        if (mActionMode == null) {
            mActionMode = mode
            val menu: Menu = mode.getMenu()
            // Remove the default menu items (select all, copy, paste, search)
            //menu.clear()

            val episodesContainer = findViewById<ListView>(R.id.episodesContainer)

            menu.add(0, DICTIONARY, 0, "Dictionary / Translate")
                .setIcon(R.drawable.baseline_menu_book_black_48dp)
                .setOnMenuItemClickListener {
                    val textView = findViewById<TextView>(R.id.podcastDescription)
                    val logEventType = LogEventType.TRANSLATOR_CLICK
                    val currentTime = System.currentTimeMillis()
                    logEventViewModel.insertEvent(LogEvent(0, logEventType, currentTime, itemOid,
                        false))
                    if (textView.hasSelection()) {
                            var min = 0
                            var max: Int = textView.getText().length
                            if (textView.isFocused()) {
                                val selStart: Int = textView.getSelectionStart()
                                val selEnd: Int = textView.getSelectionEnd()
                                min = Math.max(0, Math.min(selStart, selEnd))
                                max = Math.max(0, Math.max(selStart, selEnd))
                            }
                            // Perform your definition lookup with the selected text
                            var selectedText: CharSequence =
                                textView.getText().subSequence(min, max)
                            selectedText = selectedText.toString()
                            if (selectedText.length < 2000) {
                                showDictionaryPopup(selectedText, episodesContainer)
                            } else {
                                Toast.makeText(this,
                                    "The selection is too long. Max length: 2000 characters",
                                    Toast.LENGTH_LONG).show()
                            }
                        true
                        } else {
                        false
                    }
                }
            // id for 'Share'
            menu.removeItem(34275980)
            menu.removeItem(android.R.id.shareText)
            menu.removeItem(android.R.id.textAssist)

            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.S) {
                mode.invalidate()
            }
            // Inflate your own menu items
            //mode.getMenuInflater().inflate(R.menu.my_custom_menu, menu)
        }
    }

    override fun onActionModeFinished(mode: ActionMode) {
        super.onActionModeFinished(mode)
        mActionMode = null
    }

    private fun showDictionaryPopup(selectedText: String, parentView: View) {
        thread {
            val phrasesToSave = arrayListOf<Phrase>()
            val dialogPopup: View = layoutInflater.inflate(R.layout.popup_dictionary, null)
            val titleView = dialogPopup.findViewById<TextView>(R.id.dictPopupTitle)
            val textContainer = dialogPopup.findViewById<LinearLayoutCompat>(R.id.dictPopupTextContainer)
            val saveButton = dialogPopup.findViewById<Button>(R.id.saveWordParagraph)
            var popupHeader: String
            var processedText: String
            if (selectedText.trim().split(" ").size > 1){
                popupHeader = "Automatic Translation:"
                val translation = fetchTranslation(selectedText)
                if (translation != null) {
                    processedText = translation.translatedText
                    saveButton.text = resources.getString(R.string.save_to_my_paragraphs_list)
                    phrasesToSave.add(Phrase(0, selectedText, selectedText,
                        translation.translatedText, null, null, null))
                }
                else {
                    processedText = "No Translation Found :("
                    runOnUiThread {
                        saveButton.isEnabled = false
                    }
                }
                val paragraphContainer = layoutInflater.inflate(R.layout.layout_paragraph_translation,
                    textContainer, false) as ConstraintLayout
                val originalText = paragraphContainer.findViewById<TextView>(R.id.original)
                originalText.visibility = View.VISIBLE
                originalText.text = selectedText.trim()
                val textView = paragraphContainer.findViewById<TextView>(R.id.phrase)
                textView.text = processedText
                textContainer.addView(paragraphContainer)
            }
            else {
                runOnUiThread {
                    saveButton.isEnabled = false
                }
                popupHeader = "Dictionary Entry:"
                val dictionaryResults = fetchDictionaryData(selectedText.trim(), "dedx", "de")
                if (dictionaryResults != null) {
                    val dictionaryHits: Array<HitData> = dictionaryResults.hits
                    for (dictHit in dictionaryHits.take(3)) {
                        if (dictHit.roms == null) {
                            continue
                        }
                        for (rom in dictHit.roms) {
                            val syllables = rom.headword
                            val cleanedText = syllables.replace("[^A-Za-zÀ-ſ]".toRegex(), "")
                            val wordClass = rom.wordclass
                            val examples = arrayListOf<String>()
                            processedText = "${cleanedText}, Type:  ${wordClass}"
                            val wordCandidate = layoutInflater.inflate(R.layout.layout_word_candidate,
                                textContainer, false) as ConstraintLayout
                            val phraseView = wordCandidate.findViewById<TextView>(R.id.phrase)
                            phraseView.text = processedText
                            val syllablesView = wordCandidate.findViewById<TextView>(R.id.syllables)
                            syllablesView.text = syllables
                            val examplesContainer = wordCandidate.findViewById<LinearLayoutCompat>(R.id.examplesContainer)
                            for (arabs in rom.arabs) {
                                for (translation in arabs.translations) {
                                    val example = createExampleView(translation)
                                    examples.add(example.text.toString())
                                    examplesContainer.addView(example)
                                }
                            }
                            var translation = ""
                            val translationView =
                                wordCandidate.findViewById<TextView>(R.id.wordTranslation)
                            val translationResult =
                                fetchTranslation(rom.headword.replace("[^A-Za-zÀ-ſ]".toRegex(), ""))
                            if (translationResult != null) {
                                translationView.text = translationResult.translatedText
                                translation = translationResult.translatedText
                            }
                            else {
                                translationView.visibility = View.INVISIBLE
                            }
                            val checkBox = wordCandidate.findViewById<CheckBox>(R.id.phraseCheckbox)
                            checkBox.setOnClickListener {
                                val phrase = Phrase(0, selectedText, cleanedText,
                                    translation, syllables, wordClass, examples)
                                runOnUiThread {
                                    it as CheckBox
                                    if (it.isChecked) {
                                        phrasesToSave.add(phrase)
                                    }
                                    else {
                                        var deleteIndex = 0
                                        var found = false
                                        for (phrase in phrasesToSave) {
                                            if (phrase.phrase == cleanedText) {
                                                found = true
                                                break
                                            }
                                            deleteIndex += 1
                                        }
                                        if (found) {
                                            phrasesToSave.removeAt(deleteIndex)
                                        }
                                    }
                                    saveButton.isEnabled = phrasesToSave.size > 0 || it.isChecked
                                }
                            }
                            wordCandidate.layoutParams = wordCandidate.layoutParams.apply {
                                width = LinearLayout.LayoutParams.MATCH_PARENT
                                height = LinearLayout.LayoutParams.WRAP_CONTENT
                            }
                            textContainer.layoutParams = textContainer.layoutParams.apply {
                                width = LinearLayout.LayoutParams.MATCH_PARENT
                                height = LinearLayout.LayoutParams.WRAP_CONTENT
                            }
                            textContainer.addView(wordCandidate)
                        }
                    }
                    saveButton.text = resources.getString(R.string.save_to_my_words_list)
                }
                else {
                    val translation =
                        fetchTranslation(selectedText.replace("[^A-Za-zÀ-ſ]".toRegex(), ""))
                    if (translation != null) {
                        popupHeader = "No match found, automatic translation:"
                        processedText = translation.translatedText
                    }
                    else {
                        popupHeader = "No match found"
                        processedText = ""
                    }
                    val wordCandidate = layoutInflater.inflate(R.layout.layout_word_candidate,
                        textContainer, false) as ConstraintLayout
                    val textView = wordCandidate.findViewById<TextView>(R.id.phrase)
                    textView.text = selectedText
                    val translationView = wordCandidate.findViewById<TextView>(R.id.wordTranslation)
                    translationView.text = processedText
                    val syllables = wordCandidate.findViewById<TextView>(R.id.syllables)
                    syllables.visibility = View.GONE
                    val explanations = wordCandidate.findViewById<TextView>(R.id.examplesHeader)
                    explanations.visibility = View.GONE
                    val checkBox = wordCandidate.findViewById<CheckBox>(R.id.phraseCheckbox)
                    checkBox.setOnClickListener {
                        val phrase = Phrase(0, selectedText, selectedText, processedText,
                            null, null, null)
                        runOnUiThread {
                            it as CheckBox
                            if (it.isChecked) {
                                phrasesToSave.add(phrase)
                            }
                            else {
                                var deleteIndex = 0
                                var found = false
                                for (phrase in phrasesToSave) {
                                    if (phrase.phrase == selectedText) {
                                        found = true
                                        break
                                    }
                                    deleteIndex += 1
                                }
                                if (found) {
                                    phrasesToSave.removeAt(deleteIndex)
                                }
                            }
                            saveButton.isEnabled = phrasesToSave.size > 0 || it.isChecked
                        }
                    }
                    wordCandidate.layoutParams = wordCandidate.layoutParams.apply {
                        width = LinearLayout.LayoutParams.MATCH_PARENT
                        height = LinearLayout.LayoutParams.WRAP_CONTENT
                    }
                    textContainer.addView(wordCandidate)
                    textContainer.layoutParams = textContainer.layoutParams.apply {
                        width = LinearLayout.LayoutParams.MATCH_PARENT
                        height = LinearLayout.LayoutParams.WRAP_CONTENT
                    }
                }
            }
            val googleTranslateBar =
                layoutInflater.inflate(R.layout.layout_google_translate_bar,
                    textContainer, false)
            textContainer.addView(googleTranslateBar)

            val width = ViewGroup.LayoutParams.WRAP_CONTENT
            val height = ViewGroup.LayoutParams.WRAP_CONTENT
            val popupWindow = PopupWindow(dialogPopup, width, height, true)
            popupWindow.elevation = 40f

            saveButton.setOnClickListener {
                for (phrase in phrasesToSave) {
                    phrasesViewModel.insertPhrase(phrase)
                }
                val logEventType = LogEventType.ADD_WORD_OR_PHRASE
                val currentTime = System.currentTimeMillis()
                logEventViewModel.insertEvent(LogEvent(0, logEventType, currentTime,
                    null, false))
                Toast.makeText(this, "Added to Words and Phrases", Toast.LENGTH_SHORT).show()
                popupWindow.dismiss()
            }
            titleView.text = popupHeader

            runOnUiThread {
                if (!this.isDestroyed) {
                    popupWindow.showAtLocation(parentView, Gravity.CENTER, 0, 0)
                    popupWindow.dimBehind()
                }
            }
        }
    }

    private fun createExampleView(translation: DictTranslationData): TextView {
        val params = LinearLayout.LayoutParams(
            ViewGroup.LayoutParams.WRAP_CONTENT,
            ViewGroup.LayoutParams.MATCH_PARENT
        )
        params.setMargins(0, 40, 0, 40)
        val example = TextView(this)
        example.layoutParams = params
        example.foregroundTintMode
        example.text = translation.source.replace("<[^>]*>".toRegex(), "")
        return example
    }

    fun fetchTranslation(textInput: String, keepFormat:Boolean=false): Translation? {
        var input: String
        if (!keepFormat) {
            input = textInput.replace("[^a-zA-ZÀ-ſ]".toRegex(), " ")
            input = input.replace(" +".toRegex(), " ")
        }
        else {
            input = textInput.replace("[\"]".toRegex(), "“")
            input = input.replace("[^0-9a-zA-ZÀ-ſ .;:,'»«„“›‹‟”()</>\n-]".toRegex(), " ")
            input = input.replace(" +".toRegex(), " ")
        }
        val MEDIA_TYPE_MARKDOWN = "application/json; charset=utf-8".toMediaType()
        val postBody = "{ q: \"$input\", target: \"en\", format: \"html\" }"
        val request = Request.Builder()
            .url("https://translation.googleapis.com/language/translate/v2" +
                    "?key=${Constants.GoogleApiKey}")
            .post(postBody.toRequestBody(MEDIA_TYPE_MARKDOWN))
            .build()

        client.newCall(request).execute().use { response ->
            if (response.isSuccessful && response.body != null) {
                val translationResponse =
                    gson.fromJson(response.body!!.string(), TranslationResponse::class.java)
                response.body!!.close()
                return if (translationResponse.data.translations.size > 0){
                    translationResponse.data.translations[0].translatedText =
                        Html.fromHtml(translationResponse.data.translations[0].translatedText,
                            HtmlCompat.FROM_HTML_MODE_LEGACY).toString()
                    translationResponse.data.translations[0]
                } else {
                    null
                }
            }
            else {
                Log.e("Google Translate", "Error while translating: ${response}")
            }
        }
        return null
    }

    private fun fetchDictionaryData(word: String, dictionary: String, inputLang: String): DictionaryData? {
        /***
         * Warning: this function has to be called in a background thread!!!
         */
        var dictionaryData: DictionaryData? = null
        val request = Request.Builder()
            .url("https://api.pons.com/v1/dictionary?q=$word&l=$dictionary&in=$inputLang&ref=true")
            .header("X-Secret", Constants.PonsApiKey)
            .build()

        client.newCall(request).execute().use { response ->
            if (!response.isSuccessful || response.body == null) throw IOException("Unexpected code $response")
            val body = response.body!!.string()
            response.body!!.close()
            val dictionaryListType: Type = object : TypeToken<ArrayList<DictionaryData?>?>() {}.type
            val dictionaryDataList: List<DictionaryData>? = Gson().fromJson(body, dictionaryListType)
            if (dictionaryDataList != null && dictionaryDataList.isNotEmpty()) {
                dictionaryData = dictionaryDataList[0]
            }
        }
        return dictionaryData
    }

    fun PopupWindow.dimBehind() {
        val container = contentView.rootView
        val context = contentView.context
        val wm = context.getSystemService(Context.WINDOW_SERVICE) as WindowManager
        val p = container.layoutParams as WindowManager.LayoutParams
        p.flags = p.flags or WindowManager.LayoutParams.FLAG_DIM_BEHIND
        p.dimAmount = 0.3f
        wm.updateViewLayout(container, p)
    }

    private fun registerInterest(oid: String, contentType: String, episodeIndex: String) {
        Thread{
            try {
                val formBody = FormBody.Builder()
                    .add("oid", oid)
                    .add("contentType", contentType)
                    .add("episodeIndex", episodeIndex).build()
                val request: Request = Request.Builder().url(registerInterestUrl)
                    .put(formBody)
                    .header("participantId", Constants.participantId).build()
                val response: Response = client.newCall(request).execute()
                if (response.code == 200) {
                    Log.d("API", "Registered Interest")
                }
            }
            catch (e: SocketTimeoutException) {
                Log.e("API", "Registering interest timed out!")
            }
        }.start()
    }
}