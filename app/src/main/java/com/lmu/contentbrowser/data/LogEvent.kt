package com.lmu.contentbrowser.data

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "logs")
data class LogEvent (
    @PrimaryKey(autoGenerate = true) val id: Long,
    @ColumnInfo(name= "type") val type: LogEventType,
    @ColumnInfo(name= "time") val time: Long,
    @ColumnInfo(name= "oid") val oid: String?,
    @ColumnInfo(name= "synced") val synced: Boolean = false
    )