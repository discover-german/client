package com.lmu.contentbrowser.data

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import kotlinx.coroutines.flow.Flow

@Dao
interface FavoriteDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(favorite: Favorite): Long

    @Query("SELECT * FROM favorites")
    fun getAllItems(): Flow<List<Favorite>>

    @Query("DELETE FROM favorites WHERE id = :id")
    suspend fun deleteItem(id: Long)

    @Query("DELETE FROM favorites")
    suspend fun deleteAll()

    @Query("SELECT * FROM favorites WHERE oid = :oid AND contentType = :contentType")
    fun getFavoriteByOidAndContentType(oid: String, contentType: String): Favorite?

    @Query("UPDATE favorites SET isFavorite = :isFavorite WHERE oid = :oid AND contentType = :contentType")
    suspend fun updateIsFavoriteByOidAndContentType(isFavorite: Boolean, oid: String,
                                                    contentType: String)

    @Query("SELECT COUNT(*) FROM favorites WHERE isFavorite")
    fun getCount(): Int
}