package com.lmu.contentbrowser.data

data class Episode(
    val title: String,
    val link: String,
    val description: String,
    val published: String,
    val image_link: String
)