package com.lmu.contentbrowser.viewmodels

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.asLiveData
import androidx.lifecycle.viewModelScope
import com.lmu.contentbrowser.data.Source
import com.lmu.contentbrowser.data.WhitelistItem
import com.lmu.contentbrowser.database.MyRepository
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.launch

class WhitelistViewModel (private val repository: MyRepository) : ViewModel() {
    fun insertWhitelistItem(whitelistItem: WhitelistItem) = viewModelScope.launch {
        repository.insertWhitelistItem(whitelistItem)
    }

    fun deleteWhitelistItem(source: Source) = viewModelScope.launch {
        repository.deleteWhitelistItem(source)
    }

    fun getWhitelistItemAsync(source: Source): WhitelistItem? {
        return repository.getWhitelistItemAsync(source)
    }

    fun getAllWhitelistItemsAsync(): List<WhitelistItem> {
        return repository.getAllWhitelistItemsAsync()
    }


}

class WhitelistViewModelFactory(private val repository: MyRepository) : ViewModelProvider.Factory {
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(WhitelistViewModel::class.java)) {
            @Suppress("UNCHECKED_CAST")
            return WhitelistViewModel(repository) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }
}