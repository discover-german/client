package com.lmu.contentbrowser.data

data class similarItem(
    val item: ContentItem,
    val score: Float
)