package com.lmu.contentbrowser.activities

import android.app.AlarmManager
import android.content.Context
import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.provider.Settings.ACTION_REQUEST_SCHEDULE_EXACT_ALARM
import android.util.Log
import android.widget.ListView
import androidx.activity.viewModels
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.findNavController
import androidx.navigation.ui.setupWithNavController
import androidx.work.*
import com.awesome.germanhelper.NotificationHelper
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.google.gson.Gson
import com.lmu.contentbrowser.Constants
import com.lmu.contentbrowser.ContentBrowserApplication
import com.lmu.contentbrowser.R
import com.lmu.contentbrowser.data.Event
import com.lmu.contentbrowser.data.EventMeta
import com.lmu.contentbrowser.data.LogEvent
import com.lmu.contentbrowser.data.LogEventType
import com.lmu.contentbrowser.dialogs.EmailReminderDialog
import com.lmu.contentbrowser.dialogs.LoginDialog
import com.lmu.contentbrowser.fragments.FavoritesFragment
import com.lmu.contentbrowser.fragments.HomeFragment
import com.lmu.contentbrowser.fragments.ToolsFragment
import com.lmu.contentbrowser.receivers.NotificationBroadcastReceiver
import com.lmu.contentbrowser.viewmodels.EventViewModel
import com.lmu.contentbrowser.viewmodels.EventViewModelFactory
import com.lmu.contentbrowser.viewmodels.LogEventViewModel
import com.lmu.contentbrowser.viewmodels.LogEventViewModelFactory
import okhttp3.FormBody
import okhttp3.OkHttpClient
import okhttp3.Request
import okhttp3.Response
import java.lang.Exception
import java.time.*
import java.util.concurrent.TimeUnit


class MainActivity : AppCompatActivity() {

    private val logsUrl = Constants.SERVER_URL + "/add_logs"
    private val client: OkHttpClient =
        OkHttpClient.Builder().connectTimeout(Constants.CONNECT_TIMEOUT, TimeUnit.SECONDS).build()
    private val gson: Gson = Gson()

    val logEventViewModel: LogEventViewModel by viewModels {
        LogEventViewModelFactory((this.application
                as ContentBrowserApplication).repository)
    }

    val eventViewModel: EventViewModel by viewModels {
        EventViewModelFactory((application as ContentBrowserApplication).repository)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        supportActionBar?.hide()

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.S) {
            requestAlarmPermissions()
        }

        if (!NotificationBroadcastReceiver.running) {
            NotificationHelper().setupNotificationReceiver(this)
            NotificationBroadcastReceiver.running = true
        }

        if (Constants.participantId == "") {
            initializePassword()
        }
        else {
            setupFragmentsAndBottomNavBar()
        }

        logEventViewModel.unsyncedLogEvents.observe(this) {
            if (it!=null) {
                syncLogs(it)
            }
        }
    }

    override fun onResume() {
        super.onResume()
        logEventViewModel.insertEvent(LogEvent(0, LogEventType.APP_ON_RESUME,
            System.currentTimeMillis(), null, false))
    }

    override fun onPause() {
        super.onPause()
        logEventViewModel.insertEvent(LogEvent(0, LogEventType.APP_ON_PAUSE,
            System.currentTimeMillis(), null, false))
    }

    private fun initializePassword() {

        val sharedPref = this.getSharedPreferences(getString(R.string.preference_file_key),
            Context.MODE_PRIVATE)
        if (sharedPref.contains(getString(R.string.participant_id_key))) {
            Log.i("participant_id", "Successfully retrieved participant id")
            Constants.participantId =
                sharedPref.getString(getString(R.string.participant_id_key), "") ?: ""
            setupFragmentsAndBottomNavBar()
        } else {
            Log.i(
                "participant_id",
                "No participant id stored. Asking user to input id."
            )
            val loginDialog = LoginDialog(this)
            loginDialog.show(supportFragmentManager, "login")
        }
    }

    fun setupFragmentsAndBottomNavBar(){
        val navController = findNavController(R.id.nav_host_fragment)
        val bottomNavigationView: BottomNavigationView = findViewById(R.id.bottomNavigationView)
        bottomNavigationView.setupWithNavController(navController)

        bottomNavigationView.setOnItemReselectedListener { item ->
            if (item.itemId == R.id.homeFragment) {
                findViewById<ListView>(android.R.id.list).smoothScrollToPosition(0)
            }
        }
    }

    fun refreshCurrentFragment() {
        val navController = findNavController(R.id.nav_host_fragment)
        val id = navController.currentDestination?.id
        navController.popBackStack(id!!,true)
        navController.navigate(id)
    }

    @RequiresApi(Build.VERSION_CODES.S)
    private fun requestAlarmPermissions() {
        val alarmManager =
            (getSystemService(AppCompatActivity.ALARM_SERVICE) as AlarmManager)
        if (!alarmManager.canScheduleExactAlarms()) {
            startActivity(Intent(ACTION_REQUEST_SCHEDULE_EXACT_ALARM))
        }
    }

    private fun syncLogs(logs: List<LogEvent>) {
        Thread{
            try {
                if (logs.isNotEmpty()) {
                    val logEventsSerialised = gson.toJson(logs)
                    val formBody = FormBody.Builder()
                        .add("logs", logEventsSerialised).build()
                    if (Constants.participantId == "") {
                        tryToGetParticipantId()
                    }
                    val request: Request = Request.Builder().url(logsUrl)
                        .put(formBody)
                        .header("participantId", Constants.participantId).build()
                    val response: Response = client.newCall(request).execute()
                    if (response.code == 200) {
                        Log.i("API", "Uploaded logs: ${logs.size}")
                        for (log in logs) {
                            val updatedLog = LogEvent(log.id, log.type, log.time, log.oid, true)
                            logEventViewModel.insertEvent(updatedLog)
                        }
                        if (response.body != null) {
                            response.body!!.close()
                        }
                    }
                    else {
                        Log.e("API", "Uploading logs unsuccessful")
                    }
                }
            }
            catch (e: Exception) {
                Log.e("API", "could not sync: $e")
            }
        }.start()
    }

    private fun tryToGetParticipantId() {
        val sharedPref = this.getSharedPreferences(getString(R.string.preference_file_key),
            Context.MODE_PRIVATE)
        if (sharedPref.contains(getString(R.string.participant_id_key))) {
            Log.i("participant_id", "Successfully retrieved participant id")
            Constants.participantId =
                sharedPref.getString(getString(R.string.participant_id_key), "") ?: ""
        }
    }

    fun insertDefaultDailyReminder() {
        val localDate = LocalDate.now()
        val epochTime =
            LocalDateTime.of(localDate, LocalTime.of(11, 0))
                .toEpochSecond(OffsetDateTime.now().offset)
        val event = Event(0, "random")
        val eventMeta11 = EventMeta(0, 0, "repeat_start", epochTime)
        val eventMeta12 =
            EventMeta(0, 0, "repeat_interval", 86400) //Max Value: almost non-repeating, 7 days: 604800, 1 day: 86400
        val eventMetas =
            listOf(mapOf("start" to eventMeta11, "interval" to eventMeta12))
        eventViewModel.insertEvent(event, eventMetas)
    }

    companion object {
        var selectedItemId: Int? = null
    }
}