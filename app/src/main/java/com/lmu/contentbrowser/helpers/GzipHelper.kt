package com.lmu.contentbrowser.helpers

import java.nio.charset.StandardCharsets.UTF_8
import java.util.zip.GZIPInputStream

class GzipHelper {
    fun unzip(content: ByteArray): String =
        GZIPInputStream(content.inputStream()).bufferedReader(UTF_8).use { it.readText() }
}