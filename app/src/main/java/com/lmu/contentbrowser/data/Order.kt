package com.lmu.contentbrowser.data

enum class Order {
    NEWEST, OLDEST, ALPHABETICAL
}