package com.lmu.contentbrowser

import java.text.DateFormat
import java.text.SimpleDateFormat
import java.time.format.DateTimeFormatter

class Constants {
    companion object {
        const val SERVER_URL = ""

        val simpleDateFormat = SimpleDateFormat.getDateTimeInstance(DateFormat.SHORT, DateFormat.SHORT)
        const val GoogleApiKey = ""
        const val PonsApiKey = ""
        const val NOTIFICATION_CHANNEL_ID = "contentBrowser"
        const val NOTIFICATION_CHANNEL_NAME = "Content Browser Notifications"
        const val NOTIFICATION_CHANNEL_DESCRIPTION = "Sends Notifications for German Language Learning"
        const val CONNECT_TIMEOUT = 10L
        var participantId: String = ""
    }
}