package com.lmu.contentbrowser.data

data class Podcast(
    val _id: ObjectId,
    val title: String,
    val link: String,
    val description: String,
    val entries: List<Episode>,
    val image_link: String
)
