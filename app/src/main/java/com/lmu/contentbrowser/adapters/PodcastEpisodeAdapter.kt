package com.lmu.contentbrowser.adapters

import android.app.Activity
import android.util.Log
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.ImageView
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.constraintlayout.widget.ConstraintLayout
import com.lmu.contentbrowser.R
import com.lmu.contentbrowser.activities.PodcastActivity
import com.lmu.contentbrowser.callbacks.ImageCallback
import com.lmu.contentbrowser.data.Song
import com.lmu.contentbrowser.services.MusicService
import com.squareup.picasso.Picasso
import java.lang.IllegalArgumentException

class PodcastEpisodeAdapter (private val context: Activity, private val entries: List<String>):
    ArrayAdapter<String>(context, R.layout.layout_row_back_to_back_translation, entries) {
    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        val entry = entries[position]
        var title = ""
        var description = ""
        var imageLink = ""
        var episodeLink = ""
        var oid = ""
        if (entry.split("|").size >= 5) {
            title = entry.split("|")[0]
            description = entry.split("|")[1]
            imageLink = entry.split("|")[2]
            episodeLink = entry.split("|")[3]
            oid = entry.split("|")[4]
        }

        val rowView: View
        if (position == 0) {
            rowView = context.layoutInflater.inflate(
                R.layout.layout_podcast_header,
                parent,
                false
            ) as ConstraintLayout
            val podcastTitle: TextView = rowView.findViewById(R.id.podcastTitle)
            val podcastThumbnailLarge: ImageView = rowView.findViewById(R.id.podcastThumbnailLarge)
            val podcastDescription: TextView = rowView.findViewById(R.id.podcastDescription)

            podcastTitle.text = title
            podcastDescription.text = description
            if (imageLink != "" && imageLink != "null") {
                Picasso.get().load(imageLink).resize(1000, 0).onlyScaleDown()
                    .into(podcastThumbnailLarge, ImageCallback(podcastThumbnailLarge))
            }
        } else {
            rowView = context.layoutInflater.inflate(
                R.layout.layout_podcast_episode,
                parent,
                false
            ) as ConstraintLayout

            val podcastTitleSmall: TextView =
                rowView.findViewById(R.id.podcastTitleSmall)
            val podcastDescriptionSmall: TextView =
                rowView.findViewById(R.id.podcastDescriptionSmall)
            val podcastThumbnailSmall: ImageView =
                rowView.findViewById(R.id.podcastThumbnailSmall)

            podcastTitleSmall.text = title
            podcastDescriptionSmall.text = description
            try {
                Picasso.get().load(imageLink).into(
                    podcastThumbnailSmall,
                    ImageCallback(podcastThumbnailSmall)
                )
            }
            catch (e: IllegalArgumentException) {
                Log.e("Picasso", "image path was null: $e")
            }

            if (context is PodcastActivity) {
                val musicService = context.musicService
                rowView.setOnClickListener {
                    val newSong = Song(title, description, episodeLink,
                        imageLink, oid)
                    if (musicService != null) {
                        context.song = newSong
                        musicService.playSong(newSong)
                        Picasso.get().load(imageLink).into(context.podcastThumbnailView,
                            ImageCallback(context.podcastThumbnailView)
                        )
                        context.podcastTitleView.text = title
                        context.podcastTitleView.isSelected = true
                        context.populateHeader(title, description, imageLink)
                    }
                }
            }
        }
        return rowView
    }
}