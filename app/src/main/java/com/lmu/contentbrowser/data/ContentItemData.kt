package com.lmu.contentbrowser.data

data class ContentItemData(
    val entries: ArrayList<String>,
    val urls: ArrayList<String>,
    val contentTypes: ArrayList<String>
)