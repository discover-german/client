package com.lmu.contentbrowser.data

data class Recommendations (
    val articles: List<ArticleRecommendation>,
    val books: List<BookRecommendation>,
    val podcasts: List<PodcastRecommendation>
)