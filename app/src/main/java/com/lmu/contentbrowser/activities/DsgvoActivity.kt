package com.lmu.contentbrowser.activities

import android.content.res.Configuration
import android.os.Bundle
import android.text.Html
import android.util.Log
import android.webkit.WebView
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.core.text.HtmlCompat
import androidx.webkit.WebSettingsCompat
import androidx.webkit.WebViewFeature
import com.lmu.contentbrowser.R
import com.lmu.contentbrowser.fragments.WebViewFragment
import java.io.BufferedReader
import java.io.IOException
import java.io.InputStreamReader

class DsgvoActivity: AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_dsgvo)

        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        supportActionBar?.title = "Data Processing Statement"
        val webView = findViewById<WebView>(R.id.dsgvoContent)
        if (WebViewFeature.isFeatureSupported(WebViewFeature.FORCE_DARK)) {
            when (resources.configuration.uiMode and Configuration.UI_MODE_NIGHT_MASK) {
                Configuration.UI_MODE_NIGHT_YES -> {
                    WebSettingsCompat.setForceDark(webView.settings, WebSettingsCompat.FORCE_DARK_ON)
                }
                Configuration.UI_MODE_NIGHT_NO, Configuration.UI_MODE_NIGHT_UNDEFINED -> {
                    WebSettingsCompat.setForceDark(webView.settings, WebSettingsCompat.FORCE_DARK_AUTO)
                }
                else -> {

                }
            }
        }
        webView.loadUrl("file:///android_asset/DummyDisclosureStatement.html")
    }
}