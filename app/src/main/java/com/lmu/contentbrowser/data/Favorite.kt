package com.lmu.contentbrowser.data

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "favorites")
class Favorite (
    @PrimaryKey(autoGenerate = true) val id: Long,
    @ColumnInfo(name= "oid") val oid: String,
    @ColumnInfo(name= "contentType") val contentType: String,
    @ColumnInfo(name = "isFavorite") val isFavorite: Boolean
)