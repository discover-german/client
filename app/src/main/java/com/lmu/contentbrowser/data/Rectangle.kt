package com.lmu.contentbrowser.data

data class Rectangle(
    val bottom: Float,
    val left: Float,
    val right: Float,
    val top: Float
)