package com.lmu.contentbrowser.dialogs

import android.app.Dialog
import android.content.Context
import android.content.DialogInterface
import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.DialogFragment
import com.google.android.gms.auth.api.credentials.*
import com.google.android.gms.common.api.ApiException
import com.google.android.material.textfield.TextInputEditText
import com.lmu.contentbrowser.Constants
import com.lmu.contentbrowser.R
import com.lmu.contentbrowser.activities.UserManagementActivity

class ChangeParticipantIdDialog(private val userManagementActivity: UserManagementActivity): DialogFragment() {
    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        return activity?.let {
            val builder = AlertDialog.Builder(it)
            // Get the layout inflater
            val inflater = requireActivity().layoutInflater;

            // Inflate and set the layout for the dialog
            // Pass null as the parent view because its going in the dialog layout
            val dialogView = inflater.inflate(R.layout.layout_login_dialog, null)
            val participantIdInput = dialogView.findViewById<TextInputEditText>(R.id.participantIdInput)
            builder.setView(dialogView)
                // Add action buttons
                .setPositiveButton("Submit",
                    DialogInterface.OnClickListener { dialog, id ->
                        val participantId = participantIdInput.text.toString()
                        updateUser(participantId)
                    })
                .setNegativeButton("Cancel",
                    DialogInterface.OnClickListener { dialog, id ->
                        getDialog()?.cancel()
                    })
            builder.create()
        } ?: throw IllegalStateException("Activity cannot be null")
    }
    private fun updateUser(participantId: String) {
        Constants.participantId = participantId
        val sharedPref = activity?.getSharedPreferences(getString(R.string.preference_file_key),
            Context.MODE_PRIVATE) ?: return
        with (sharedPref.edit()) {
            putString(getString(R.string.participant_id_key), participantId)
            apply()
        }

        userManagementActivity.fillUserCard()
    }
}