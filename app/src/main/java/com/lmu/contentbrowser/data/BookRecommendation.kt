package com.lmu.contentbrowser.data

data class BookRecommendation (
    val recommendation: Book,
    val trigger: Book
)