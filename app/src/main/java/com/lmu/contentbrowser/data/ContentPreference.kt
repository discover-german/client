package com.lmu.contentbrowser.data

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "contentPreferences")
data class ContentPreference(
    @PrimaryKey @ColumnInfo(name= "key") val key: String,
    @ColumnInfo(name= "value") val value: Int
)
