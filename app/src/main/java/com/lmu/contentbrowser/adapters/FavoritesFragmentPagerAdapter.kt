package com.lmu.contentbrowser.adapters

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.lifecycle.Lifecycle
import androidx.viewpager2.adapter.FragmentStateAdapter
import com.lmu.contentbrowser.fragments.FavoritesTabFragment
import com.lmu.contentbrowser.fragments.HistoryTabFragment

class FavoritesFragmentPagerAdapter(fragmentManager: FragmentManager,
                                    lifecycle: Lifecycle):
    FragmentStateAdapter(fragmentManager, lifecycle) {


    override fun getItemCount(): Int {
        return 2
    }

    override fun createFragment(position: Int): Fragment {
        when(position) {
            0 -> return FavoritesTabFragment()
            1 -> return HistoryTabFragment()
            else -> return FavoritesTabFragment()
        }
    }
}