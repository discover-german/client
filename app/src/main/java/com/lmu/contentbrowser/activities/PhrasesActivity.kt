package com.lmu.contentbrowser.activities

import android.os.Bundle
import android.widget.ListView
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import com.lmu.contentbrowser.ContentBrowserApplication
import com.lmu.contentbrowser.R
import com.lmu.contentbrowser.adapters.PhrasesAdapter
import com.lmu.contentbrowser.data.LogEvent
import com.lmu.contentbrowser.data.LogEventType
import com.lmu.contentbrowser.viewmodels.LogEventViewModel
import com.lmu.contentbrowser.viewmodels.LogEventViewModelFactory
import com.lmu.contentbrowser.viewmodels.PhrasesViewModel
import com.lmu.contentbrowser.viewmodels.PhrasesViewModelFactory

class PhrasesActivity: AppCompatActivity() {
    val phraseViewModel: PhrasesViewModel by viewModels {
        PhrasesViewModelFactory((application as ContentBrowserApplication).repository)
    }
    val logEventViewModel: LogEventViewModel by viewModels {
        LogEventViewModelFactory((application as ContentBrowserApplication).repository)
    }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_phrases)

        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        val listView = findViewById<ListView>(R.id.listview)


        phraseViewModel.allPhrases.observe(this) {
            if (it == null || it.isEmpty()) {
                supportActionBar?.title = "No words or Phrases yet"
            }
            else {
                supportActionBar?.title = "My Words and Phrases"
            }
            val entries = arrayListOf<String>()
            for (phrase in it) {
                entries.add("${phrase.id}|${phrase.phrase}|${phrase.translation}|" +
                        "${phrase.syllables}|${phrase.wordClass}|" +
                        "${phrase.examples?.joinToString (separator = ";")}")
            }
            val phrasesAdapter = PhrasesAdapter(this, entries.toTypedArray())
            listView.adapter = phrasesAdapter
        }
    }

    override fun onResume() {
        super.onResume()
        val logEventType = LogEventType.PHRASES_START
        val currentTime = System.currentTimeMillis()
        logEventViewModel.insertEvent(
            LogEvent(0, logEventType, currentTime, null,
                false)
        )
    }

    override fun onPause() {
        super.onPause()
        val logEventType = LogEventType.PHRASES_END
        val currentTime = System.currentTimeMillis()
        logEventViewModel.insertEvent(
            LogEvent(0, logEventType, currentTime, null,
                false)
        )
    }
}