package com.lmu.contentbrowser.adapters

import android.app.Activity
import android.content.Intent
import android.graphics.drawable.GradientDrawable
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.content.res.AppCompatResources
import androidx.cardview.widget.CardView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.ContextCompat
import androidx.fragment.app.FragmentActivity
import com.lmu.contentbrowser.Constants
import com.lmu.contentbrowser.ContentBrowserApplication
import com.lmu.contentbrowser.R
import com.lmu.contentbrowser.activities.PodcastActivity
import com.lmu.contentbrowser.activities.ViewerActivity
import com.lmu.contentbrowser.viewmodels.HistoryViewModel
import com.squareup.picasso.Picasso
import kotlinx.coroutines.*
import kotlin.random.Random


class HistoryAdapter(private val context: Activity, private val entries: Array<String>):
    ArrayAdapter<String>(context, R.layout.layout_content_item, entries) {

    override fun getView(position: Int, view: View?, parent: ViewGroup): View {
        val inflater = context.layoutInflater
        val rowView = inflater.inflate(R.layout.layout_history_item, parent, false)

        var contentType = ""
        var title = ""
        var unixTime = ""
        var description = ""
        var oid = ""
        var imageLink = ""
        var url = ""
        var readableTime = ""
        var itemId = ""

        val entry: String = entries[position]
        if (entry.split("|").size == 8) {
            contentType = entry.split("|")[0]
            title = entry.split("|")[1]
            unixTime = entry.split("|")[2]
            description = entry.split("|")[3]
            oid = entry.split("|")[4]
            imageLink = entry.split("|")[5]
            url = entry.split("|")[6]
            itemId = entry.split("|")[7]

            readableTime = Constants.simpleDateFormat.format(unixTime.toLong())
        }

        val historyTitle = rowView.findViewById<TextView>(R.id.historyTitle)
        historyTitle.text = title

        val accessedAt = rowView.findViewById<TextView>(R.id.historyAccessTime)
        accessedAt.text = readableTime

        val historyConstraint = rowView.findViewById<ConstraintLayout>(R.id.historyConstraint)

        when(contentType) {
            "article" -> setGradientDrawable(historyConstraint, R.drawable.article_gradient)
            "book" -> setGradientDrawable(historyConstraint, R.drawable.book_gradient)
            "podcast" -> setGradientDrawable(historyConstraint, R.drawable.podcast_gradient)
            "error" -> setGradientDrawable(historyConstraint, R.drawable.error_gradient)
            else -> false
        }

        val historyImage = rowView.findViewById<ImageView>(R.id.historyImage)

        if (imageLink != "" && imageLink != "null") {
            Picasso.get().load(imageLink).resize(200, 0)
                .onlyScaleDown().into(historyImage)
        }
        else {
            historyImage.scaleType = ImageView.ScaleType.FIT_CENTER
            when(contentType) {
                "article" -> historyImage.setImageResource(R.drawable.baseline_newspaper_black_48dp)
                "book" -> historyImage.setImageResource(R.drawable.baseline_menu_book_black_48dp)
                "podcast" -> historyImage.setImageResource(R.drawable.baseline_podcasts_black_48dp)
                "error" -> historyImage.setImageResource(R.drawable.baseline_error_outline_black_24dp)
                else -> false
            }
        }

        val deleteButton = rowView.findViewById<CardView>(R.id.deleteHistoryItem)
        deleteButton.setOnClickListener {
            CoroutineScope(Dispatchers.IO).launch {
                (context.application as ContentBrowserApplication).repository.deleteHistoryItem(itemId.toLong())
            }
        }

        rowView.setOnClickListener {
            val intent = if (contentType == "podcast") {
                Intent(context, PodcastActivity::class.java)
            } else {
                Intent(context, ViewerActivity::class.java)
            }
            intent.putExtra("item", "${contentType}|${title}|${description}|" +
                    "${oid}|null|${imageLink}")
            intent.putExtra("url", url)
            context.startActivity(intent)
        }

        return rowView
    }

    private fun setColor(myView: View, color: Int) {
        context.runOnUiThread {
            myView.setBackgroundColor(ContextCompat.getColor(context,(color)))
        }
    }

    private fun setGradientDrawable(myView: View, drawable: Int) {
        context.runOnUiThread {
            val bg = AppCompatResources.getDrawable(context, drawable) as GradientDrawable
            val random = Random(System.nanoTime())
            val xVal = random.nextDouble(0.3, 0.7).toFloat()
            val yVal = random.nextDouble(0.3, 0.7).toFloat()
            bg.setGradientCenter(xVal, yVal)
            myView.background = bg
        }
    }
}