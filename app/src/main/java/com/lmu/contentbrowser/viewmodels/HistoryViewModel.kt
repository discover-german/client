package com.lmu.contentbrowser.viewmodels

import androidx.lifecycle.*
import androidx.room.FtsOptions
import com.lmu.contentbrowser.data.HistoryItem
import com.lmu.contentbrowser.database.MyRepository
import kotlinx.coroutines.launch

class HistoryViewModel (private val repository: MyRepository) : ViewModel() {

    val allHistoryItems = repository.allHistoryItems.asLiveData()
    val searchResults: MutableLiveData<List<HistoryItem>> by lazy {
        MutableLiveData<List<HistoryItem>>()
    }

    suspend fun insertHistoryItem(historyItem: HistoryItem): Long {
        return repository.insertHistoryItem(historyItem)
    }

    suspend fun getHistoryItemById(id: Long): HistoryItem {
        return repository.getHistoryItemById(id)
    }

    suspend fun deleteHistoryItem(id: Long) {
        repository.deleteHistoryItem(id)
    }

    suspend fun searchHistoryItem(query: String): List<HistoryItem> {
        return repository.searchInHistory(query)
    }

    fun searchWithScore(query: String?) {
        // 1
        viewModelScope.launch {
            // 2
            if (query.isNullOrBlank()) {
                // 3
                searchResults.postValue(arrayListOf())
            } else {
                // 4
                val sanitizedQuery = sanitizeSearchQuery(query)
                repository.searchWithMatchInfo(sanitizedQuery).let { results ->
                    // 5
                    results.sortedByDescending { result -> calculateScore(result.matchInfo) }
                        // 6
                        .map { result -> result.historyItem }
                        // 7
                        .let { searchResults.postValue(it) }
                }
            }
        }
    }

}

private fun sanitizeSearchQuery(query: String): String {
    var cleanQuery = query.trim()
    return cleanQuery
}

private fun calculateScore(matchInfo: ByteArray): Double {
    val info = matchInfo.toIntArray()

    val numPhrases = info[0]
    val numColumns = info[1]

    var score = 0.0
    for (phrase in 0 until numPhrases) {
        val offset = 2 + phrase * numColumns * 3
        for (column in 0 until numColumns) {
            val numHitsInRow = info[offset + 3 * column]
            val numHitsInAllRows = info[offset + 3 * column + 1]
            if (numHitsInAllRows > 0) {
                if (column == 0) {
                    // title hits should be worth double
                    score += 2 * ( numHitsInRow.toDouble() / numHitsInAllRows.toDouble())
                }
                else {
                    score += numHitsInRow.toDouble() / numHitsInAllRows.toDouble()
                }
            }
        }
    }

    return score
}

fun ByteArray.toIntArray(skipSize: Int = 4): IntArray {
    val cleanedArr = IntArray(this.size / skipSize)
    for ((pointer, i) in (this.indices step skipSize).withIndex()) {
        cleanedArr[pointer] = this[i].toInt()
    }

    return cleanedArr
}

class HistoryViewModelFactory(private val repository: MyRepository) : ViewModelProvider.Factory {
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(HistoryViewModel::class.java)) {
            @Suppress("UNCHECKED_CAST")
            return HistoryViewModel(repository) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }
}