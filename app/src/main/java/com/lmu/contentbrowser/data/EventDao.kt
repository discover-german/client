package com.lmu.contentbrowser.data

import androidx.room.*
import androidx.sqlite.db.SimpleSQLiteQuery
import kotlinx.coroutines.flow.Flow

@Dao
interface EventDao {
    //@Query("SELECT EV.* FROM events EV RIGHT JOIN events_meta EM1 ON EM1.event_id = EV.id RIGHT JOIN events_meta EM2 ON EM2.meta_key = CONCAT( 'repeat_interval_', EM1.id ) WHERE EM1.meta_key = 'repeat_start' AND ( ( CASE ( :myTime - EM1.meta_value ) WHEN 0 THEN 1 ELSE ( :myTime - EM1.meta_value ) END ) / EM2.meta_value ) = 1 LIMIT 0 , 30")
    //fun getEventsForTime(myTime: String): Flow<List<Event>>

    @RawQuery(observedEntities = arrayOf(Event::class))
    fun getEventsForQuery(query: SimpleSQLiteQuery): Flow<List<Event>>

    @RawQuery(observedEntities = arrayOf(Event::class))
    fun getEventsForQueryOnce(query: SimpleSQLiteQuery): List<Event>

    @Query("SELECT * FROM events")
    fun getAllEvents(): Flow<List<Event>>

    @Query("SELECT * FROM events")
    fun getAllEventsOnce(): List<Event>

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    suspend fun insert(event: Event): Long

    @Query("SELECT strftime('%s', 'now')")
    suspend fun currentTime(): Long

    @Query("DELETE FROM events WHERE id = :id")
    suspend fun deleteForId(id: Long)

    @Query("DELETE FROM events")
    suspend fun deleteAll()
}