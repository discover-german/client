package com.lmu.contentbrowser.receivers

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.util.Log
import androidx.work.*
import com.awesome.germanhelper.NotificationHelper
import com.lmu.contentbrowser.Constants
import com.lmu.contentbrowser.R
import com.lmu.contentbrowser.workers.NotificationWorker
import java.util.concurrent.TimeUnit


/**
 * Receives the signal from the AlarmManager to run the notification service.
 */
class NotificationBroadcastReceiver: BroadcastReceiver() {
    override fun onReceive(context: Context?, intent: Intent?) {
        if (context != null) {
            initializePassword(context)
            if (!running) {
                notificationHelper.setupNotificationReceiver(context)
                running = true
            }
            else {
                notificationHelper.checkForNotifications(context)
            }
        }
    }

    private fun initializePassword(context: Context) {
        if (Constants.participantId == "") {
            val sharedPref =
                context.getSharedPreferences(
                    context.resources.getString(R.string.participant_id_key), Context.MODE_PRIVATE)
            if (sharedPref.contains(context.resources.getString(R.string.participant_id_key))) {
                Constants.participantId =
                    sharedPref.getString(context.resources.getString(R.string.participant_id_key), "") ?: ""
                Log.i("participant_id", "Successfully retrieved participant id")
            } else {
                Log.i(
                    "participant_id",
                    "No participant id stored. Open app to initialize."
                )
            }
        }
    }

    companion object {
        val notificationHelper = NotificationHelper()
        var running = false
    }
}