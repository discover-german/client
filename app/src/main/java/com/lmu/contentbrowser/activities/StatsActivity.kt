package com.lmu.contentbrowser.activities

import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.LinearLayoutCompat
import androidx.cardview.widget.CardView
import com.google.gson.Gson
import com.lmu.contentbrowser.R
import com.lmu.contentbrowser.data.Statistics
import okhttp3.OkHttpClient
import okhttp3.Request
import okhttp3.Response
import java.io.IOException
import okhttp3.HttpUrl
import okhttp3.HttpUrl.Companion.toHttpUrlOrNull
import androidx.core.app.NavUtils

import android.view.MenuItem
import android.widget.ImageView
import android.widget.ProgressBar
import androidx.activity.viewModels
import com.lmu.contentbrowser.Constants
import com.lmu.contentbrowser.ContentBrowserApplication
import com.lmu.contentbrowser.data.LogEvent
import com.lmu.contentbrowser.data.LogEventType
import com.lmu.contentbrowser.viewmodels.LogEventViewModel
import com.lmu.contentbrowser.viewmodels.LogEventViewModelFactory


class StatsActivity: AppCompatActivity() {
    val statsUrl = Constants.SERVER_URL + "/statistics"
    val client = OkHttpClient()
    val gson = Gson()
    var itemOid = ""

    val logEventViewModel: LogEventViewModel by viewModels {
        LogEventViewModelFactory((application as ContentBrowserApplication).repository)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_stats)

        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setTitle(R.string.statsActivityBar)

        val b = intent.extras
        val oid = b?.getString("oid")
        val contentType = b?.getString("contentType")
        val triggerTitle = b?.getString("triggerTitle")
        val episodeIndex = b?.getString("episodeIndex")

        if (oid!=null) {
           itemOid = oid
        }

        val loadingCircle = findViewById<ProgressBar>(R.id.loadingCircle)
        loadingCircle.visibility = View.VISIBLE

        Thread{
            try {
                val urlBuilder: HttpUrl.Builder =
                    statsUrl.toHttpUrlOrNull()!!
                        .newBuilder()
                urlBuilder.addQueryParameter("oid", oid)
                urlBuilder.addQueryParameter("contentType", contentType)
                urlBuilder.addQueryParameter("episodeIndex", episodeIndex)
                val request: Request = Request.Builder().url(urlBuilder.build())
                    .header("participantId", Constants.participantId).build()
                val response: Response = client.newCall(request).execute()
                if (response.code == 200 && response.body != null) {
                    val statistics = gson.fromJson(response.body?.string(), Statistics::class.java)
                    response.body!!.close()
                    runOnUiThread {
                        val statsTitleView = findViewById<TextView>(R.id.statsTitle)
                        statsTitleView.text = statistics.title
                        val statsKeywordsView = findViewById<TextView>(R.id.keywords)
                        statsKeywordsView.text = statistics.keywords.joinToString(", ")
                        val readabilityIndexView = findViewById<TextView>(R.id.readabilityIndex)
                        readabilityIndexView.text = "%.0f".format(statistics.readabilityIndex)
                        val difficultyView = findViewById<TextView>(R.id.difficulty)
                        difficultyView.text = statistics.difficulty
                        val triggerTitleView = findViewById<TextView>(R.id.triggerTitle)
                        triggerTitleView.text = triggerTitle
                        val relatedItemsView = findViewById<LinearLayoutCompat>(R.id.relatedItems)

                        for (element in statistics.similarItems) {
                            val item = layoutInflater.inflate(R.layout.layout_content_item,
                                null)
                            val title = item.findViewById<TextView>(R.id.entryTitle)
                            val description = item.findViewById<TextView>(R.id.entryDescription)
                            val cntType = item.findViewById<TextView>(R.id.contentType)
                            val statsButton = item.findViewById<CardView>(R.id.statsButton)
                            val imageCard = item.findViewById<CardView>(R.id.entryThumbnail)

                            title.text = element.item.title
                            description.text = element.item.body.take(300).replace("<[^>]*>".toRegex(), "")
                            cntType.text = "Similarity score: %.1f%%".format(element.score*100)
                            statsButton.visibility = View.INVISIBLE
                            imageCard.visibility = View.GONE
                            relatedItemsView.addView(item)
                        }
                        loadingCircle.visibility = View.GONE
                    }
                }
            } catch (exception: IOException) {
                Log.e("API", "Could not fetch recommendations: ${exception}")
            }
        }.start()
    }

    override fun onResume() {
        super.onResume()
        val logEventType = LogEventType.STATS_START
        val currentTime = System.currentTimeMillis()
        logEventViewModel.insertEvent(
            LogEvent(0, logEventType, currentTime, itemOid,
                false)
        )
    }

    override fun onPause() {
        super.onPause()
        val logEventType = LogEventType.STATS_END
        val currentTime = System.currentTimeMillis()
        logEventViewModel.insertEvent(
            LogEvent(0, logEventType, currentTime, itemOid,
                false)
        )
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            android.R.id.home -> {
                NavUtils.navigateUpFromSameTask(this)
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }
}