package com.lmu.contentbrowser.data

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import kotlinx.coroutines.flow.Flow

@Dao
interface LogEventDao {
    @Query("SELECT * FROM logs")
    fun getAllEvents(): Flow<List<LogEvent>>

    @Query("SELECT * FROM logs WHERE synced = :synced")
    fun getSyncedEvents(synced: Boolean): Flow<List<LogEvent>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(event: LogEvent): Long

    @Query("DELETE FROM logs WHERE id = :id")
    suspend fun deleteForId(id: Long)
}