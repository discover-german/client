package com.lmu.contentbrowser.activities

import android.os.Bundle
import android.view.View
import android.widget.ListView
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import com.lmu.contentbrowser.ContentBrowserApplication
import com.lmu.contentbrowser.R
import com.lmu.contentbrowser.adapters.EventsAdapter
import com.lmu.contentbrowser.dialogs.CreateNotificationDialog
import com.lmu.contentbrowser.viewmodels.EventViewModel
import com.lmu.contentbrowser.viewmodels.EventViewModelFactory
import com.lmu.contentbrowser.viewmodels.LogEventViewModel
import com.lmu.contentbrowser.viewmodels.LogEventViewModelFactory


class NotificationManagerActivity: AppCompatActivity() {
    val eventViewModel: EventViewModel by viewModels {
        EventViewModelFactory((application as ContentBrowserApplication).repository)
    }
    val logEventViewModel: LogEventViewModel by viewModels {
        LogEventViewModelFactory((application as ContentBrowserApplication).repository)
    }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_notification_manager)

        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.title = "Manage Notifications"

        eventViewModel.allEvents.observe(this) {
            Thread {
                val entries = arrayListOf<String>()
                for (event in it) {
                    val id = event.id
                    val eventType = event.type
                    val time = eventViewModel.getTimeForEvent(event.id)
                    val interval = eventViewModel.getIntervalForEvent(event.id)
                    entries.add("${id}|${eventType}|${time}|${interval}")
                }
                runOnUiThread {
                    val listView = findViewById<ListView>(R.id.listview)
                    val adapter = EventsAdapter(this, entries.toTypedArray())
                    listView.adapter = adapter
                }
            }.start()
        }

        val fab: View = findViewById(R.id.fab)
        fab.setOnClickListener { view ->
            CreateNotificationDialog(eventViewModel, logEventViewModel).show(supportFragmentManager,
                "Create a new notification")
        }
    }
}