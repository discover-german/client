package com.lmu.contentbrowser.database

import androidx.annotation.WorkerThread
import androidx.lifecycle.asLiveData
import androidx.sqlite.db.SimpleSQLiteQuery
import com.lmu.contentbrowser.data.*
import kotlinx.coroutines.flow.Flow
import java.time.LocalDateTime
import java.time.OffsetDateTime
import java.time.format.DateTimeFormatter

/**
 * Manages the Room database
 */
class MyRepository(private val whitelistItemDao: WhitelistItemDao,
                   private val phraseDao: PhraseDao,
                   private val favoriteDao: FavoriteDao,
                   private val historyItemDao: HistoryItemDao,
                   private val eventDao: EventDao,
                   private val eventMetaDao: EventMetaDao,
                   private val contentPreferenceDao: ContentPreferenceDao,
                   private val logEventDao: LogEventDao) {

    val allPhrases: Flow<List<Phrase>> = phraseDao.getAllItems()
    val allFavorites: Flow<List<Favorite>> = favoriteDao.getAllItems()
    val allHistoryItems: Flow<List<HistoryItem>> = historyItemDao.getAllItems()
    val allEvents: Flow<List<Event>> = eventDao.getAllEvents()
    val allEventMetas: Flow<List<EventMeta>> = eventMetaDao.getAllEventMetas()
    val allLogEvents: Flow<List<LogEvent>> = logEventDao.getAllEvents()

    @Suppress("RedundantSuspendModifier")
    @WorkerThread
    suspend fun insertWhitelistItem(WhitelistItem: WhitelistItem) {
        val whitelistItemId = whitelistItemDao.insert(WhitelistItem)
    }

    @Suppress("RedundantSuspendModifier")
    @WorkerThread
    suspend fun deleteWhitelistItem(source: Source) {
        return whitelistItemDao.deleteItem(source)
    }

    fun getWhitelistItemAsync(source: Source): WhitelistItem? {
        return whitelistItemDao.getItemBySourceAsync(source)
    }

    fun getAllWhitelistItemsAsync(): List<WhitelistItem> {
        return whitelistItemDao.getAllItemsAsync()
    }

    @Suppress("RedundantSuspendModifier")
    @WorkerThread
    suspend fun insertPhrase(phrase: Phrase): Long {
        return phraseDao.insert(phrase)
    }

    @Suppress("RedundantSuspendModifier")
    @WorkerThread
    suspend fun deletePhrase(id: Long) {
        phraseDao.deleteItem(id)
    }

    @Suppress("RedundantSuspendModifier")
    @WorkerThread
    suspend fun getRandomPhrase(): Phrase? {
        return phraseDao.getRandomItem()
    }

    @Suppress("RedundantSuspendModifier")
    @WorkerThread
    suspend fun insertFavorite(favorite: Favorite): Long {
        return favoriteDao.insert(favorite)
    }

    @Suppress("RedundantSuspendModifier")
    @WorkerThread
    suspend fun deleteFavorite(id: Long) {
        favoriteDao.deleteItem(id)
    }

    @Suppress("RedundantSuspendModifier")
    @WorkerThread
    suspend fun updateFavoriteStatus(isFavorite: Boolean, oid: String, contentType: String) {
        favoriteDao.updateIsFavoriteByOidAndContentType(isFavorite, oid, contentType)
    }

    @Suppress("RedundantSuspendModifier")
    @WorkerThread
    fun getFavoriteItemByOidAndContentType(oid: String, contentType: String): Favorite? {
        return favoriteDao.getFavoriteByOidAndContentType(oid, contentType)
    }

    @Suppress("RedundantSuspendModifier")
    @WorkerThread
    fun getFavoritesCount(): Int {
        return favoriteDao.getCount()
    }

            @Suppress("RedundantSuspendModifier")
    @WorkerThread
    suspend fun insertHistoryItem(historyItem: HistoryItem): Long {
        return historyItemDao.insert(historyItem)
    }

    @Suppress("RedundantSuspendModifier")
    @WorkerThread
    suspend fun deleteHistoryItem(id: Long) {
        val historyItem = historyItemDao.getItemById(id)
        historyItemDao.deleteItem(id)
        val favoriteItem =
            favoriteDao.getFavoriteByOidAndContentType(historyItem.oid, historyItem.contentType)
        if (favoriteItem != null) {
            favoriteDao.deleteItem(favoriteItem.id)
        }
    }

    @Suppress("RedundantSuspendModifier")
    @WorkerThread
    fun getHistoryItemById(id: Long): HistoryItem {
        return historyItemDao.getItemById(id)
    }

    @Suppress("RedundantSuspendModifier")
    @WorkerThread
    suspend fun searchInHistory(query: String): List<HistoryItem> {
        return historyItemDao.search(query)
    }

    @Suppress("RedundantSuspendModifier")
    @WorkerThread
    suspend fun searchWithMatchInfo(query: String): List<HistoryItemWithMatchInfo> {
        return historyItemDao.searchWithMatchInfo(query)
    }

    @Suppress("RedundantSuspendModifier")
    @WorkerThread
    suspend fun insertEvent(event: Event, eventMetas: List<Map<String, EventMeta>>) {
        // always tie event.id to meta object
        val eventId = eventDao.insert(event)
        var startMetaId: Long
        for (pair in eventMetas) {
            val startMeta = pair["start"]
            val intervalMeta = pair["interval"]
            if (startMeta != null && intervalMeta != null) {
                startMeta.event_id = eventId.toInt()
                // safe start meta id
                startMetaId = eventMetaDao.insert(startMeta)
                intervalMeta.event_id = eventId.toInt()
                // tie it to interval meta object
                intervalMeta.meta_key = "repeat_interval_$startMetaId"
                eventMetaDao.insert(intervalMeta)
            }
        }
    }

    @Suppress("RedundantSuspendModifier")
    @WorkerThread
    fun getUpcomingEvents(): List<Event> {
        val currentTime = (System.currentTimeMillis() / 1000L)
        val currentLocalDateTime = LocalDateTime.ofEpochSecond(currentTime, 0, OffsetDateTime.now().getOffset())
        val tomorrowDate =  "${currentLocalDateTime.year}-" +
                "${"%02d".format(currentLocalDateTime.monthValue)}-" +
                "${"%02d".format(currentLocalDateTime.dayOfMonth+1)} "+
                "00:00"
        val formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm")
        val endOfDayTime = LocalDateTime.parse("$tomorrowDate", formatter).toEpochSecond(
            OffsetDateTime.now().getOffset()) - 1
        val upcomingEventsQuery = SimpleSQLiteQuery(
            "SELECT EV.* " +
                    "FROM events EV " +
                    "LEFT JOIN events_meta EM1 ON EM1.event_id = EV.id " +
                    "LEFT JOIN events_meta EM2 ON EM2.meta_key = 'repeat_interval_' || EM1.id " +
                    "WHERE EM1.meta_key = 'repeat_start' AND " +
                    // checks if the event is in the future and if the starting time is earlier
                    // than the end of the current day
                    "( ( EM1.meta_value - $currentTime > 0 AND " +
                    " $endOfDayTime - EM1.meta_value > 0 ) OR " +
                    // The following expression might be unnecessarily complicated.
                    // It checks if the time to the next repetition of the event is smaller
                    // than the remaining time of day
                    "( EM2.meta_value - ( ( $currentTime - EM1.meta_value ) % EM2.meta_value ) < ($endOfDayTime - $currentTime) ) ) " +
                    "LIMIT 0, 30;")
        val upcomingEvents: List<Event> = eventDao.getEventsForQueryOnce(upcomingEventsQuery)
        return upcomingEvents
    }

    @Suppress("RedundantSuspendModifier")
    @WorkerThread
    fun getCurrentEvents(timeWindow: Long): List<Event> {
        val currentTime = (System.currentTimeMillis() / 1000L)
        val currentEventsQuery = SimpleSQLiteQuery(
            "SELECT EV.* " +
                    "FROM events EV " +
                    "LEFT JOIN events_meta EM1 ON EM1.event_id = EV.id " +
                    "LEFT JOIN events_meta EM2 ON EM2.meta_key = 'repeat_interval_' || EM1.id " +
                    "WHERE EM1.meta_key = 'repeat_start' AND " +
                    "CASE " +
                    // the first 'when' handles non repeating events. it checks if there where any
                    // events since the last check, that need to be pushed.
                    "WHEN (ABS( $currentTime - EM1.meta_value ) < $timeWindow) AND " +
                    "( $currentTime - EM1.meta_value) > 0 THEN 1 " +
                    // the 'else' handles repeating events. the behaviour is similar to
                    // the non repeating events.
                    "ELSE ( ( ( $currentTime - EM1.meta_value ) % EM2.meta_value ) < $timeWindow ) " +
                    "AND ($currentTime - EM1.meta_value > 0) " +
                    "END = 1 " +
                    "LIMIT 0, 30;")
        val currentEvents: List<Event> = eventDao.getEventsForQueryOnce(currentEventsQuery)
        return currentEvents
    }

    @Suppress("RedundantSuspendModifier")
    @WorkerThread
    fun getEventsForDate(epochDay: Long): List<Event> {
        val startOfDayTime = epochDay * 86400L + OffsetDateTime.now().getOffset().totalSeconds
        val endOfDayTime = startOfDayTime + 86400L
        val dateEventsQuery = SimpleSQLiteQuery(
            "SELECT EV.* " +
                    "FROM events EV " +
                    "LEFT JOIN events_meta EM1 ON EM1.event_id = EV.id " +
                    "LEFT JOIN events_meta EM2 ON EM2.meta_key = 'repeat_interval_' || EM1.id " +
                    "WHERE EM1.meta_key = 'repeat_start' AND " +
                    // checks if the event is supposed to happen today
                    "( ( EM1.meta_value - $startOfDayTime > 0 AND " +
                    " $endOfDayTime- EM1.meta_value > 0 ) OR " +
                    // The following expression checks if the event is supposed to repeat today
                    "( EM2.meta_value - ( ( $endOfDayTime - EM1.meta_value ) % EM2.meta_value ) < (86400) ) ) " +
                    "LIMIT 0, 30;")
        val eventsForDay: List<Event> = eventDao.getEventsForQueryOnce(dateEventsQuery)
        return eventsForDay
    }

    @Suppress("RedundantSuspendModifier")
    @WorkerThread
    fun getEventMetasForID(id: Long): List<EventMeta> {
        val metaQuery = SimpleSQLiteQuery("SELECT EV.* FROM events_meta EV WHERE EV.event_id = $id")
        val eventMetas: List<EventMeta> = eventMetaDao.getEventMetasForQueryOnce(metaQuery)
        return eventMetas
    }

    @Suppress("RedundantSuspendModifier")
    @WorkerThread
    suspend fun currentTime() {
        val currentTime = eventDao.currentTime()
    }

    @Suppress("RedundantSuspendModifier")
    @WorkerThread
    suspend fun deleteAllEventsAndMetas() {
        eventDao.deleteAll()
        eventMetaDao.deleteAll()
    }

    @Suppress("RedundantSuspendModifier")
    @WorkerThread
    suspend fun deleteEventAndMetas(id: Long) {
        eventDao.deleteForId(id)
        eventMetaDao.deleteForEventId(id)
    }

    @Suppress("RedundantSuspendModifier")
    @WorkerThread
    fun insertContentPreference(contentPreference: ContentPreference): Long {
        return contentPreferenceDao.insert(contentPreference)
    }

    @Suppress("RedundantSuspendModifier")
    @WorkerThread
    fun getContentPreferenceByKeyAsync(key: String): ContentPreference? {
        return contentPreferenceDao.getItemByKeyAsync(key)
    }

    @Suppress("RedundantSuspendModifier")
    @WorkerThread
    fun getSyncedLogEvents(synced: Boolean): Flow<List<LogEvent>> {
        return logEventDao.getSyncedEvents(synced)
    }

    @Suppress("RedundantSuspendModifier")
    @WorkerThread
    suspend fun insertLogEvent(logEvent: LogEvent): Long {
        return logEventDao.insert(logEvent)
    }

    @Suppress("RedundantSuspendModifier")
    @WorkerThread
    suspend fun deleteLogEvent(id: Long) {
        return logEventDao.deleteForId(id)
    }
}