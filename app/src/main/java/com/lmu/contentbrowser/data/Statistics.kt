package com.lmu.contentbrowser.data

data class Statistics(
    val similarItems: List<similarItem>,
    val title: String,
    val keywords: List<String>,
    val readabilityIndex: Float,
    val difficulty: String
)
