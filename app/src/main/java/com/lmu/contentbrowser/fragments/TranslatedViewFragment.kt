package com.lmu.contentbrowser.fragments

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.fragment.app.ListFragment
import com.google.android.gms.tasks.SuccessContinuation
import com.google.android.gms.tasks.Task
import com.google.android.gms.tasks.Tasks
import com.google.gson.Gson
import com.google.mlkit.nl.translate.TranslateLanguage
import com.google.mlkit.nl.translate.Translation
import com.google.mlkit.nl.translate.TranslatorOptions
import com.lmu.contentbrowser.Constants
import com.lmu.contentbrowser.R
import com.lmu.contentbrowser.activities.ViewerActivity
import com.lmu.contentbrowser.adapters.BackToBackAdapter
import com.lmu.contentbrowser.data.Book
import com.lmu.contentbrowser.helpers.GzipHelper
import okhttp3.HttpUrl
import okhttp3.HttpUrl.Companion.toHttpUrlOrNull
import okhttp3.OkHttpClient
import okhttp3.Request
import okhttp3.Response
import java.util.concurrent.TimeUnit


class TranslatedViewFragment: ListFragment() {
    private var bookPages: ArrayList<String>? = null
    private var bookPageIndex = 0
    private val getItemUrl = Constants.SERVER_URL+"/item"
    private val client = OkHttpClient.Builder().connectTimeout(Constants.CONNECT_TIMEOUT,
        TimeUnit.SECONDS).build()
    private val gson = Gson()
    private val gzipHelper = GzipHelper()
    val options = TranslatorOptions.Builder()
        .setSourceLanguage(TranslateLanguage.GERMAN)
        .setTargetLanguage(TranslateLanguage.ENGLISH)
        .build()
    val englishGermanTranslator = Translation.getClient(options)

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_translatedview, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val b = requireActivity().intent.extras
        val item = b?.getString("item")
        var body = ""
        var contentType = ""
        var oid = ""
        if (item!=null && item.split("|").size >= 6) {
            contentType = item.split("|")[0]
            body = item.split("|")[2]
            oid = item.split("|")[3]
        }
        else if((requireActivity() as ViewerActivity).itemContentType != "" &&
            (requireActivity() as ViewerActivity).itemDescription != "" &&
            (requireActivity() as ViewerActivity).itemOid != "") {
            contentType = (requireActivity() as ViewerActivity).itemContentType
            body = (requireActivity() as ViewerActivity).itemDescription
            oid = (requireActivity() as ViewerActivity).itemOid
        }
        else {
            return
        }
        val loadingCircle = view.findViewById<ProgressBar>(R.id.loadingCircle)
        loadingCircle.visibility = View.VISIBLE
        englishGermanTranslator.downloadModelIfNeeded().addOnSuccessListener {
            if (contentType == "book") {
                bookPages = arrayListOf(body)
                Thread{
                    populatePages(oid)
                }.start()
                if (requireActivity() is ViewerActivity) {
                    displayPage(bookPages!![0])
                }
            }
            else {
                if (requireActivity() is ViewerActivity) {
                    displayPage(body)
                }
            }
        }
    }

    private fun displayPage(page: String) {
        val loadingCircle = requireView().findViewById<ProgressBar>(R.id.loadingCircle)
        if (bookPages!= null && view != null && this.isAdded) {
            val topContainer = requireView().findViewById<FrameLayout>(R.id.topNavBar)
            topContainer.removeAllViews()
            val topNavBar = prepareNavigationBar(bookPageIndex, topContainer)
            topContainer.addView(topNavBar)
        }
        Thread {
            val body = page.replace("#[#]+".toRegex(), "")
            val originalParagraphs = body.split("\n")
            Log.i("translation", "characters: ${body.length}")
            val tasks: MutableList<Task<String>> = ArrayList()
            for (paragraph in body.split("\n")) {
                tasks.add(englishGermanTranslator.translate(paragraph))
            }
            val resultTask: Task<String> = Tasks.whenAll(tasks).onSuccessTask(
                SuccessContinuation<Void, String> {
                    val results: MutableList<String> = ArrayList()
                    for (task: Task<String> in tasks) {
                        results.add(task.result)
                    }
                    Tasks.forResult<String>(java.lang.String.join("\n", results))
                }).addOnSuccessListener {
                    if (it!=null) {
                        val translatedParagraphs = it.split("\n")
                        if (originalParagraphs.size == translatedParagraphs.size) {
                            val paragraphs = arrayListOf<String>()
                            for ((original, translated) in originalParagraphs zip translatedParagraphs) {
                                paragraphs.add("$original|$translated")
                            }
                            if (activity != null) {
                                requireActivity().runOnUiThread {
                                    if (this.isAdded) {
                                        val backToBackAdapter = BackToBackAdapter(
                                            requireActivity(),
                                            paragraphs.toTypedArray()
                                        )
                                        listView.adapter = backToBackAdapter
                                        val googleTranslateBar = layoutInflater.inflate(R.layout.layout_google_translate_bar, listView, false)
                                        listView.addFooterView(googleTranslateBar)
                                    }
                                }
                            }
                        }
                    }
                loadingCircle.visibility = View.GONE
            }
        }.start()
    }

    private fun populatePages(oid: String) {
        try {
            val urlBuilder: HttpUrl.Builder =
                getItemUrl.toHttpUrlOrNull()!!
                    .newBuilder()
            urlBuilder.addQueryParameter("oid", oid)
            urlBuilder.addQueryParameter("contentType", "book")
            val request: Request = Request.Builder().url(urlBuilder.build())
                .header("participantId", Constants.participantId).build()
            val response: Response = client.newCall(request).execute()
            if (response.code == 200 && response.body != null) {
                val responseBody = response.body!!.bytes()
                val responseDecompressed = gzipHelper.unzip(responseBody)
                val book = gson.fromJson(responseDecompressed, Book::class.java)
                for (page in book.text.drop(1)) {
                    bookPages?.add(page)
                }
            }
        }
        catch (e: Exception) {
            Log.e("API", "could not fetch books: $e")
            if (activity!=null) {
                requireActivity().runOnUiThread {
                    Toast.makeText(
                        requireContext(),
                        "Could not fetch book pages",
                        Toast.LENGTH_SHORT
                    ).show()
                }
            }
        }
    }

    private fun prepareNavigationBar(index: Int,
                                     textContainer: ViewGroup
    ): ConstraintLayout {
        val navigationBar: ConstraintLayout = layoutInflater.inflate(
            R.layout.layout_nav_bar_book,
            textContainer, false
        ) as ConstraintLayout
        val navBack: ImageView = navigationBar.findViewById(R.id.navBack)
        val navBackText: TextView = navigationBar.findViewById(R.id.navBackText)
        val navForward: ImageView = navigationBar.findViewById(R.id.navForward)
        val navForwardText: TextView = navigationBar.findViewById(R.id.navForwardText)
        val pageIndexDescription: TextView = navigationBar.findViewById(R.id.pageIndexDescription)
        if (index == 0) {
            navBack.visibility = View.INVISIBLE
            navBackText.visibility = View.INVISIBLE
            navForward.setOnClickListener {
                bookPageIndex += 1
                if (bookPages!=null) {
                    displayPage(bookPages!![bookPageIndex])
                }
            }
        }
        else if(index == bookPages!!.size - 1) {
            navForward.visibility = View.INVISIBLE
            navForwardText.visibility = View.INVISIBLE
            navBack.setOnClickListener {
                bookPageIndex -= 1
                if (bookPages!=null) {
                    displayPage(bookPages!![bookPageIndex])
                }
            }
        }
        else {
            navForward.setOnClickListener {
                bookPageIndex += 1
                if (bookPages!=null) {
                    displayPage(bookPages!![bookPageIndex])
                }
            }
            navBack.setOnClickListener {
                bookPageIndex -= 1
                if (bookPages!=null) {
                    displayPage(bookPages!![bookPageIndex])
                }
            }
        }
        pageIndexDescription.text = "Page Number: ${bookPageIndex+1}"
        return navigationBar
    }
}