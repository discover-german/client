package com.lmu.contentbrowser.activities

import android.os.Bundle
import android.widget.ListView
import androidx.appcompat.app.AppCompatActivity
import com.lmu.contentbrowser.R
import com.lmu.contentbrowser.adapters.PublisherAdapter

class PublisherActivity: AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_publisher)

        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        val listview = findViewById<ListView>(R.id.listview)

        val b = intent.extras
        val url = b?.getString("url")
        val author = b?.getString("author")
        if (url != null && author != null) {
            val entry = "${publisherNames[author]}|${url}|${publisherEmails[author]}|" +
                    "${publisherPhones[author]}"
            val adapter = PublisherAdapter(this, arrayOf(entry))
            listview.adapter = adapter
        }
        else {
            val entries = arrayListOf<String>()
            for (key in publisherNames.keys) {
                val entry = "${publisherNames[key]}|${publisherUrls[key]}|${publisherEmails[key]}|" +
                        "${publisherPhones[key]}"
                entries.add(entry)
            }
            val adapter = PublisherAdapter(this, entries.toTypedArray())
            listview.adapter = adapter
        }
    }

    private val publisherNames = mapOf(
        "DER SPIEGEL" to "DER SPIEGEL GmbH & Co. KG",
        "FOCUS Online" to "BurdaForward GmbH",
        "Kruschel.de" to "VRM GmbH & Co. KG",
        "logo!" to "Zweites Deutsches Fernsehen",
        "nachrichtenleicht" to "Deutschlandradio",
        "P.M." to "Red Bull Media House GmbH",
        "FitForFun" to "BurdaForward GmbH",
        "GQ Magazin" to "Condé Nast Germany GmbH",
        "gofeminin" to "gofeminin.de GmbH")
    private val publisherEmails = mapOf(
        "DER SPIEGEL" to "spiegel@spiegel.de",
        "FOCUS Online" to "redaktion@focus.de",
        "Kruschel.de" to "kruschel@vrm.de",
        "logo!" to "info@zdf.de",
        "nachrichtenleicht" to "nachrichtenleicht@deutschlandfunk.de",
        "P.M." to "kontakt@servustv.com",
        "FitForFun" to "info@fitforfun.de",
        "GQ Magazin" to "mail@condenast.de",
        "gofeminin" to "community@gofeminin.de")
    private val publisherPhones = mapOf(
        "DER SPIEGEL" to "+49 40 3007-0",
        "FOCUS Online" to "089/92 50-3292",
        "Kruschel.de" to "+49 61 31/48 30",
        "logo!" to "+40 6131/70-0",
        "nachrichtenleicht" to "+49 221 345 1012",
        "P.M." to "0043 662 842244 28120",
        "FitForFun" to "040/4131 – 0",
        "GQ Magazin" to "089 38104-0",
        "gofeminin" to "+49 (0) 221 / 28325-0")
    private val publisherUrls = mapOf(
        "DER SPIEGEL" to "https://www.spiegel.de/",
        "FOCUS Online" to "https://www.focus.de/",
        "Kruschel.de" to "https://kruschel-kinder.de/",
        "logo!" to "https://www.zdf.de/kinder/logo",
        "nachrichtenleicht" to "https://www.nachrichtenleicht.de/",
        "P.M." to "https://www.redbullmediahouse.com/de",
        "FitForFun" to "https://www.burda-forward.de/",
        "GQ Magazin" to "www.condenast.de",
        "gofeminin" to "https://www.gofeminin.de/"
    )
}