package com.lmu.contentbrowser.adapters

import android.app.Activity
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.TextView
import androidx.cardview.widget.CardView
import com.lmu.contentbrowser.ContentBrowserApplication
import com.lmu.contentbrowser.R
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class PhrasesAdapter (private val context: Activity, private val entries: Array<String>):
    ArrayAdapter<String>(context, R.layout.layout_content_item, entries) {

    override fun getView(position: Int, view: View?, parent: ViewGroup): View {
        val inflater = context.layoutInflater
        val rowView = inflater.inflate(R.layout.layout_phrase_row, parent, false)

        val entry = entries[position]

        if (entry.split("|").size == 6) {
            val id = entry.split("|")[0].toLong()
            val phrase = entry.split("|")[1]
            val translation = entry.split("|")[2]
            val syllables = entry.split("|")[3]
            val wordClass = entry.split("|")[4]
            val examples = entry.split("|")[5]

            val phraseView = rowView.findViewById<TextView>(R.id.phrase)
            val translationView = rowView.findViewById<TextView>(R.id.translation)
            val syllablesView = rowView.findViewById<TextView>(R.id.syllables)
            val wordClassView = rowView.findViewById<TextView>(R.id.wordClass)
            val examplesView = rowView.findViewById<TextView>(R.id.examples)
            val deleteButton = rowView.findViewById<CardView>(R.id.deleteButton)

            val syllablesHeaderView = rowView.findViewById<TextView>(R.id.syllablesHeader)
            val wordClassHeaderView = rowView.findViewById<TextView>(R.id.wordClassHeader)
            val examplesHeader = rowView.findViewById<TextView>(R.id.examplesHeader)

            phraseView.text = phrase
            translationView.text = translation
            if (syllables != "null") {
                syllablesView.text = syllables
            }
            else {
                syllablesView.visibility = View.GONE
                syllablesHeaderView.visibility = View.GONE
            }
            if (wordClass != "null") {
                wordClassView.text = wordClass
            }
            else {
                wordClassView.visibility = View.GONE
                wordClassHeaderView.visibility = View.GONE
            }
            if (examples != "null") {
                examplesView.text = examples.split(";").joinToString (separator = "\n\n")
            }
            else {
                examplesView.visibility = View.GONE
                examplesHeader.visibility = View.GONE
            }
            deleteButton.setOnClickListener {
                CoroutineScope(Dispatchers.IO).launch {
                    (context.application as ContentBrowserApplication).repository.deletePhrase(id)
                }
            }
        }

        return rowView
    }
}