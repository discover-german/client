package com.lmu.contentbrowser.fragments

import android.content.Context
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.google.gson.Gson
import com.lmu.contentbrowser.R
import com.lmu.contentbrowser.adapters.ContentAdapter
import android.content.Intent
import android.widget.AbsListView
import android.widget.ListView
import androidx.activity.viewModels
import androidx.core.view.isVisible
import androidx.fragment.app.ListFragment
import androidx.fragment.app.viewModels
import androidx.room.DeleteColumn
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.lmu.contentbrowser.Constants
import com.lmu.contentbrowser.Constants.Companion.CONNECT_TIMEOUT
import com.lmu.contentbrowser.ContentBrowserApplication
import com.lmu.contentbrowser.activities.ViewerActivity
import com.lmu.contentbrowser.activities.PodcastActivity
import com.lmu.contentbrowser.data.*
import com.lmu.contentbrowser.helpers.GzipHelper
import com.lmu.contentbrowser.viewmodels.*
import okhttp3.*
import okhttp3.HttpUrl.Companion.toHttpUrl
import java.util.concurrent.TimeUnit

class HomeFragment: ListFragment() {
    private val recommendationsUrl = Constants.SERVER_URL+"/recommendations"
    private val client: OkHttpClient =
        OkHttpClient.Builder().connectTimeout(CONNECT_TIMEOUT, TimeUnit.SECONDS).build()
    private val gson: Gson = Gson()
    private val gzipHelper = GzipHelper()

    private lateinit var entries: ArrayList<String>
    private lateinit var urls: ArrayList<String>
    private lateinit var contentTypes: ArrayList<String>

    val contentPreferenceModel: ContentPreferenceViewModel by viewModels {
        ContentPreferenceViewModelFactory((requireActivity().application
                as ContentBrowserApplication).repository)
    }

    val whitelistModel: WhitelistViewModel by viewModels {
        WhitelistViewModelFactory((requireActivity().application
                as ContentBrowserApplication).repository)
    }

    val logEventViewModel: LogEventViewModel by viewModels {
        LogEventViewModelFactory((requireActivity().application
                as ContentBrowserApplication).repository)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_home, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        initializeContentPreferences()
        populateHomeScreen()
        val swipeRefreshLayout = view.findViewById<SwipeRefreshLayout>(R.id.swipeRefresh)
        swipeRefreshLayout.setOnRefreshListener {
            populateHomeScreen()
            val logEventType = LogEventType.SWIPE_TO_REFRESH
            val currentTime = System.currentTimeMillis()
            logEventViewModel.insertEvent(LogEvent(0, logEventType, currentTime,
                null, false))
        }
        swipeRefreshLayout.isRefreshing = true
        listView.setOnScrollListener(HomeScreenScrollChangeListener())
    }

    fun popDataItem(position: Int) {
        if (this::entries.isInitialized && this::urls.isInitialized &&
            this::contentTypes.isInitialized) {
            entries.removeAt(position)
            urls.removeAt(position)
            contentTypes.removeAt(position)
        }
    }

    private fun populateHomeScreen() {
        Thread {
            try {
                val urlBuilder = createUrlBuilder()
                if (Constants.participantId == "") {
                    tryToGetParticipantId()
                }

                val swipeRefreshLayout = requireView()
                    .findViewById<SwipeRefreshLayout>(R.id.swipeRefresh)
                swipeRefreshLayout.isRefreshing = true

                val request: Request = Request.Builder().url(urlBuilder.build())
                    .header("participantId", Constants.participantId).build()
                val response: Response = client.newCall(request).execute()

                if (response.isSuccessful && response.body != null) {
                    val responseBody = response.body!!.bytes()
                    response.body!!.close()
                    val responseDecompressed = gzipHelper.unzip(responseBody)
                    val recommendations: Recommendations = gson.fromJson(
                        responseDecompressed,
                        Recommendations::class.java
                    )
                    val itemData = createItemDataFromRecommendations(recommendations)
                    initializeList(itemData)
                    swipeRefreshLayout.isRefreshing = false
                } else {
                    showErrorCard()
                }
            } catch (exception: Exception) {
                Log.e("API", "Could not fetch recommendations: ${exception}")
                showErrorCard()
            }
        }.start()
    }

    private fun initializeList(itemData: ContentItemData) {
        entries = itemData.entries
        urls = itemData.urls
        contentTypes = itemData.contentTypes

        val myListAdapter = ContentAdapter(requireActivity(), entries.toList())
        requireActivity().runOnUiThread {
            if (view != null) {
                listView.adapter = myListAdapter
                listView.setOnItemClickListener { parent, view, position, id ->
                    lateinit var intent: Intent
                    if (contentTypes[position] == "podcast") {
                        intent = Intent(activity, PodcastActivity::class.java)
                    } else {
                        intent = Intent(activity, ViewerActivity::class.java)
                    }
                    intent.putExtra("url", urls[position])
                    intent.putExtra("item", entries[position])
                    this.startActivity(intent)
                }
            }
        }
    }

    private fun showErrorCard() {
        val entries = arrayListOf<String>()
        entries.add("error|Could not fetch recommendations|Please swipe down vertically to refresh " +
                "the home screen. If the error persists, check your internet connection or contact " +
                "the study supervisor.|||")
        if (activity != null && view != null && this.isAdded) {
            requireActivity().runOnUiThread {
                if (this.isAdded) {
                    val myListAdapter = ContentAdapter(requireActivity(), entries.toList())
                    listView.adapter = myListAdapter
                }
            }
            val swipeRefreshLayout = requireView().findViewById<SwipeRefreshLayout>(R.id.swipeRefresh)
            swipeRefreshLayout.isRefreshing = false
        }
    }

    private fun removeSuperfluousComma(itemJson: String): String {
        val cleanedItems = arrayListOf<String>()
        val items = itemJson.split(",")
        for (item in items) {
            if (item == "") {
                continue
            }
            cleanedItems.add(item)
        }
        return cleanedItems.joinToString(",")
    }

    private fun createItemDataFromRecommendations(recommendations: Recommendations): ContentItemData {
        val entries = arrayListOf<String>()
        val urls = arrayListOf<String>()
        val contentTypes = arrayListOf<String>()
        val articles = recommendations.articles
        val books = recommendations.books
        val podcasts = recommendations.podcasts
        val articlesIterator = articles.iterator()
        val booksIterator = books.iterator()
        val podcastsIterator = podcasts.iterator()
        while (articlesIterator.hasNext() || booksIterator.hasNext() ||
            podcastsIterator.hasNext()) {

            if (articlesIterator.hasNext()) {
                val articleRecommendation = articlesIterator.next()
                val article = articleRecommendation.recommendation
                val trigger = articleRecommendation.trigger
                val triggerTitle = trigger.title.replace("|", ",")
                val articleTitle = article.title.replace("|", ",")
                val articleDescription = article.body.replace("|", ",")
                entries.add("article|${articleTitle}|${articleDescription}|" +
                        article._id.oid+"|${triggerTitle}|${article.image_link}|" +
                        "${article.author}|${article.link}")
                urls.add(article.link)
                contentTypes.add("article")
            }
            if (booksIterator.hasNext()) {
                val bookRecommendation = booksIterator.next()
                val book = bookRecommendation.recommendation
                val bookTitle = book.title.replace("|", ",")
                val trigger = bookRecommendation.trigger
                val triggerTitle = trigger.title.replace("|", ",")
                if (book.text.isNotEmpty()) {
                    val bookDescription = book.text[0]
                    entries.add("book|${bookTitle}|${bookDescription}|" +
                            book._id.oid+"|${triggerTitle}|${book.image_link}")
                    urls.add(book.link)
                    contentTypes.add("book")
                }
            }
            if (podcastsIterator.hasNext()) {
                val podcastRecommendation = podcastsIterator.next()
                val podcast = podcastRecommendation.recommendation
                val trigger = podcastRecommendation.trigger
                val episodeIndex = podcastRecommendation.episodeIndex
                val episode = podcast.entries[episodeIndex]
                var podcastTitle = episode.title
                podcastTitle = podcastTitle.replace("<[^>]*>".toRegex(), "")
                podcastTitle = podcastTitle.replace("|", "")
                var podcastDescription = episode.description
                podcastDescription = podcastDescription.replace("<[^>]*>".toRegex(), "")
                podcastDescription = podcastDescription.replace("|", "")
                entries.add("podcast|${podcastTitle}|${podcastDescription}|" +
                        podcast._id.oid+"|${trigger.title}|${episode.image_link}|$episodeIndex")
                urls.add(episode.link)
                contentTypes.add("podcast")
            }
        }
        return ContentItemData(entries, urls, contentTypes)
    }

    private fun initializeContentPreferences() {
        Thread {
            val difficulty = contentPreferenceModel.getContentPreferenceByKeyAsync("difficulty")
            if (difficulty == null) {
                contentPreferenceModel.insertContentPreference(ContentPreference("difficulty", 2))
            }
            val personalisationAmount =
                contentPreferenceModel.getContentPreferenceByKeyAsync("personalisationAmount")
            if (personalisationAmount == null) {
                contentPreferenceModel.insertContentPreference(
                    ContentPreference("personalisationAmount", 8)
                )
            }
        }.start()
    }

    private fun createUrlBuilder(): HttpUrl.Builder {
        val urlBuilder: HttpUrl.Builder = recommendationsUrl.toHttpUrl().newBuilder()
        val whitelistItems = whitelistModel.getAllWhitelistItemsAsync()
        var blacklistJson = ""
        for (item in whitelistItems) {
            if (!item.allowed) {
                blacklistJson += "\"${item.source.name}\","
            }
        }
        blacklistJson = removeSuperfluousComma(blacklistJson)
        urlBuilder.addQueryParameter("blacklist", "[$blacklistJson]")
        val personalisationAmount =
            contentPreferenceModel
                .getContentPreferenceByKeyAsync("personalisationAmount")
        if (personalisationAmount != null) {
            urlBuilder.addQueryParameter("cutoff",
                personalisationAmount.value.toString())
        }
        else {
            urlBuilder.addQueryParameter("cutoff", "8")
        }
        val difficulty =
            contentPreferenceModel.getContentPreferenceByKeyAsync("difficulty")
        if (difficulty != null) {
            urlBuilder.addQueryParameter("difficulty", difficulty.value.toString())
        }
        else {
            urlBuilder.addQueryParameter("difficulty", "2")
        }
        return urlBuilder
    }

    private fun tryToGetParticipantId() {
        val sharedPref = requireActivity().getSharedPreferences(getString(R.string.preference_file_key),
            Context.MODE_PRIVATE)
        if (sharedPref.contains(getString(R.string.participant_id_key))) {
            Log.i("participant_id", "Successfully retrieved participant id")
            Constants.participantId =
                sharedPref.getString(getString(R.string.participant_id_key), "") ?: ""
        }
    }

    inner class HomeScreenScrollChangeListener: AbsListView.OnScrollListener  {
        var lastFirstVisibleItem = 0
        var waypointReached = false
        var logEventType: LogEventType = LogEventType.NONE
        override fun onScrollStateChanged(view: AbsListView?, scrollState: Int) {

        }

        override fun onScroll(
            view: AbsListView?,
            firstVisibleItem: Int,
            visibleItemCount: Int,
            totalItemCount: Int
        ) {
            if (lastFirstVisibleItem < firstVisibleItem) {
                val scrollProgress: Float = firstVisibleItem.toFloat() / totalItemCount
                when {
                    scrollProgress >= 0.25 && logEventType == LogEventType.NONE -> {
                        logEventType = LogEventType.SCROLL_QUARTER
                        waypointReached = true
                    }
                    scrollProgress >= 0.5 && logEventType == LogEventType.SCROLL_QUARTER -> {
                        logEventType = LogEventType.SCROLL_HALFWAY
                        waypointReached = true
                    }
                    scrollProgress >= 0.75 && logEventType == LogEventType.SCROLL_HALFWAY -> {
                        logEventType = LogEventType.SCROLL_THREE_QUARTER
                        waypointReached = true
                    }
                    scrollProgress >= 0.9 && logEventType == LogEventType.SCROLL_THREE_QUARTER -> {
                        logEventType = LogEventType.SCROLL_END
                        waypointReached = true
                    }
                }
                if (waypointReached) {
                    val currentTime = System.currentTimeMillis()
                    logEventViewModel.insertEvent(LogEvent(0, logEventType, currentTime,
                        null, false))
                    waypointReached = false
                }
            }
            lastFirstVisibleItem = firstVisibleItem
        }

    }
}