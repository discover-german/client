package com.lmu.contentbrowser.data

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import kotlinx.coroutines.flow.Flow

@Dao
interface PhraseDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(phrase: Phrase): Long

    @Query("SELECT * FROM phrases")
    fun getAllItems(): Flow<List<Phrase>>

    @Query("DELETE FROM phrases WHERE id = :id")
    suspend fun deleteItem(id: Long)

    @Query("DELETE FROM phrases")
    suspend fun deleteAll()

    @Query("SELECT * FROM phrases ORDER BY RANDOM() LIMIT 1;")
    suspend fun getRandomItem(): Phrase?
}