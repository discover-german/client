package com.lmu.contentbrowser.adapters

import android.app.Activity
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.TextView
import androidx.cardview.widget.CardView
import com.lmu.contentbrowser.ContentBrowserApplication
import com.lmu.contentbrowser.R
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.coroutineScope
import kotlinx.coroutines.launch

class EventsAdapter (private val context: Activity, private val entries: Array<String>):
    ArrayAdapter<String>(context, R.layout.layout_content_item, entries) {

    override fun getView(position: Int, view: View?, parent: ViewGroup): View {
        val inflater = context.layoutInflater
        val rowView = inflater.inflate(R.layout.layout_notification_event, parent, false)

        val entry = entries[position]

        var id = 0L
        var eventType = ""
        var time = ""
        var interval = ""

        if (entry.split("|").size == 4) {
            id = entry.split("|")[0].toLong()
            eventType = entry.split("|")[1]
            time = entry.split("|")[2]
            interval = entry.split("|")[3]
        }

        val eventTypeView = rowView.findViewById<TextView>(R.id.eventType)
        val eventTimeView = rowView.findViewById<TextView>(R.id.eventTime)
        val eventRepeatView = rowView.findViewById<TextView>(R.id.eventRepeat)

        when (eventType) {
            "article" -> eventTypeView.text = "recommend article"
            "book" -> eventTypeView.text = "recommend book"
            "podcast" -> eventTypeView.text = "recommend podcast"
            "flashcard" -> eventTypeView.text = "show flashcard"
            "random" -> eventTypeView.text = "surprise me"
        }
        eventTimeView.text = time
        eventRepeatView.text = interval

        val deleteButton = rowView.findViewById<CardView>(R.id.deleteButton)
        deleteButton.setOnClickListener {
            Thread {
                CoroutineScope(Dispatchers.IO).launch {
                    (context.application as ContentBrowserApplication).repository.deleteEventAndMetas(id)
                }
            }.start()
        }

        return rowView
    }
}