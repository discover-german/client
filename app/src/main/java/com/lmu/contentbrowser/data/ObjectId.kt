package com.lmu.contentbrowser.data

import com.google.gson.annotations.SerializedName

data class ObjectId (
    @SerializedName("\$oid")
    val oid: String
)