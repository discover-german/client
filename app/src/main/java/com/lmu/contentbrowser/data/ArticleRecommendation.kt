package com.lmu.contentbrowser.data

data class ArticleRecommendation(
    val recommendation: Article,
    val trigger: Article
)
