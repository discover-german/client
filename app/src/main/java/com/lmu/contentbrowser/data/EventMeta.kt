package com.lmu.contentbrowser.data

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "events_meta")
class EventMeta(

    @PrimaryKey(autoGenerate = true) val id: Long,

    @ColumnInfo(name = "event_id") var event_id: Int,
    @ColumnInfo(name = "meta_key") var meta_key: String,
    @ColumnInfo(name = "meta_value") val meta_value: Long

)