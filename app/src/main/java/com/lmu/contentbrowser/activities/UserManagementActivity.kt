package com.lmu.contentbrowser.activities

import android.app.KeyguardManager
import android.content.Context
import android.content.DialogInterface
import android.content.pm.PackageManager
import android.hardware.biometrics.BiometricPrompt
import android.os.Build
import android.os.Bundle
import android.os.CancellationSignal
import android.util.Log
import android.widget.TextView
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import androidx.cardview.widget.CardView
import androidx.core.app.ActivityCompat
import com.google.android.gms.auth.api.credentials.CredentialRequest
import com.google.android.gms.auth.api.credentials.Credentials
import com.google.android.gms.auth.api.credentials.CredentialsClient
import com.google.android.gms.auth.api.credentials.IdentityProviders
import com.lmu.contentbrowser.Constants
import com.lmu.contentbrowser.R
import com.lmu.contentbrowser.dialogs.ChangeParticipantIdDialog

class UserManagementActivity: AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_user_management)

        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        supportActionBar?.title = "User Management"

        val sharedPref = this.getSharedPreferences(getString(R.string.preference_file_key),
            Context.MODE_PRIVATE)
        if (sharedPref.contains(getString(R.string.participant_id_key))) {
            Constants.participantId =
                sharedPref.getString(getString(R.string.participant_id_key), "") ?: ""
            fillUserCard()
            Log.i("participant_id", "Successfully retrieved participant id")
        } else {
            Log.i(
                "participant_id",
                "No participant id stored. Open app to initialize."
            )
        }

        val changePasswordButton = findViewById<CardView>(R.id.changePassword)
        changePasswordButton.setOnClickListener {
            val changeParticipantIdDialog = ChangeParticipantIdDialog(this)
            changeParticipantIdDialog.show(supportFragmentManager, "change participant id")
        }
    }

    fun fillUserCard() {
        val participantId = findViewById<TextView>(R.id.participantId)
        participantId.text = Constants.participantId
    }
}