package com.lmu.contentbrowser.fragments

import android.content.Intent
import android.content.res.Configuration
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.PopupMenu
import android.widget.TextView
import androidx.appcompat.widget.LinearLayoutCompat
import androidx.cardview.widget.CardView
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import com.lmu.contentbrowser.ContentBrowserApplication
import com.lmu.contentbrowser.R
import com.lmu.contentbrowser.activities.PodcastActivity
import com.lmu.contentbrowser.activities.ViewerActivity
import com.lmu.contentbrowser.callbacks.ImageCallback
import com.lmu.contentbrowser.data.HistoryItem
import com.lmu.contentbrowser.data.Order
import com.lmu.contentbrowser.listeners.SearchListener
import com.lmu.contentbrowser.viewmodels.FavoritesViewModel
import com.lmu.contentbrowser.viewmodels.FavoritesViewModelFactory
import com.lmu.contentbrowser.viewmodels.HistoryViewModel
import com.lmu.contentbrowser.viewmodels.HistoryViewModelFactory
import com.squareup.picasso.Picasso

class FavoritesTabFragment: Fragment() {

    var order = Order.NEWEST

    val historyViewModel: HistoryViewModel by viewModels {
        HistoryViewModelFactory((requireActivity().application
                as ContentBrowserApplication).repository)
    }

    val favoritesViewModel: FavoritesViewModel by viewModels {
        FavoritesViewModelFactory((requireActivity().application
                as ContentBrowserApplication).repository)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_favorites_tab, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val recentFavoritesOrderButton = view.findViewById<CardView>(R.id.recentFavoritesOrderButton)
        recentFavoritesOrderButton.setOnClickListener {
            val pm = PopupMenu(context, it)
            pm.menuInflater.inflate(R.menu.sort_menu, pm.menu)
            pm.setOnMenuItemClickListener {
                when(it.itemId) {
                    R.id.newest -> order = Order.NEWEST
                    R.id.oldest -> order = Order.OLDEST
                    R.id.alphabetical -> order = Order.ALPHABETICAL
                }
                establishHistoryItemsObserver()
                true
            }
            pm.show()
        }
        establishHistoryItemsObserver()
        val searchView = view.findViewById<androidx.appcompat.widget.SearchView>(R.id.searchView)
        searchView.setOnQueryTextListener(SearchListener(requireContext(), true))
    }

    private fun establishHistoryItemsObserver() {
        if (view != null) {
            val recentFavoritesOrder = view?.findViewById<TextView>(R.id.recentFavoritesOrder)
            recentFavoritesOrder?.text = when (order) {
                Order.NEWEST -> "Newest"
                Order.OLDEST -> "Oldest"
                Order.ALPHABETICAL -> "Alphabetical"
            }
            historyViewModel.allHistoryItems.removeObservers(viewLifecycleOwner)
            historyViewModel.allHistoryItems.observe(viewLifecycleOwner) {
                Thread {
                    val displayedItems = arrayListOf<String>()
                    val sortedHistory = when (order) {
                        Order.NEWEST -> it.sortedBy { it.timeAdded }.reversed()
                        Order.OLDEST -> it.sortedBy { it.timeAdded }
                        Order.ALPHABETICAL -> it.sortedBy { it.title }
                    }
                    var favoritesCount = favoritesViewModel.getFavoritesCount()
                    lateinit var favoritesRow: LinearLayoutCompat
                    val favoritesContainer =
                        view?.findViewById<LinearLayoutCompat>(R.id.favoritesContainer)
                    requireActivity().runOnUiThread {
                        favoritesContainer?.removeAllViews()
                    }
                    var favoritesIndex = 1
                    for (historyItem in sortedHistory) {
                        val favorite = favoritesViewModel.getItemByOidAndContentType(
                            historyItem.oid,
                            historyItem.contentType
                        )
                        if (favorite != null && favorite.isFavorite && !displayedItems.contains(favorite.oid)) {
                            displayedItems.add(favorite.oid)
                            requireActivity().runOnUiThread {
                                if (this.isAdded) {
                                    if (favoritesIndex % 2 == 1) {
                                        favoritesRow = layoutInflater.inflate(
                                            R.layout.layout_favorite_row,
                                            requireView().findViewById(R.id.favoriteParent), false
                                        ) as LinearLayoutCompat

                                        val favoriteItem =
                                            fillFavoriteItem(favoritesRow, historyItem)
                                        favoritesRow.addView(favoriteItem)
                                    } else {
                                        val favoriteItem =
                                            fillFavoriteItem(favoritesRow, historyItem)
                                        favoritesRow.addView(favoriteItem)
                                        favoritesContainer?.addView(favoritesRow)
                                    }
                                    if (favoritesIndex == favoritesCount && favoritesIndex % 2 == 1) {
                                        val space =
                                            layoutInflater.inflate(
                                                R.layout.layout_favorite_item_spacer,
                                                favoritesRow, false
                                            )
                                        favoritesRow.addView(space)
                                        favoritesContainer?.addView(favoritesRow)
                                    }
                                    favoritesIndex++
                                }
                            }
                        }
                    }
                }.start()
            }
        }
    }

    private fun fillFavoriteItem(favoritesRow: LinearLayoutCompat, historyItem: HistoryItem): View {
        val favoriteItem =
            layoutInflater.inflate(R.layout.layout_favorite_item,
                favoritesRow, false)

        val favoriteImage =
            favoriteItem.findViewById<ImageView>(R.id.favoriteImage)
        if (historyItem.imageLink != "" && historyItem.imageLink != "null") {
            Picasso.get().load(historyItem.imageLink)
                .resize(500, 0).onlyScaleDown()
                .into(favoriteImage, ImageCallback(favoriteImage))
        } else {
            val nightModeFlags = requireContext().resources.configuration.uiMode and
                    Configuration.UI_MODE_NIGHT_MASK
            when (historyItem.contentType) {
                "article" -> {
                    val bgColor = if (nightModeFlags == Configuration.UI_MODE_NIGHT_YES) {
                        R.color.articleDark
                    }
                    else {
                        R.color.article
                    }
                    favoriteImage.setImageResource(R.drawable.baseline_newspaper_black_48dp)
                    favoriteImage.setBackgroundColor(
                        ContextCompat.getColor(
                            requireContext(),
                            bgColor
                        )
                    )
                }
                "book" -> {
                    val bgColor = if (nightModeFlags == Configuration.UI_MODE_NIGHT_YES) {
                        R.color.bookDark
                    }
                    else {
                        R.color.book
                    }
                    favoriteImage.setImageResource(R.drawable.baseline_menu_book_black_48dp)
                    favoriteImage.setBackgroundColor(
                        ContextCompat.getColor(
                            requireContext(),
                            bgColor
                        )
                    )
                }
                "podcast" -> {
                    val bgColor = if (nightModeFlags == Configuration.UI_MODE_NIGHT_YES) {
                        R.color.podcastDark
                    }
                    else {
                        R.color.podcast
                    }
                    favoriteImage.setImageResource(R.drawable.baseline_podcasts_black_48dp)
                    favoriteImage.setBackgroundColor(
                        ContextCompat.getColor(
                            requireContext(),
                            bgColor
                        )
                    )
                }
                "error" -> {
                    val bgColor = if (nightModeFlags == Configuration.UI_MODE_NIGHT_YES) {
                        R.color.errorDark
                    }
                    else {
                        R.color.error
                    }
                    favoriteImage.setImageResource(R.drawable.baseline_error_outline_black_24dp)
                    favoriteImage.setBackgroundColor(
                        ContextCompat.getColor(
                            requireContext(),
                            bgColor
                        )
                    )
                }
                else -> false
            }
        }
        val favoriteTitle =
            favoriteItem.findViewById<TextView>(R.id.favoriteTitle)
        favoriteTitle.text = historyItem.title

        favoriteImage.setOnClickListener {
            favoriteClickHandler(historyItem)
        }

        favoriteItem.setOnClickListener {
            favoriteClickHandler(historyItem)
        }

        return favoriteItem
    }

    private fun favoriteClickHandler(historyItem: HistoryItem) {
        val intent = if (historyItem.contentType == "podcast") {
            Intent(context, PodcastActivity::class.java)
        } else {
            Intent(context, ViewerActivity::class.java)
        }
        intent.putExtra("item", "${historyItem.contentType}|${historyItem.title}" +
                "|${historyItem.description}|" +
                "${historyItem.oid}|null|${historyItem.imageLink}")
        intent.putExtra("url", historyItem.url)
        requireContext().startActivity(intent)
    }
}