package com.lmu.contentbrowser.dialogs

import android.app.Dialog
import android.content.Context
import android.content.DialogInterface
import android.content.IntentSender
import android.os.Bundle
import android.util.Log
import android.view.autofill.AutofillManager
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.DialogFragment
import com.google.android.gms.auth.api.credentials.Credential
import com.google.android.gms.auth.api.credentials.Credentials
import com.google.android.gms.auth.api.credentials.CredentialsClient
import com.google.android.gms.common.api.ApiException
import com.google.android.gms.common.api.ResolvableApiException
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.google.android.material.textfield.TextInputEditText
import com.lmu.contentbrowser.Constants
import com.lmu.contentbrowser.R
import com.lmu.contentbrowser.activities.MainActivity
import java.lang.Exception

class LoginDialog(private val mainActivity: MainActivity): DialogFragment() {
    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        return activity?.let {
            val builder = MaterialAlertDialogBuilder(mainActivity)
            // Get the layout inflater
            val inflater = requireActivity().layoutInflater

            // Inflate and set the layout for the dialog
            // Pass null as the parent view because its going in the dialog layout
            val dialogView = inflater.inflate(R.layout.layout_login_dialog, null)
            val participantIdInput = dialogView.findViewById<TextInputEditText>(R.id.participantIdInput)
            builder.setView(dialogView)
                // Add action buttons
                .setPositiveButton("Submit",
                    DialogInterface.OnClickListener { dialog, id ->
                        val participantId = participantIdInput.text.toString()
                        createUser(participantId)
                        mainActivity.setupFragmentsAndBottomNavBar()
                        mainActivity.insertDefaultDailyReminder()
                        mainActivity.refreshCurrentFragment()
                    })
                .setNegativeButton("Cancel",
                    DialogInterface.OnClickListener { dialog, id ->
                        mainActivity.setupFragmentsAndBottomNavBar()
                        getDialog()?.cancel()
                        mainActivity.insertDefaultDailyReminder()
                        mainActivity.refreshCurrentFragment()
                    })
            builder.create()
        } ?: throw IllegalStateException("Activity cannot be null")
    }

    private fun createUser(participantId: String) {
        Constants.participantId = participantId
        val sharedPref = activity?.getSharedPreferences(getString(R.string.preference_file_key),
        Context.MODE_PRIVATE) ?: return
        with (sharedPref.edit()) {
            putString(getString(R.string.participant_id_key), participantId)
            apply()
        }
        mainActivity.setupFragmentsAndBottomNavBar()
    }
}