package com.lmu.contentbrowser.dialogs

import android.app.DatePickerDialog
import android.app.Dialog
import android.app.TimePickerDialog
import android.content.DialogInterface
import android.os.Bundle
import android.view.View
import android.widget.*
import androidx.appcompat.app.AlertDialog
import androidx.cardview.widget.CardView
import androidx.fragment.app.DialogFragment
import com.lmu.contentbrowser.R
import com.lmu.contentbrowser.data.Event
import com.lmu.contentbrowser.data.EventMeta
import com.lmu.contentbrowser.data.LogEvent
import com.lmu.contentbrowser.data.LogEventType
import com.lmu.contentbrowser.viewmodels.EventViewModel
import com.lmu.contentbrowser.viewmodels.LogEventViewModel
import java.text.SimpleDateFormat
import java.time.LocalDateTime
import java.time.OffsetDateTime
import java.time.format.DateTimeFormatter
import java.util.*

class CreateNotificationDialog(val eventViewModel: EventViewModel,
                               val logEventViewModel: LogEventViewModel): DialogFragment() {
    var eventTime = ""
    var eventDate = ""
    var eventInterval = Long.MAX_VALUE
    var eventType = "article"

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        return activity?.let {
            val builder = AlertDialog.Builder(it)
            // Get the layout inflater
            val inflater = requireActivity().layoutInflater;

            // Inflate and set the layout for the dialog
            // Pass null as the parent view because its going in the dialog layout
            val dialogView = inflater.inflate(R.layout.layout_create_notification_dialog, null)

            populateDialogView(dialogView)

            builder.setView(dialogView)
                // Add action buttons
                .setPositiveButton("Submit",
                    DialogInterface.OnClickListener { dialog, id ->
                        insertEvent(
                            eventTime,
                            eventDate,
                            eventType,
                            eventInterval
                        )
                    })
                .setNegativeButton("Cancel",
                    DialogInterface.OnClickListener { dialog, id ->
                        getDialog()?.cancel()
                    })
            builder.create()
        } ?: throw IllegalStateException("Activity cannot be null")
    }

    private fun populateDialogView(dialogView: View) {
        val dateFormat = SimpleDateFormat("yyyy-MM-dd")
        val timeFormat = SimpleDateFormat("HH:mm")

        val calendar = Calendar.getInstance()

        val currentTime = timeFormat.format(calendar.timeInMillis)
        val currentDate = dateFormat.format(calendar.timeInMillis)

        eventTime = currentTime
        eventDate = currentDate
        eventInterval = Long.MAX_VALUE

        val selectedTimeView = dialogView.findViewById<TextView>(R.id.selectedTime)
        val selectedDateView = dialogView.findViewById<TextView>(R.id.selectedDate)
        val selectedIntervalView = dialogView.findViewById<TextView>(R.id.selectedInterval)
        val selectedEventTypeView = dialogView.findViewById<TextView>(R.id.selectedEventType)

        selectedTimeView.text = currentTime
        selectedDateView.text = currentDate
        selectedIntervalView.text = "never"
        selectedEventTypeView.text = "recommend article"

        val selectTimeButton = dialogView.findViewById<CardView>(R.id.selectTimeButton)
        val selectDateButton = dialogView.findViewById<CardView>(R.id.selectDateButton)
        val selectIntervalButton = dialogView.findViewById<CardView>(R.id.selectIntervalButton)
        val selectEventTypeButton = dialogView.findViewById<CardView>(R.id.selectEventTypeButton)

        selectTimeButton.setOnClickListener {
            val timeDialog = TimePickerDialog(requireContext(), TimeSetListener(selectedTimeView),
                calendar.get(Calendar.HOUR_OF_DAY), calendar.get(Calendar.MINUTE), true)
            timeDialog.show()
        }

        selectDateButton.setOnClickListener {
            //val calendar = Calendar.getInstance()
            val dateDialog = DatePickerDialog(requireContext())
            //dateDialog.datePicker.setda = calendar.get(Calendar.DAY_OF_MONTH)
            dateDialog.setOnDateSetListener(
                DateSetListener(
                    selectedDateView
                )
            )
            dateDialog.show()
        }

        selectIntervalButton.setOnClickListener {
            val pm = PopupMenu(context, it)
            pm.menuInflater.inflate(R.menu.interval_menu, pm.menu)
            pm.setOnMenuItemClickListener {
                when(it.itemId) {
                    R.id.daily -> {
                        eventInterval = 24*60*60
                        selectedIntervalView.text = "daily"
                    }
                    R.id.bidaily -> {
                        eventInterval = 2*24*60*60
                        selectedIntervalView.text = "alternate day"
                    }
                    R.id.weekly -> {
                        eventInterval = 7*24*60*60
                        selectedIntervalView.text = "weekly"
                    }
                    R.id.never -> {
                        eventInterval = Long.MAX_VALUE
                        selectedIntervalView.text = "never"
                    }
                }
                true
            }
            pm.show()
        }

        selectEventTypeButton.setOnClickListener {
            val pm = PopupMenu(context, it)
            pm.menuInflater.inflate(R.menu.event_menu, pm.menu)
            pm.setOnMenuItemClickListener {
                when(it.itemId) {
                    R.id.article -> {
                        eventType = "article"
                        selectedEventTypeView.text = "recommend article"
                    }
                    R.id.book -> {
                        eventType = "book"
                        selectedEventTypeView.text = "recommend book"
                    }
                    R.id.podcast -> {
                        eventType = "podcast"
                        selectedEventTypeView.text = "recommend podcast"
                    }
                    R.id.flashcard -> {
                        eventType = "flashcard"
                        selectedEventTypeView.text = "recommend flashcard"
                    }
                    R.id.random -> {
                        eventType = "random"
                        selectedEventTypeView.text = "surprise me"
                    }
                }
                true
            }
            pm.show()
        }
    }


    private fun insertEvent(time: String, date: String, eventType: String, interval: Long = Long.MAX_VALUE) {

        if (time != "" && date != "") {
            val formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm")
            val dateTime = LocalDateTime.parse("$date $time", formatter)
            val epochTime =
                dateTime.toEpochSecond(OffsetDateTime.now().offset)
            val event1 = Event(0, eventType)
            val eventMeta11 = EventMeta(0, 0, "repeat_start", epochTime)
            val eventMeta12 =
                EventMeta(0, 0, "repeat_interval", interval) //Max Value: almost non-repeating, 7 days: 604800
            val eventMetas =
                listOf(mapOf("start" to eventMeta11, "interval" to eventMeta12))
            eventViewModel.insertEvent(event1, eventMetas)
            val logEventType = when (eventType) {
                "article" -> LogEventType.ARTICLE_NOTIFICATION_SET
                "book" -> LogEventType.BOOK_NOTIFICATION_SET
                "podcast" -> LogEventType.PODCAST_NOTIFICATION_SET
                "flashcard" -> LogEventType.FLASHCARD_NOTIFICATION_SET
                "random" -> LogEventType.SURPRISE_NOTIFICATION_SET
                else -> LogEventType.NONE
            }
            logEventViewModel.insertEvent(
                LogEvent(0, logEventType, System.currentTimeMillis(),
                    null, false)
            )
            Toast.makeText(requireContext(),
                "Created a new event for $date at $time",
                Toast.LENGTH_SHORT).show()
        }
    }

    inner class TimeSetListener(val selectedTimeView: TextView): TimePickerDialog.OnTimeSetListener {
        override fun onTimeSet(view: TimePicker?, hourOfDay: Int, minute: Int) {
            eventTime = String.format("%02d:%02d", hourOfDay, minute)
            selectedTimeView.text = eventTime
        }
    }

    inner class DateSetListener(val selectedDateView: TextView): DatePickerDialog.OnDateSetListener {
        override fun onDateSet(view: DatePicker?, year: Int, month: Int, dayOfMonth: Int) {
            eventDate = String.format("%04d-%02d-%02d", year, month+1, dayOfMonth)
            selectedDateView.text = eventDate
        }

    }
}