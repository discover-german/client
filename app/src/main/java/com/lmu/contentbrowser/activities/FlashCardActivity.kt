package com.lmu.contentbrowser.activities

import android.content.Intent
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.widget.FrameLayout
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.lifecycleScope
import com.lmu.contentbrowser.ContentBrowserApplication
import com.lmu.contentbrowser.R
import com.lmu.contentbrowser.data.LogEvent
import com.lmu.contentbrowser.data.LogEventType
import com.lmu.contentbrowser.fragments.FlashCardBackFragment
import com.lmu.contentbrowser.fragments.FlashCardFrontFragment
import com.lmu.contentbrowser.viewmodels.LogEventViewModel
import com.lmu.contentbrowser.viewmodels.LogEventViewModelFactory
import com.lmu.contentbrowser.viewmodels.PhrasesViewModel
import com.lmu.contentbrowser.viewmodels.PhrasesViewModelFactory
import kotlinx.coroutines.launch
import kotlin.random.Random

class FlashCardActivity: AppCompatActivity() {
    var mShowingBack = false
    var frontText: String = ""
    var backText: String = ""

    val phraseViewModel: PhrasesViewModel by viewModels {
        PhrasesViewModelFactory((application as ContentBrowserApplication).repository)
    }

    val logEventViewModel: LogEventViewModel by viewModels {
        LogEventViewModelFactory((application as ContentBrowserApplication).repository)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_flashcard)

        val fragmentContainer = findViewById<FrameLayout>(R.id.fragment_container)

        fillCard(savedInstanceState)

        fragmentContainer.setOnClickListener { flipCard() }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // R.menu.mymenu is a reference to an xml file named mymenu.xml which should be inside your res/menu directory.
        // If you don't have res/menu, just create a directory named "menu" inside res
        menuInflater.inflate(R.menu.flashcard_menu, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onResume() {
        super.onResume()
        val logEventType = LogEventType.FLASHCARD_START
        val currentTime = System.currentTimeMillis()
        logEventViewModel.insertEvent(
            LogEvent(0, logEventType, currentTime, null,
            false)
        )
    }

    override fun onPause() {
        super.onPause()
        val logEventType = LogEventType.FLASHCARD_END
        val currentTime = System.currentTimeMillis()
        logEventViewModel.insertEvent(LogEvent(0, logEventType, currentTime, null,
            false))
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        val id = item.itemId
        if (id == R.id.refreshButton) {
            val refreshIntent = Intent(this, this::class.java)
            startActivity(refreshIntent)
            finish()
        }
        return super.onOptionsItemSelected(item)
    }

    private fun fillCard(savedInstanceState: Bundle?) {
        lifecycleScope.launch {
            val phrase = phraseViewModel.getRandomPhrase()
            if (phrase != null) {
                supportActionBar?.title = "Random Flashcard"
                val random = Random(System.nanoTime())
                if (random.nextBoolean()) {
                    frontText = phrase.phrase
                    backText = phrase.translation
                } else {
                    frontText = phrase.translation
                    backText = phrase.phrase
                }
                runOnUiThread {
                    if (savedInstanceState == null) {
                        supportFragmentManager
                            .beginTransaction()
                            .add(R.id.fragment_container, FlashCardFrontFragment(frontText))
                            .commit()
                    }
                }
            }
            else {
                supportActionBar?.title = "No Words for Flashcard yet"
                return@launch
            }
        }
    }

    fun flipCard() {
        if (mShowingBack) {
            supportFragmentManager.popBackStack()
            mShowingBack = false
            return
        }

        // Flip to the back.
        mShowingBack = true

        // Create and commit a new fragment transaction that adds the fragment for the back of
        // the card, uses custom animations, and is part of the fragment manager's back stack.
        supportFragmentManager
            .beginTransaction() // Replace the default fragment animations with animator resources representing
            // rotations when switching to the back of the card, as well as animator
            // resources representing rotations when flipping back to the front (e.g. when
            // the system Back button is pressed).
            .setCustomAnimations(
                R.animator.card_flip_right_in, R.animator.card_flip_right_out,
                R.animator.card_flip_left_in, R.animator.card_flip_left_out) // Replace any fragments currently in the container view with a fragment
            // representing the next page (indicated by the just-incremented currentPage
            // variable).
            .replace(R.id.fragment_container, FlashCardBackFragment(backText)) // Add this transaction to the back stack, allowing users to press Back
            // to get to the front of the card.
            .addToBackStack(null) // Commit the transaction.
            .commit()
    }
}