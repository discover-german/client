package com.lmu.contentbrowser.callbacks

import android.util.Log
import android.widget.ImageView
import com.lmu.contentbrowser.R
import java.lang.Exception

class ImageCallback(val imageView: ImageView): com.squareup.picasso.Callback {
    override fun onSuccess() {
        Log.i("Picasso", "image successfully loaded")
    }

    override fun onError(e: Exception?) {
        imageView.setImageResource(R.drawable.baseline_error_outline_black_24dp)
        Log.e("Picasso", "error loading image: $e")
    }
}