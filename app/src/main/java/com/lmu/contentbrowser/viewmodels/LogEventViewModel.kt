package com.lmu.contentbrowser.viewmodels

import androidx.lifecycle.*
import com.lmu.contentbrowser.data.LogEvent
import com.lmu.contentbrowser.database.MyRepository
import kotlinx.coroutines.launch

class LogEventViewModel (private val repository: MyRepository) : ViewModel() {

    val unsyncedLogEvents: LiveData<List<LogEvent>> = repository.getSyncedLogEvents(false).asLiveData()
    val syncedLogEvents: LiveData<List<LogEvent>> = repository.getSyncedLogEvents(true).asLiveData()

    fun insertEvent(logEvent: LogEvent) = viewModelScope.launch {
        repository.insertLogEvent(logEvent)
    }

    fun deleteEvent(id: Long) = viewModelScope.launch {
        repository.deleteLogEvent(id)
    }
}

class LogEventViewModelFactory(private val repository: MyRepository) : ViewModelProvider.Factory {
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(LogEventViewModel::class.java)) {
            @Suppress("UNCHECKED_CAST")
            return LogEventViewModel(repository) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }
}