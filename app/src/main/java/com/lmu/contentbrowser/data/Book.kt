package com.lmu.contentbrowser.data

data class Book(
    val _id: ObjectId,
    val title: String,
    val author: String,
    val link: String,
    val text: List<String>,
    val image_link: String?
)