package com.lmu.contentbrowser.dialogs

import android.app.Dialog
import android.content.Context
import android.content.DialogInterface
import android.os.Bundle
import android.util.Log
import android.widget.ArrayAdapter
import androidx.fragment.app.DialogFragment
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.lmu.contentbrowser.Constants
import com.lmu.contentbrowser.R
import com.lmu.contentbrowser.activities.MainActivity
import com.lmu.contentbrowser.fragments.HomeFragment
import okhttp3.FormBody
import okhttp3.OkHttpClient
import okhttp3.Request
import okhttp3.Response
import java.util.concurrent.TimeUnit

class DismissContentItemDialog(private val mContext: Context, private val arrayAdapter: ArrayAdapter<String>,
                               private val entries: List<String>, private val deleteIndex: Int,
                               private val oid: String,
                               private val contentType: String,
                               private val episodeIndex: String): DialogFragment() {
    private val client = OkHttpClient.Builder().connectTimeout(Constants.CONNECT_TIMEOUT,
        TimeUnit.SECONDS).build()
    private val dismissUrl = Constants.SERVER_URL + "/dismiss"
    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        return activity?.let {
            val builder = MaterialAlertDialogBuilder(mContext)
            // Get the layout inflater
            val inflater = requireActivity().layoutInflater
            val dialogView = inflater.inflate(R.layout.layout_dismiss_dialog, null)
            builder.setView(dialogView)
                .setPositiveButton("Remove",
                    DialogInterface.OnClickListener { dialog, id ->
                        dismissItem(oid, contentType, episodeIndex)
                        arrayAdapter.remove(entries[deleteIndex])
                        arrayAdapter.notifyDataSetChanged()
                        if (mContext is MainActivity) {
                            val navHostFragment =
                                mContext.supportFragmentManager.findFragmentById(R.id.nav_host_fragment)
                            if (navHostFragment != null) {
                                val homeFragment = navHostFragment.childFragmentManager.fragments[0]
                                if (homeFragment != null && homeFragment is HomeFragment) {
                                    homeFragment.popDataItem(deleteIndex)
                                }
                            }
                        }
                    })
                .setNegativeButton("Cancel",
                    DialogInterface.OnClickListener { dialog, id ->
                        getDialog()?.cancel()
                    })
            builder.create()
        } ?: throw IllegalStateException("Activity cannot be null")
    }

    private fun dismissItem(oid: String, contentType: String,
                            episodeIndex: String) {
        Thread {
            try {
                val formBody = FormBody.Builder()
                    .add("oid", oid)
                    .add("contentType", contentType)
                    .add("episodeIndex", episodeIndex).build()
                val request: Request = Request.Builder().url(dismissUrl)
                    .put(formBody)
                    .header("participantId", Constants.participantId).build()
                val response: Response = client.newCall(request).execute()
                if (response.code == 200) {
                    Log.d("API", "Dismissed Item")
                }
            }
            catch (e: Exception) {
                Log.e("API", "could not dismiss item")
            }
        }.start()
    }
}