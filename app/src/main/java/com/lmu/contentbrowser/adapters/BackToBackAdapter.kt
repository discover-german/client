package com.lmu.contentbrowser.adapters

import android.app.Activity
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.TextView
import com.lmu.contentbrowser.R

class BackToBackAdapter (private val context: Activity, private val entries: Array<String>):
    ArrayAdapter<String>(context, R.layout.layout_row_back_to_back_translation, entries) {
    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        val row = context.layoutInflater.inflate(R.layout.layout_row_back_to_back_translation, parent, false)
        val originalView = row.findViewById<TextView>(R.id.original)
        val translationView = row.findViewById<TextView>(R.id.translation)
        val entry = entries[position]
        if (entry.split("|").size == 2) {
            originalView.text = entry.split("|")[0]
            translationView.text = entry.split("|")[1]
        }
        return row
    }
}