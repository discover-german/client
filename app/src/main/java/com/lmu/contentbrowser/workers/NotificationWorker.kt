package com.lmu.contentbrowser.workers

import android.content.Context
import androidx.work.Worker
import androidx.work.WorkerParameters
import com.awesome.germanhelper.NotificationHelper


class NotificationWorker(val context: Context, workerParams: WorkerParameters) :
    Worker(context, workerParams) {

    val notificationHelper = NotificationHelper()

    override fun doWork(): Result {
        notificationHelper.setupNotificationReceiver(context)
        notificationHelper.checkForNotifications(context)
        return Result.success()
    }

    companion object {
        var scheduled = false
    }
}